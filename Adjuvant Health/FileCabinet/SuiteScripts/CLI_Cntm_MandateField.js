/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @returns {Boolean} True to save line item, false to abort save Mandate the Division based on A/C Type Mandate the Division and Provider
 */

// initialize script parameters
var acctypes = nlapiGetContext().getSetting('script', 'custscript_cntm_acc_type_div');
acctypes = acctypes.split(',');

var accnums = nlapiGetContext().getSetting('script', 'custscript_cntm_acc_num_div');
accnums = accnums.split(',');

var acctypes_div_prov = nlapiGetContext().getSetting('script', 'custscript_cntm_acc_type_div_prov');
acctypes_div_prov = acctypes_div_prov.split(',');

var divisionIds = nlapiGetContext().getSetting('script', 'custscript_cntm_div_id_for_dep');
divisionIds = divisionIds.split(',');

var departmentIds = nlapiGetContext().getSetting('script', 'custscript_cntm_dep_id_for_prov');
departmentIds = departmentIds.split(',');

var provider_acctypes = nlapiGetContext().getSetting('script', 'custscript_cntm_acc_type_for_prov');
provider_acctypes = provider_acctypes.split(',');

var item_recordType = nlapiGetContext().getSetting('script', 'custscript_cntm_item_record_type');
item_recordType = item_recordType.split(',');

var postingDate = nlapiGetContext().getSetting('script', 'custscript_cntm_transc_date');
postingDate = postingDate.split(',');

var recordType;
function initailize_variable_clientPageInit()
{
	try
	{
		nlapiLogExecution('DEBUG', '', 'In pageInit');

		recordType = nlapiGetRecordType();
		nlapiLogExecution('DEBUG', '', 'Record Type: ' + recordType);
		nlapiLogExecution('DEBUG', '', 'Acceptable A/C Types for Division: ' + acctypes);
		nlapiLogExecution('DEBUG', '', 'Acceptable A/C Number for Division: ' + accnums);
		nlapiLogExecution('DEBUG', '', 'Acceptable A/C Type for Division & Provider: ' + acctypes_div_prov);
		nlapiLogExecution('DEBUG', '', 'Acceptable Division Ids for Department: ' + divisionIds);
		nlapiLogExecution('DEBUG', '', 'Acceptable Department Ids for Provider: ' + departmentIds);
		nlapiLogExecution('DEBUG', '', 'Acceptable A/C Type for Provider: ' + provider_acctypes);
		nlapiLogExecution('DEBUG', '', 'Records which do not have A/C: ' + item_recordType);

		postingDate = nlapiDateToString(nlapiStringToDate(postingDate[0], postingDate[1]));
		nlapiLogExecution('DEBUG', '', 'Acceptable Date for Validation: ' + postingDate);
		

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'PageInit Error Details: ' + e.message);

	}

}

// make Field Mandatory based on criteria
function mandateField_validateLine(type)
{
	try
	{
		nlapiLogExecution('DEBUG', '', 'In validateField : Field Type '+type);

		// to check if any of conditions failed
		var flag = true;

		// Record Date
		var date = nlapiGetFieldValue('trandate');
		nlapiLogExecution('DEBUG', '', 'Record Date: ' + date);

		// check if date is equal and after Posting date
		var isDateValid = (new Date(date) >= new Date(postingDate));

		nlapiLogExecution('DEBUG', '', 'Is Date Valid for update: ' + isDateValid);

		var accId;
		var accObj;
		var accnum;
		var acctype;

		if (item_recordType.indexOf(recordType) < 0)
		{
			accId = nlapiGetCurrentLineItemValue(type, 'account');
			nlapiLogExecution('DEBUG', '', 'A/C Id: ' + accId);

			if (accId)
			{
				accObj = nlapiLookupField('account', accId, [ 'number', 'type' ]);
				nlapiLogExecution('DEBUG', '', 'A/C Obj: ' + JSON.stringify(accObj));

				// A/C number
				accnum = accObj['number'];
				nlapiLogExecution('DEBUG', '', 'A/C Num: ' + accnum);

				// A/C type
				acctype = accObj['type'];
				nlapiLogExecution('DEBUG', '', 'A/C Type: ' + acctype);
			}
		}

		// Division
		var division = nlapiGetCurrentLineItemValue(type, 'class');
		nlapiLogExecution('DEBUG', '', 'Division Intially: ' + division);

		// Custom Provider
		var provider = nlapiGetCurrentLineItemValue(type, 'custcol_cntm_custom_provider');
		nlapiLogExecution('DEBUG', '', 'Custom Provider Intially: ' + provider);

		// Department
		var department = nlapiGetCurrentLineItemValue(type, 'department');
		nlapiLogExecution('DEBUG', '', 'Department Intially: ' + department);

		// Mandatory Division based on A/C number
		if (isDateValid && (accnum && accnums.indexOf(accnum) > -1))
		{
			nlapiLogExecution('DEBUG', '', 'Executing Mandatory Division based on A/C number..');
			flag = mandateDivision(division);
			if (!flag)
			{
				return flag;
			}

		}

		// Mandatory Division based on A/C Type
		if (isDateValid && ((acctype && acctypes.indexOf(acctype) > -1) || (item_recordType.indexOf(recordType) > -1)))
		{
			nlapiLogExecution('DEBUG', '', ' Executing Mandatory Division based on A/C Type..');
			flag = mandateDivision(division);
			if (!flag)
			{
				return flag;
			}
		}

		// Mandatory Division and Provider based on A/C Type
		if (isDateValid && (acctype && acctypes_div_prov.indexOf(acctype) > -1))
		{
			nlapiLogExecution('DEBUG', '', 'Executing Mandatory Division and Provider based on A/C Type..');
			flag = (mandateDivision(division) && mandateProvider(provider));
			if (!flag)
			{
				return flag;
			}
		}

		// Department mandatory based on Division
		if (isDateValid && (division && divisionIds.indexOf(division) > -1) && (accId || (item_recordType.indexOf(recordType) > -1)))
		{
			nlapiLogExecution('DEBUG', '', 'Executing Department mandatory based on Division..');
			flag = mandateDepartment(department);
			if (!flag)
			{
				return flag;
			}
		}
		// Provider mandatory based on Account Type and Department
		if (isDateValid && (department && provider_acctypes.indexOf(acctype) > -1 && departmentIds.indexOf(department) > -1))
		{
			nlapiLogExecution('DEBUG', '', 'Executing Provider mandatory based on Account Type and Department..');
			flag = mandateProvider(provider);
			if (!flag)
			{
				return flag;
			}
		}

		nlapiLogExecution('DEBUG', '', 'Current Value of Flag: ' + flag);

		// Standard Provider
		var standardProvider = nlapiGetCurrentLineItemValue(type, 'location');
		nlapiLogExecution('DEBUG', '', 'Standard Provider Intially: ' + standardProvider);

		nlapiLogExecution('DEBUG', '', 'Custom Provider on validate: ' + provider);

		// Sync standard provider
		nlapiSetCurrentLineItemValue(type, 'location', provider, false, true);

		return flag;
	}
	catch (e)
	{
		if (e.code == 'INSUFFICIENT_PERMISSION')
			alert(e.message);
		nlapiLogExecution('ERROR', '', 'ValidateLine Error Details: ' + e.message);

	}

}

// Reset Fields on A/C change
function resetFields_fieldChanged(type, name, linenum)
{
	try
	{
			//nlapiLogExecution('DEBUG', '', 'In fieldChanged: ' + name);
			if((recordType=='vendorbill'|| recordType=='vendorcredit' || recordType=='creditcardcharge') && type=='item')
				return true;
		// reset lines provider, division and department when date changes
		/*if (!type && name == 'trandate')
		{

			var recordType = nlapiGetRecordType();
			var lineType;
			// for line sublist
			if (recordType.search('journalentry') > -1)
			{
				lineType = 'line';

			}
			// for expense sublist
			else if (recordType == 'vendorbill' || recordType == 'vendorcredit' || recordType == 'creditcardcharge')
			{
				lineType = 'expense';
			}
			// for item sublist
			else if (recordType == 'invoice')
			{

				lineType = 'item';
			}

			var count = nlapiGetLineItemCount(lineType);
			nlapiLogExecution('DEBUG', '', 'Count on Date Change : ' + count);
			for (var linenum = 1; linenum <= count; linenum++)
			{
				nlapiLogExecution('DEBUG', '', 'Line : ' + linenum);
				nlapiSelectLineItem(lineType, linenum);
				//nlapiSetCurrentLineItemValue(lineType, 'class', '', false, false);
				//nlapiSetCurrentLineItemValue(lineType, 'location', '', false, false);
				//nlapiSetCurrentLineItemValue(lineType, 'custcol_cntm_custom_provider', '', false, false);
				//nlapiSetCurrentLineItemValue(lineType, 'department', '', false, false);
				nlapiCommitLineItem(lineType);
				
			}

		}*/

		if (type && name == 'account')
		{

			nlapiSetCurrentLineItemValue(type, 'custcol_cntm_custom_provider', '', false, true);
			nlapiSetCurrentLineItemValue(type, 'class', '', false, true);
			nlapiSetCurrentLineItemValue(type, 'department', '', false, true);

		}
		if (type && name == 'class')
		{

			// nlapiSetCurrentLineItemValue(type, 'custcol_cntm_custom_provider', '', false, false);
			nlapiSetCurrentLineItemValue(type, 'department', '', false, true);

		}
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'FieldChanged Error Details: ' + e.message);

	}

}

// to make Division mandate
function mandateDivision(division)
{

	if (division == '')
	{
		alert('Please select a Division');
		return false;
	}
	return true;
}

// to make Provider mandate
function mandateProvider(provider)
{
	if (provider == '')
	{
		alert('Please select a Provider');
		return false;
	}
	return true;
}

// to make Department mandatory
function mandateDepartment(department)
{
	if (department == '')
	{
		alert('Please select a Department');
		return false;
	}
	return true;
}
