/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @returns {Void}
 */

// Script Parameters
var acctypes;
var accnums;
var acctypes_div_prov;
var divisionIds;
var departmentIds;
var provider_acctypes;
function setMandatory_beforeSubmit(type)
{
	var currentContext = nlapiGetContext().getExecutionContext();
	nlapiLogExecution('DEBUG', '', 'Current Context: ' + currentContext);

	// if (currentContext != 'userinterface')
	{

		try
		{
			// initialize script parameters
			acctypes = nlapiGetContext().getSetting('script', 'custscript_cntm_acc_type_div_ue');
			acctypes = acctypes.split(',');
			nlapiLogExecution('DEBUG', '', 'Acceptable A/C Types for Division: ' + acctypes);

			accnums = nlapiGetContext().getSetting('script', 'custscript_cntm_acc_num_div_ue');
			nlapiLogExecution('DEBUG', '', 'Acceptable A/C Number for Division: ' + accnums);
			accnums = accnums.split(',');

			acctypes_div_prov = nlapiGetContext().getSetting('script', 'custscript_cntm_acc_type_div_prov_ue');
			nlapiLogExecution('DEBUG', '', 'Acceptable A/C Type for Division & Provider: ' + acctypes_div_prov);
			acctypes_div_prov = acctypes_div_prov.split(',');

			divisionIds = nlapiGetContext().getSetting('script', 'custscript_cntm_div_id_for_dep_ue');
			nlapiLogExecution('DEBUG', '', 'Acceptable Division Ids for Department: ' + divisionIds);
			divisionIds = divisionIds.split(',');

			departmentIds = nlapiGetContext().getSetting('script', 'custscript_cntm_dep_id_for_prov_ue');
			nlapiLogExecution('DEBUG', '', 'Acceptable Department Ids for Provider: ' + departmentIds);
			departmentIds = departmentIds.split(',');

			provider_acctypes = nlapiGetContext().getSetting('script', 'custscript_cntm_acc_type_for_prov_ue');
			nlapiLogExecution('DEBUG', '', 'Acceptable A/C Type for Provider: ' + provider_acctypes);
			provider_acctypes = provider_acctypes.split(',');

			// var record_types = nlapiGetContext().getSetting('script', 'custscript_cntm_rec_types_ue');
			// nlapiLogExecution('DEBUG', '', 'Acceptable A/C Type for Provider: ' + record_types);
			// record_types=record_types.split(',');

			var recordId = nlapiGetRecordId();
			var recordType = nlapiGetRecordType();
			nlapiLogExecution('DEBUG', '', 'RecordType: ' + recordType + ' RecordId: ' + recordId);

			// Criteria Date to check if record is ready for update
			var postingDate = nlapiGetContext().getSetting('script', 'custscript_cntm_transc_date_ue');
			postingDate = postingDate.split(',');
			postingDate = nlapiDateToString(nlapiStringToDate(postingDate[0], postingDate[1]));
			nlapiLogExecution('DEBUG', '', 'Acceptable Date for Validation: ' + postingDate);

			// Record Transaction Date
			var date = nlapiGetFieldValue('trandate');
			nlapiLogExecution('DEBUG', '', 'Record Date: ' + date);

			// check if transaction is valid for update
			var isDateValid = (new Date(date) >= new Date(postingDate));
			nlapiLogExecution('DEBUG', '', 'Is Date valid for update: ' + isDateValid);

			var accDetailsMap;

			// for line sublist
			if (isDateValid && recordType.search('journalentry') > -1)
			{
				accDetailsMap = loadAccountDetails();
				validateLines('line', isDateValid, recordType, accDetailsMap);
			}
			// for expense sublist
			else if (isDateValid && (recordType == 'vendorbill' || recordType == 'vendorcredit' || recordType == 'creditcardcharge'))
			{
				accDetailsMap = loadAccountDetails();
				validateLines('expense', isDateValid, recordType, accDetailsMap);
			}
			// for item sublist
			else if (isDateValid && recordType == 'invoice')
			{

				validateLines('item', isDateValid, recordType, null);
			}

		}
		catch (e)
		{
			// Check if custom made error is occurred
			if (e.code == '001' || e.code == '002' || e.code == '003')
				throw e;
			nlapiLogExecution('ERROR', '', 'Unable to Submit Record: ' + recordId + ' Record Type: ' + recordType + ' Error Details: ' + e.message);

		}
	}

}

function validateLines(lineType, isDateValid, recordType, accDetailsMap)
{
	var accId;
	var accObj;
	var accnum;
	var acctype;
	nlapiLogExecution('DEBUG', '', 'lineType: ' + lineType);

	for (var linenum = 1; linenum <= nlapiGetLineItemCount(lineType); linenum++)
	{
		// Standard Provider
		var standardProvider = nlapiGetLineItemValue(lineType, 'location', linenum);
		nlapiLogExecution('DEBUG', '', 'Standard Provider Intially: ' + standardProvider);

		if (standardProvider)
		{
			// Copying Standard Provider into Custom Provider
			nlapiSetLineItemValue(lineType, 'custcol_cntm_custom_provider', linenum, standardProvider);
		}
		// check if record does not have item as sublist
		if (lineType != 'item')
		{
			accId = nlapiGetLineItemValue(lineType, 'account', linenum);
			nlapiLogExecution('DEBUG', '', 'A/C Id: ' + accId);

			if (accId)
			{
				accObj = accDetailsMap[accId];
				nlapiLogExecution('DEBUG', '', 'A/C Obj: ' + JSON.stringify(accObj));

				// A/C number
				accnum = accObj['number'];
				nlapiLogExecution('DEBUG', '', 'A/C Num: ' + accnum);

				// A/C type
				acctype = accObj['type'];
				nlapiLogExecution('DEBUG', '', 'A/C Type: ' + acctype);
			}
		}

		// Division
		var division = nlapiGetLineItemValue(lineType, 'class', linenum);
		nlapiLogExecution('DEBUG', '', 'Division Intially: ' + division);

		// Custom Provider
		var provider = nlapiGetLineItemValue(lineType, 'custcol_cntm_custom_provider', linenum);
		nlapiLogExecution('DEBUG', '', 'Custom Provider Intially: ' + provider);

		// Department
		var department = nlapiGetLineItemValue(lineType, 'department', linenum);
		nlapiLogExecution('DEBUG', '', 'Department Intially: ' + department);

		// Mandatory Division based on A/C number
		if (accnum && accnums.indexOf(accnum) > -1)
		{
			nlapiLogExecution('DEBUG', '', 'Executing Mandatory Division based on A/C number..');
			mandateDivision(division);

		}

		// Mandatory Division based on A/C Type, for an item A/C is default to Income
		if ((acctype && acctypes.indexOf(acctype) > -1) || (lineType == 'item'))
		{
			nlapiLogExecution('DEBUG', '', ' Executing Mandatory Division based on A/C Type..');
			mandateDivision(division);

		}

		// Mandatory Division and Provider based on A/C Type
		if (acctype && acctypes_div_prov.indexOf(acctype) > -1)
		{
			nlapiLogExecution('DEBUG', '', 'Executing Mandatory Division and Provider based on A/C Type..');
			mandateDivision(division);
			mandateProvider(provider);
		}

		// Department mandatory based on Division, for an item A/C is default to Income
		if ((division && divisionIds.indexOf(division) > -1) && (accId || (lineType == 'item')))
		{
			nlapiLogExecution('DEBUG', '', 'Executing Department mandatory based on Division..');
			mandateDepartment(department);
		}
		// Provider mandatory based on Account Type and Department
		if ((department && provider_acctypes.indexOf(acctype) > -1 && departmentIds.indexOf(department) > -1))
		{
			nlapiLogExecution('DEBUG', '', 'Executing Provider mandatory based on Account Type and Department..');
			mandateProvider(provider);
		}

		nlapiLogExecution('DEBUG', '', 'Custom Provider on validate: ' + provider);

		// Sync standard provider
		nlapiSetLineItemValue(lineType, 'location', linenum, provider);

	}
}

// to make Division mandate
function mandateDivision(division)
{

	if (!division)
	{
		nlapiLogExecution('DEBUG', '', 'making division mandatory');
		var error = nlapiCreateError('001', 'Please select a Division', false);
		throw error;

	}
}

// to make Provider mandate
function mandateProvider(provider)
{
	if (!provider)
	{
		nlapiLogExecution('DEBUG', '', 'making provider mandatory');
		var error = nlapiCreateError('002', 'Please select a Provider', false);
		throw error;
	}
}

// to make Department mandatory
function mandateDepartment(department)
{
	if (!department)
	{
		nlapiLogExecution('DEBUG', '', 'making division department');
		var error = nlapiCreateError('003', 'Please select a Department', false);
		throw error;
	}
}

function loadAccountDetails()
{
	var accSearch = nlapiLoadSearch(null, 'customsearch_cntm_acc_search');
	var searchResults = accSearch.runSearch();
	var counter = 0;
	var results;
	var resultIndex = 0;
	var resultStep = 1000;
	var resultSet;

	// more than 1000 records
	do
	{
		resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
		if (resultIndex == 0)
			results = resultSet;
		else if (resultSet)
			results = results.concat(resultSet);
		resultIndex = resultIndex + resultStep;
	}
	while (resultSet.length > 0);
	nlapiLogExecution('DEBUG', 'Found A/C Records with Length', results.length);

	var accMap = {};
	for (var line = 0; line < results.length; line++)
	{

		accMap[results[line].getValue(new nlobjSearchColumn("internalid"))] = {
			type : results[line].getValue(new nlobjSearchColumn("type")),
			number : results[line].getValue(new nlobjSearchColumn("number"))
		};
	}
	return accMap;

}
