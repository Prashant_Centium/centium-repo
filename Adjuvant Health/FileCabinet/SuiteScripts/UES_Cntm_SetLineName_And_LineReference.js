/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Jun 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve, cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only) dropship, specialorder, orderitems (PO only) paybills (vendor payments)
 * @returns {Void}
 */
function setLineName_and_reference_afterSubmit(type)
{
	try
	{
		nlapiLogExecution('DEBUG', '', 'Type: ' + type);

		var recordType = nlapiGetRecordType();
		nlapiLogExecution('DEBUG', '', 'RecordType: ' + recordType);

		var recordId = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', '', 'RecordId: ' + recordId);

		var record = nlapiLoadRecord(recordType, recordId);

		var count = record.getLineItemCount('expense');
		
		// JE
		if(recordType.indexOf('journalentry')>-1){
			count = record.getLineItemCount('line');
		}
		nlapiLogExecution('DEBUG', '', 'Count: ' + count);

		var refNum = record.getFieldValue('tranid');
		nlapiLogExecution('DEBUG', '', 'Ref Number: ' + refNum);

		var vendorName = record.getFieldValue('entity');
		nlapiLogExecution('DEBUG', '', 'Vendor Name: ' + vendorName);

		var memo = record.getFieldValue('memo');
		nlapiLogExecution('DEBUG', '', 'Memo: ' + memo);

		for (var linenum = 1; linenum <= count; linenum++)
		{

			if (!record.getLineItemValue('expense', 'custcolvbinvref', linenum) || type == 'copy' || type == 'create')
			{
				record.setLineItemValue('expense', 'custcolvbinvref', linenum, refNum);

			}
			if (!record.getLineItemValue('expense', 'custcolcc_name', linenum) || type == 'copy' || type == 'create')
			{
				record.setLineItemValue('expense', 'custcolcc_name', linenum, vendorName);

			}

			// set line memo when it is empty for Vendor Bill
			if (recordType == 'vendorbill' && memo && !record.getLineItemValue('expense', 'memo', linenum))
			{
				record.setLineItemValue('expense', 'memo', linenum, memo);

			}
			
			// set line memo when it is empty for Journal Entry
			if (recordType.indexOf('journalentry')>-1 && memo && !record.getLineItemValue('line', 'memo', linenum))
			{
				record.setLineItemValue('line', 'memo', linenum, memo);

			}

		}
		nlapiSubmitRecord(record);

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Occurred on Record Id: ' + recordId + ' RecordType: ' + recordType + ' Error Details: ' + e.message);
	}
}
