/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function hide_standardProvider_beforeLoad(type, form, request)
{
	try
	{
		var currentContext = nlapiGetContext().getExecutionContext();
		nlapiLogExecution('DEBUG', '', 'Current Context: ' + currentContext);

		if(currentContext == 'userinterface'){
			// Only for Invoice
			if (nlapiGetLineItemField('item', 'location') && nlapiGetRecordType()=='invoice')
			{
				nlapiGetLineItemField('item', 'location').setDisplayType('hidden');

			}
			if (nlapiGetLineItemField('expense', 'location'))
			{
				nlapiGetLineItemField('expense', 'location').setDisplayType('hidden');

			}
			if (nlapiGetLineItemField('line', 'location'))
			{
				nlapiGetLineItemField('line', 'location').setDisplayType('hidden');

			}
		}
		

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);

	}

}
/*
 * function hide_standardProvider_syncData_afterSubmit(type) { try { var recordId = nlapiGetRecordId(); var recordType = nlapiGetRecordType(); var record = nlapiLoadRecord(recordType, recordId); var count = record.getLineItemCount('item');
 * 
 * for (var linenum = 1; linenum <= count; line++) { record.setLineItemValue('item', 'custcol_cntm_custom_provider', linenum, record.getLineItemValue('item', 'location', linenum));
 *  } nlapiSubmitRecord(record);
 *  } catch (e) { nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message + ' Record Id: ' + recordId + ' Record Type: ' + recordType);
 *  }
 *  }
 */
