/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Nov 2019     Prashant Kumar
 *
 */

/**
 * @returns {Void} Any or no return value
 */
var sender=nlapiGetContext().getSetting('SCRIPT','custscript_cntm_sender');
var cc=nlapiGetContext().getSetting('SCRIPT','custscript_cntm_cc');
var template=nlapiGetContext().getSetting('SCRIPT','custscript_cntm_template_id');
var duedays=nlapiGetContext().getSetting('SCRIPT','custscript_cntm_duedays');
var recordtype=nlapiGetContext().getSetting('SCRIPT','custscript_cntm_type');
function mail_template(id,form,type) {
	
	try{

		var salesrep=nlapiGetFieldValue('salesrep');
		var ccArr=new Array();
		ccArr.push(cc);
		if(salesrep){//Sales Rep Email Id
			ccArr.push(nlapiLookupField('employee', salesrep, 'email'));
		}
		var recipient = nlapiGetFieldValue('entity');// Customer Record Id
		if(recipient){//CC mail Ids
			if(recordtype=='T')
				recipient=nlapiLookupField('customer', recipient, 'custentity_cntm_billing_email_customer');// Billing Email Id on Customer
			else{
				recipient=nlapiGetFieldValue('partner');
				recipient=nlapiLookupField('partner', recipient, 'custentity_cntm_billing_email_partner');// Billing Email Id on Partner

			}

		}
		var attachments=new Array();
		attachments.push(generateTemplate(template, nlapiGetRecordId()));
		attachments.push(nlapiPrintRecord('TRANSACTION',nlapiGetRecordId(),'PDF',null));
		nlapiLogExecution('DEBUG','--','Recipient: '+recipient+', Sender: '+sender+", CC: "+JSON.stringify(ccArr)+", Template Id:  "+template+", DueDays: "+duedays+", Is Customer: "+recordtype);
		//nlapiSendEmail(sender, recipient, "Invoice "+duedays+" Days Overdue","The attached invoice is "+duedays+" days overdue. ",ccArr,null,null,attachments);
		nlapiSendEmail(sender, recipient, "Invoice "+duedays+" Days Overdue","Dear "+nlapiGetFieldText('entity')+" ,\n\n We have not received payment for the PAST DUE Digital Guardian Invoice(s) attached.",ccArr,null,null,attachments,true,null,null);

	}catch(e){
		nlapiLogExecution('ERROR','',e.message);

	}

	
}

function generateTemplate(templateId,recordId){
	var renderer = nlapiCreateTemplateRenderer(); // initiate nlapiCreateTemplateRenderer()
	renderer.setTemplate(nlapiLoadFile(templateId).getValue()); // set the template
	renderer.addRecord('record', nlapiLoadRecord('invoice',recordId)); // add the record that we loaded
	var xmlsample = renderer.renderToString(); // convert to xml
	var pdf=nlapiXMLToPDF(xmlsample); // convert to pdf
	return pdf;
}

