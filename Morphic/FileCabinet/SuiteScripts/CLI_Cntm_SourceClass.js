/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Apr 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @param {String}
 *            name Field internal id
 * @returns {Void}
 * 
 * Set the Default class on line level when project/customer/vendor/Employee name is selected or A/C. 
 * project/customer/vendor/name is loaded from custom record.which has project name with their defined class.
 */

// Non Expense Parameter
var non_expns_param;
// Expense Parameter
var expns_param;
// Department parameter to make class default
var departmentId;
// Record Type
var recordType;
// Category Id for Prepaid Expenses
var categoryId;

/*
 * // allocation Parameter var _isfromexpensealloc; // amortization Parameter var _isfromamortization; // reversal Parameter var _isreversal;
 */

function cli_cntm_sourceClass_pageInit(type)
{
	nlapiLogExecution('DEBUG', '', 'In PageInit');

	// Setting Non Expense Parameter
	non_expns_param = nlapiGetContext().getSetting('Script', 'custscript_cntm_nonexpense_acc_def_class');
	nlapiLogExecution('DEBUG', 'Non Expense Parameter', non_expns_param);

	// Setting Expense Parameter
	expns_param = nlapiGetContext().getSetting('Script', 'custscript_cntm_expense_acc_def_class');
	nlapiLogExecution('DEBUG', 'Expense Parameter', expns_param);

	categoryId = nlapiGetContext().getSetting('Script', 'custscript_cntm_cat_id');
	nlapiLogExecution('DEBUG', 'Category Id Parameter', categoryId);

	// Setting Department Parameter
	departmentId = nlapiGetContext().getSetting('Script', 'custscript_cntm_dept_def_class');
	departmentId = departmentId.split(',');

	recordType = nlapiGetRecordType();

	nlapiLogExecution('DEBUG', 'Deparment Id to set default class', departmentId);

	/*
	 * _isfromexpensealloc = nlapiGetFieldValue('isfromexpensealloc'); nlapiLogExecution('DEBUG', '', '_isfromexpensealloc: ' + _isfromexpensealloc);
	 * 
	 * _isfromamortization = nlapiGetFieldValue('isfromamortization'); nlapiLogExecution('DEBUG', '', '_isfromamortization: ' + _isfromamortization);
	 * 
	 * _isreversal = nlapiGetFieldValue('isreversal'); nlapiLogExecution('DEBUG', '', '_isreversal: ' + _isreversal);
	 * 
	 */// var bodyLevelClass = nlapiGetFieldValue('class');
	// nlapiLogExecution('DEBUG', 'bodyLevelClass', bodyLevelClass);
	// Set Default Class on Body Level for records except JE
	if (nlapiGetRecordType().search('journal') < 0)
	{
		nlapiLogExecution('DEBUG', '', 'Setting body class field');
		nlapiSetFieldValue('class', non_expns_param);
	}

}
function cli_cntm_sourceClass_fieldChanged(type, name)
{
	nlapiLogExecution('DEBUG', '', 'In FieldChanged');
	try
	{

		nlapiLogExecution('DEBUG', '', 'recordType: ' + recordType + '; LineType: ' + type + '; FieldChanged: ' + name);

		// A/C No.
		var accnum;

		// A/C Type
		var acctype;

		// For Department on Line
		var lineDepartmentId;

		var projectClass;
		// recordType: VendorBill, VendorCredit, ExpenseReport, Check, Journal, Deposit
		// if ((name == 'entity' && recordType.search('journalentry') > -1 && (_isfromexpensealloc == 'F' && _isfromamortization == 'F' && _isreversal == 'F')) || (name == 'customer') || (name == 'entity' && recordType == 'deposit'))
		if (name == 'entity' || name == 'customer')
		{

			if (recordType != 'expensereport')
			{
				accnum = nlapiGetCurrentLineItemValue(type, 'account');
				nlapiLogExecution('DEBUG', 'A/C No.', accnum);

				// A/C Type
				acctype = nlapiLookupField('account', accnum, 'type', true);
				nlapiLogExecution('DEBUG', 'A/C Type', acctype);
			}

			lineDepartmentId = nlapiGetCurrentLineItemValue(type, 'department');
			nlapiLogExecution('DEBUG', '', 'Department Id on Line: ' + lineDepartmentId);

			// Check if A/C Type is Expense
			if ((departmentId.indexOf(lineDepartmentId) < 0) && (recordType == 'expensereport' || acctype.toLowerCase() == 'expense'))
			{
				// Entity can be Project/Customer/Vendor/Employee
				var entity = nlapiGetCurrentLineItemValue(type, name);
				nlapiLogExecution('DEBUG', 'Entity', entity);

				// When Entity is not empty
				if (!(entity == '' || entity == null || entity == undefined))
				{
					// Load Class based on Project/Customer/Employee/Vendor name
					projectClass = loadClassBasedOnEntity(entity);
					nlapiLogExecution('DEBUG', 'Class', projectClass);

					// When project class is found.
					if (projectClass)
					{
						nlapiSetCurrentLineItemValue(type, 'class', projectClass);

					}
					else
					{
						nlapiSetCurrentLineItemValue(type, 'class', expns_param);// default class

					}
				}
				else
				// Entity is Empty
				{
					nlapiSetCurrentLineItemValue(type, 'class', expns_param);// default class
				}
			}
			else
			// For Non Expense A/C.
			{
				nlapiSetCurrentLineItemValue(type, 'class', non_expns_param);// default class
			}

		}// default class on A/C select
		// else if ((name == 'account' && recordType.search('journalentry') > -1 && (_isfromexpensealloc == 'F' && _isfromamortization == 'F' && _isreversal == 'F')) || (name == 'account' && recordType.search('journalentry') < 0))
		else if (name == 'account')

		{
			accnum = nlapiGetCurrentLineItemValue(type, 'account');
			nlapiLogExecution('DEBUG', 'A/C No.', accnum);

			lineDepartmentId = nlapiGetCurrentLineItemValue(type, 'department');
			nlapiLogExecution('DEBUG', '', 'Department Id on Line: ' + lineDepartmentId);

			// A/C Type
			acctype = nlapiLookupField('account', accnum, 'type', true);
			nlapiLogExecution('DEBUG', 'A/C Type', acctype);
			// For Expense
			if ((departmentId.indexOf(lineDepartmentId) < 0) && (acctype.toLowerCase() == 'expense'))
			{
				// Entity can be Project/Customer/Vendor/Employee
				if (recordType == 'vendorbill' || recordType == 'check' || recordType == 'expensereport' || recordType == 'vendorcredit')
				{
					entity = nlapiGetCurrentLineItemValue(type, 'customer');

				}
				else
				{
					entity = nlapiGetCurrentLineItemValue(type, 'entity');

				}

				nlapiLogExecution('DEBUG', 'Entity', entity);

				// entity is empty
				if (!entity)
				{
					nlapiSetCurrentLineItemValue(type, 'class', expns_param);// default class
				}
				else
				{
					// Load Class based on Project/Customer/Employee/Vendor name
					projectClass = loadClassBasedOnEntity(entity);
					nlapiLogExecution('DEBUG', 'Class', projectClass);

					// When project class is found.
					if (projectClass)
					{
						nlapiSetCurrentLineItemValue(type, 'class', projectClass);

					}
					else
					{
						nlapiSetCurrentLineItemValue(type, 'class', expns_param);// default class

					}
				}
			}
			else
			{
				// For Non Expense
				nlapiSetCurrentLineItemValue(type, 'class', non_expns_param);// set default

			}

		}
		else if (name == 'department')
		{

			lineDepartmentId = nlapiGetCurrentLineItemValue(type, 'department');
			nlapiLogExecution('DEBUG', '', 'Department Id on Line: ' + lineDepartmentId);

			// Check if department is equal to default class department
			if (departmentId.indexOf(lineDepartmentId) > -1)
			{
				nlapiLogExecution('DEBUG', '', 'Setting default class..');
				nlapiSetCurrentLineItemValue(type, 'class', non_expns_param);// set default

			}
			else
			{
				if (recordType != 'expensereport')
				{
					accnum = nlapiGetCurrentLineItemValue(type, 'account');
					nlapiLogExecution('DEBUG', 'A/C No.', accnum);

					// A/C Type
					acctype = nlapiLookupField('account', accnum, 'type', true);
					nlapiLogExecution('DEBUG', 'A/C Type', acctype);
				}

				if (acctype == 'expense' || (recordType == 'expensereport' && nlapiGetCurrentLineItemValue('expense', 'category') != categoryId))
				{
					var project;
					if (recordType == 'vendorbill' || recordType == 'check' || recordType == 'expensereport' || recordType == 'vendorcredit')
					{
						project = nlapiGetCurrentLineItemValue(type, 'customer');

					}
					else
					{
						project = nlapiGetCurrentLineItemValue(type, 'entity');

					}

					if (project)
					{
						// Load Class based on Project/Customer/Employee/Vendor name
						nlapiLogExecution('DEBUG', 'Entity', project);

						projectClass = loadClassBasedOnEntity(project);
						nlapiLogExecution('DEBUG', 'Entity Class', projectClass);

						// When project class is found.
						if (projectClass)
						{
							nlapiSetCurrentLineItemValue(type, 'class', projectClass);

						}
						else
						{
							nlapiSetCurrentLineItemValue(type, 'class', expns_param);// default class

						}

					}
					else
					{
						nlapiSetCurrentLineItemValue(type, 'class', expns_param);// default class

					}
				}
				else
				{

					nlapiSetCurrentLineItemValue(type, 'class', non_expns_param);// default class

				}
			}

		}
		/*
		 * // For Expense Report Category change else if (recordType == 'expensereport' && name == 'category') { if (nlapiGetCurrentLineItemValue('expense', 'category') == categoryId) { nlapiSetCurrentLineItemValue(type, 'class', non_expns_param);// default class
		 *  } else { var project = nlapiGetCurrentLineItemValue(type, 'customer');
		 * 
		 * if (project) { // Load Class based on Project/Customer/Employee/Vendor name nlapiLogExecution('DEBUG', 'Entity', project);
		 * 
		 * projectClass = loadClassBasedOnEntity(project); nlapiLogExecution('DEBUG', 'Entity Class', projectClass);
		 *  // When project class is found. if (projectClass) { nlapiSetCurrentLineItemValue(type, 'class', projectClass);
		 *  } else { nlapiSetCurrentLineItemValue(type, 'class', expns_param);// default class
		 *  }
		 *  }
		 *  } }
		 */
	}

	catch (e)
	{
		nlapiLogExecution('ERROR', 'Error Occured..', e.message);
	}
}

// Load Class Based On Entity
function loadClassBasedOnEntity(entity)
{
	// Search the class for specific entity
	var searchRecord = nlapiSearchRecord("customrecord_cntm_default_class_field_ma", null, [ [ "custrecord_cntm_project_name", "anyof", entity ] ], [ new nlobjSearchColumn("scriptid").setSort(false), new nlobjSearchColumn("custrecord_cntm_project_name"), new nlobjSearchColumn("custrecord_cntm_class") ]);

	if (searchRecord)
	{
		return searchRecord[0].getValue(new nlobjSearchColumn("custrecord_cntm_class"));

	}

	return null;
}
