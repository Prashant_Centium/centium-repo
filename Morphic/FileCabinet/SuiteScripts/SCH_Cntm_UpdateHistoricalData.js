/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Apr 2019     Prashant Kumar
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void} Script to upadate historical data based on saved search criteria
 */
function sch_cntm_updateHistoricalData(type)
{
	try
	{
		nlapiLogExecution('DEBUG', '', '* Execution Started *');

		// return map of internal ids of JE and VBill
		var historicalData = loadSearch();

		// Iterate through each internal id
		for (var recordCount = 0; recordCount < historicalData.length; recordCount++)
		{
			nlapiLogExecution('DEBUG', 'Linenum', recordCount);
			var recordId = historicalData[recordCount].getValue(new nlobjSearchColumn("internalid", null, "GROUP"));
			nlapiLogExecution('DEBUG', 'RecordId', recordId);

			var recordType = historicalData[recordCount].getValue(new nlobjSearchColumn("recordtype", null, "GROUP"));
			nlapiLogExecution('DEBUG', 'RecordType', recordType);

			
			// If record is locked
			try
			{	
				var record = nlapiLoadRecord(recordType, recordId);
				nlapiLogExecution('DEBUG', 'Record Loaded', 'record : ' + record);

				nlapiSubmitRecord(record);
				// nlapiLogExecution('DEBUG', '', 'Record Updated : ' + record);
			}
			catch (e)
			{
				nlapiLogExecution('ERROR', '', 'Record Not Updated with Record Id: ' + recordId + ', Record Type: ' + recordType + ' Error : ' + e + ' Error Details: ' + e.message);
				continue;
				// if (e.code == 'INSUFFICIENT_PERMISSION')
				// {
				// nlapiLogExecution('ERROR', 'Record NOT Submitted', 'Exception : ' + e.message);
				// continue;
				// }
			}

			nlapiLogExecution('DEBUG', '', 'Record Submitted');

			// To yield script
			var remain = nlapiGetContext().getRemainingUsage();
			nlapiLogExecution('DEBUG', '1 Usage Remaining while Record Loading..', remain);

			if (remain < 500)
			{
				// ...yield the script
				var state = nlapiYieldScript();
				// Throw an error or log the yield results
				if (state.status == 'FAILURE')
					throw "Failed to yield script";
				else if (state.status == 'RESUME')
					nlapiLogExecution('DEBUG', 'Resuming script');
			}
		}
	}
	catch (e1)
	{
		nlapiLogExecution('ERROR', '', 'Error Occurred with Record Id:' + recordId + ', Record Type: ' + recordType + ' with Error code: ' + e1 + 'Error Details: ' + e1.message);
	}

	nlapiLogExecution('DEBUG', '', '* Execution Completed *');
}

// This Loads the saved search for historical data based on criteria. It contains internal ids or JE and VBill
function loadSearch()
{
	var savedSearchObj = nlapiLoadSearch(null, 'customsearch_cntm_tran_search_je_and_vb');
	var searchResults = savedSearchObj.runSearch();
	var counter = 0;
	var results;
	var resultIndex = 0;
	var resultStep = 1000;
	var resultSet;

	// gather data
	do
	{
		resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
		if (resultIndex == 0)
		{
			results = resultSet;
		}
		else if (resultSet)
		{
			results = results.concat(resultSet);
		}
		resultIndex = resultIndex + resultStep;

		// To yield script
		var remain = nlapiGetContext().getRemainingUsage();
		nlapiLogExecution('DEBUG', '2 Usage Remaining', remain);
		if (remain < 100)
		{
			// ...yield the script
			var state = nlapiYieldScript();
			// Throw an error or log the yield results
			if (state.status == 'FAILURE')
				throw "Failed to yield script";
			else if (state.status == 'RESUME')
				nlapiLogExecution('DEBUG', 'Resuming script');
		}
	}
	while (resultSet.length > 0);
	{
		nlapiLogExecution('DEBUG', 'Length', results.length);
	}

	return results;
}
