/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Apr 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve, cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only) dropship, specialorder, orderitems (PO only) paybills (vendor payments)
 * @returns {Void} Set the Default class on line level on the basis of project/customer/vendor/Employee name project/customer/vendor/name is loaded from custom record.which has project name with their defined class.
 */
// Load custom record class map
// This map contains entity as a key and class as a value
var customRecordMap;
// This map contains accnum as a key and acctype as a value
var accTypeMap;

// Non Expense Parameter
var non_expns_param = nlapiGetContext().getSetting('Script', 'custscript_cntm_nonexpense_acc_def_cls_u');

// Expense Parameter
var expns_param = nlapiGetContext().getSetting('Script', 'custscript_cntm_expense_acc_def_cls_u');

// Department to make class default to N/A
var departmentId = nlapiGetContext().getSetting('Script', 'custscript_cntm_dept_def_cls_u');
departmentId = departmentId.split(',');

// Category Id for Prepaid Expenses
var categoryId = nlapiGetContext().getSetting('Script', 'custscript_cntm_cat_id_u');
var recordType;

function ues_cntm_sourceDefaultClass_afterSubmit(type)
{
	try
	{
		nlapiLogExecution('DEBUG', '', '* AfterSubmit Starts *');
		nlapiLogExecution('DEBUG', 'Non Expense Parameter', non_expns_param);
		nlapiLogExecution('DEBUG', 'Expense Parameter', expns_param);
		nlapiLogExecution('DEBUG', 'Deparment Id Parameter', departmentId);
		nlapiLogExecution('DEBUG', 'Category Id Parameter', categoryId);

		// Load record Type
		recordType = nlapiGetRecordType();
		nlapiLogExecution('DEBUG', 'RecordType', recordType);

		// Load record
		var record = nlapiLoadRecord(recordType, nlapiGetRecordId());

		var _isfromexpensealloc = record.getFieldValue('isfromexpensealloc');
		nlapiLogExecution('DEBUG', '', '_isfromexpensealloc: ' + _isfromexpensealloc);

		var _isfromamortization = record.getFieldValue('isfromamortization');
		nlapiLogExecution('DEBUG', '', '_isfromamortization: ' + _isfromamortization);

		var _isreversal = record.getFieldValue('isreversal');
		nlapiLogExecution('DEBUG', '', '_isreversal: ' + _isreversal);

		// For Check,Expense Report,VendorBill,Vendor Credit
		if (recordType == 'vendorbill' || recordType == 'check' || recordType == 'expensereport' || recordType == 'vendorcredit')
		{
			customRecordMap = loadCustomRecordMap();
			accTypeMap = loadAccType();
			updateLines('expense', record, 'customer');
		}
		// For Deposit
		else if (recordType == 'deposit')
		{
			customRecordMap = loadCustomRecordMap();
			accTypeMap = loadAccType();
			updateLines('other', record, 'entity');

		}
		// Vendor Payment
		else if (recordType == 'vendorpayment')
		{
			nlapiLogExecution('DEBUG', '', 'No lines in Vendor Payment with Class');
			nlapiLogExecution('DEBUG', '', 'Setting body class field');
			record.setFieldValue('class', non_expns_param);
			nlapiSubmitRecord(record);
		}

		// For JournalEntry
		else
		{
			if (_isfromexpensealloc == 'F' && _isfromamortization == 'F' && _isreversal == 'F') // If Journal entry is met with this condition
			{
				nlapiLogExecution('DEBUG', '', 'Normal JE');
				customRecordMap = loadCustomRecordMap();
				accTypeMap = loadAccType();
				updateLines('line', record, 'entity');
			}
			else if (_isfromamortization == 'T')
			{

				nlapiLogExecution('DEBUG', '', 'Amortization JE');
				nlapiLogExecution('DEBUG', '', 'Remaining Usage before Function Execution: ' + nlapiGetContext().getRemainingUsage());
				updateProjectName('line', record);
			}
			else
			{
				nlapiLogExecution('DEBUG', 'JE NOT Processed!', '* Journal Entry is created from either Expense Allocation or Reversal nor Amortization*');
			}
		}
		nlapiLogExecution('DEBUG', '', '* AfterSubmit Ends *');
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Record Not Updated with Record Id: ' + nlapiGetRecordId() + ', Record Type: ' + recordType + ' Error : ' + e + ' Error Details: ' + e.message);

	}

}

// Update lines on JE and VBill
function updateLines(lineType, record, entityType)
{
	nlapiLogExecution('DEBUG', 'UpdateLines Called with map Data', JSON.stringify(customRecordMap));
	nlapiLogExecution('DEBUG', 'Expense AccType Map', JSON.stringify(accTypeMap));

	var projectClass;

	if (lineType != 'line')
	{// if not a Journal
		nlapiLogExecution('DEBUG', '', 'Setting body class field');
		record.setFieldValue('class', non_expns_param);
	}

	nlapiLogExecution('DEBUG', 'lineCount', record.getLineItemCount(lineType));
	nlapiLogExecution('DEBUG', 'linetype', lineType);

	// Iterate through each line to set default class
	for (var linenum = 1; linenum <= record.getLineItemCount(lineType); linenum++)
	{
		nlapiLogExecution('DEBUG', 'Linenum', linenum);

		var accnum = record.getLineItemValue(lineType, 'account', linenum);
		nlapiLogExecution('DEBUG', 'A/C No.', accnum);

		projectClass = record.getLineItemValue(lineType, 'class', linenum);
		nlapiLogExecution('DEBUG', 'Class Initially', projectClass);

		// For Department on Line
		var lineDepartmentId = record.getLineItemValue(lineType, 'department', linenum);
		nlapiLogExecution('DEBUG', '', 'Department Id on Line: ' + lineDepartmentId);

		// Set N/A as class if department is N/A or G&A
		if (departmentId.indexOf(lineDepartmentId) > -1)
		{
			record.setLineItemValue(lineType, 'class', linenum, non_expns_param);// for default

		}
		// For expense A/C type
		// For Expense report category Id is hardcoded
		else if ((recordType == 'expensereport' && record.getLineItemValue(lineType, 'category', linenum) != categoryId) || accTypeMap[accnum])
		{
			var entity = record.getLineItemValue(lineType, entityType, linenum);
			nlapiLogExecution('DEBUG', 'Entity', entity);

			// Entity has value
			if (entity)
			{
				nlapiLogExecution('DEBUG', 'Entity--Class in Map', entity + '--' + customRecordMap[entity]);

				// If entity has class in custom record map
				if (entity in customRecordMap)
				{
					nlapiLogExecution('DEBUG', 'Class exists in Custom Record for Entity', 'Setting class for expense A/C');
					nlapiLogExecution('DEBUG', 'customRecordMap.Entity', 'Setting class for expense A/C with value ' + customRecordMap[entity]);
					record.setLineItemValue(lineType, 'class', linenum, customRecordMap[entity]);
				}
				// If class not found in record map
				else
				{
					nlapiLogExecution('DEBUG', '', 'Setting default class for expense A/C');
					record.setLineItemValue(lineType, 'class', linenum, expns_param);

				}
			}
			else
			// Entity is empty
			{

				nlapiLogExecution('DEBUG', '', 'Setting default class for expense A/C');
				record.setLineItemValue(lineType, 'class', linenum, expns_param);

			}

		}
		else
		// For non-expense A/C type
		{
			nlapiLogExecution('DEBUG', '', 'Setting default class for nonexpense A/C');
			record.setLineItemValue(lineType, 'class', linenum, non_expns_param);// for default
		}

	}
	nlapiSubmitRecord(record);
}

// Load Saved Search of custom records to get class for defined entity
function loadCustomRecordMap()
{
	var savedSearchObj = nlapiLoadSearch('customrecord_cntm_default_class_field_ma', 'customsearch_cntm_class_field_mapping_s');
	var searchResults = savedSearchObj.runSearch();
	var counter = 0;
	var results;
	var resultIndex = 0;
	var resultStep = 1000;
	var resultSet;

	// more than 1000 records
	do
	{
		resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
		if (resultIndex == 0)
			results = resultSet;
		else if (resultSet)
			results = results.concat(resultSet);
		resultIndex = resultIndex + resultStep;
	}
	while (resultSet.length > 0);
	nlapiLogExecution('DEBUG', 'Found Custom Records with Length', results.length);

	var map = {};
	for (var line = 0; line < results.length; line++)
	{

		map[results[line].getValue(new nlobjSearchColumn("custrecord_cntm_project_name"))] = results[line].getValue(new nlobjSearchColumn("custrecord_cntm_class"));
	}
	return map;
}

function loadAccType()
{
	var accSearch = nlapiLoadSearch(null, 'customsearch_cntm_acc_search');
	var searchResults = accSearch.runSearch();
	var counter = 0;
	var results;
	var resultIndex = 0;
	var resultStep = 1000;
	var resultSet;

	// more than 1000 records
	do
	{
		resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
		if (resultIndex == 0)
			results = resultSet;
		else if (resultSet)
			results = results.concat(resultSet);
		resultIndex = resultIndex + resultStep;
	}
	while (resultSet.length > 0);
	nlapiLogExecution('DEBUG', 'Found A/C Records with Length', results.length);

	var accMap = {};
	for (var line = 0; line < results.length; line++)
	{

		accMap[results[line].getValue(new nlobjSearchColumn("internalid"))] = results[line].getValue(new nlobjSearchColumn("type"));
	}
	return accMap;

}

// Update Project Name
function updateProjectName(lineType, record)
{
	try
	{ // map to store schedule number with its project name
		var scheduleIdMap = {};

		for (var linenum = 1; linenum <= record.getLineItemCount(lineType); linenum++)
		{

			var scheduleNumId = record.getLineItemValue(lineType, 'schedulenum', linenum);
			nlapiLogExecution('DEBUG', '', 'scheduleNumId: ' + scheduleNumId);

			if (!(scheduleNumId in scheduleIdMap))
			{
				var projectNameId = source_projectId(scheduleNumId);
				nlapiLogExecution('DEBUG', '', 'projectNameId: ' + projectNameId);

				scheduleIdMap[scheduleNumId] = projectNameId;
			}

			if (scheduleIdMap[scheduleNumId])
			{
				nlapiLogExecution('DEBUG', '', 'Project Name is not empty ' + 'LineType: ' + lineType + ' ;Linenum: ' + linenum);

				record.setLineItemValue(lineType, 'entity', linenum, scheduleIdMap[scheduleNumId]);
			}
			else
			{
				nlapiLogExecution('DEBUG', '', 'Project Name is empty');

				record.setLineItemValue(lineType, 'entity', linenum, '');

			}
			nlapiLogExecution('DEBUG', '', 'Remaining Usage: ' + nlapiGetContext().getRemainingUsage());

			if ((nlapiGetContext().getRemainingUsage()) < 50)
			{
				break;
			}

		}
		nlapiSubmitRecord(record);
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Occurred on Record Id: ' + nlapiGetRecordId() + ', Record Type: ' + nlapiGetRecordType() + ' Error : ' + e + ' Error Details: ' + e.message);
	}
}

function source_projectId(schId)
{
	// Saved Search on transaction to fetch project Id
	var transactionSearch = nlapiSearchRecord("transaction", null, [ [ "type", "anyof", "VendBill", "Journal" ], "AND", [ "amortizationschedule.internalid", "anyof", schId ] ], [ new nlobjSearchColumn("internalid", "amortizationSchedule", null), new nlobjSearchColumn("entityid", "customer", null), new nlobjSearchColumn("internalid", "customer", null) ]);

	if (transactionSearch)
	{
		return transactionSearch[0].getValue(new nlobjSearchColumn("internalid", "customer", null));

	}

	nlapiLogExecution('DEBUG', '', 'Amortization Scheduled Record does not exist');
	return '';

}
