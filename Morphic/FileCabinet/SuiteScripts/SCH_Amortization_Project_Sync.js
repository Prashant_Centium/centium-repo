/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve, cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only) dropship, specialorder, orderitems (PO only) paybills (vendor payments)
 * @returns {Void}
 */
function amort_project_sync(type)
{
	try
	{
		nlapiLogExecution('DEBUG', '', '* Script Starts *');

		var recordIds = loadAmortScheduledRecords();
		var schId;
		var vBillId;
		for (var linenum = 0; linenum < recordIds.length; linenum++)
		{
			nlapiLogExecution('DEBUG', '', 'Linenum: ' + linenum);
			schId = recordIds[linenum].getValue(new nlobjSearchColumn('internalid'));
			nlapiLogExecution('DEBUG', '', 'Amortization Scheduled Id: ' + schId);

			vBillId = recordIds[linenum].getValue(new nlobjSearchColumn('srctran'));
			nlapiLogExecution('DEBUG', '', 'VBillId: ' + vBillId);

			var projectId = source_projectId(vBillId, schId);
			nlapiLogExecution('DEBUG', '', 'Project ID: ' + projectId);
			
			var record=nlapiLoadRecord('reverevrecschedule', schId);
			nlapiLogExecution('DEBUG', '', 'Amortization Record: ' +record);


			if (projectId)
			{
				nlapiLogExecution('DEBUG', '', 'Project Id Found.');
				// record.setFieldValue('job',projectId);
			}
			else
			{
				nlapiLogExecution('DEBUG', '', 'Project Id Not Found.');
				// record.setFieldValue('job','');
			}
		}
		//nlapiSubmitRecord(record);
		nlapiLogExecution('DEBUG', '', '* Script Ends *');

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', ' Error : ' + e + ' Error Details: ' + e.message);
	}

}

function source_projectId(vBillId, schId)
{
	var vendorbillSearch = nlapiSearchRecord("vendorbill", null, [ [ "type", "anyof", "VendBill" ], "AND", [ "internalid", "anyof", vBillId ], "AND", [ "amortizationschedule.internalid", "anyof", schId ] ], [ new nlobjSearchColumn("internalid", "amortizationSchedule", null), new nlobjSearchColumn("internalid", "job", null) ]);

	var projectId = vendorbillSearch[0].getValue(new nlobjSearchColumn("internalid", "job", null));

	return projectId;

}

function loadAmortScheduledRecords()
{
	var savedSearchObj = nlapiLoadSearch(null, 'customsearch_cntm_amort_schedule_ids');
	var searchResults = savedSearchObj.runSearch();
	var counter = 0;
	var results;
	var resultIndex = 0;
	var resultStep = 1000;
	var resultSet;

	// more than 1000 records
	do
	{
		resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
		if (resultIndex == 0)
			results = resultSet;
		else if (resultSet)
			results = results.concat(resultSet);
		resultIndex = resultIndex + resultStep;
	}
	while (resultSet.length > 0);
	nlapiLogExecution('DEBUG', 'Found Custom Records with Length', results.length);

	return results;
}
