/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function amort_project_sync_afterSubmit(type){
	try
	{ 
		nlapiLogExecution('DEBUG', '', '* Script Starts *');

		var record=nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
		nlapiLogExecution('DEBUG', '', record);
		
		var vbId=record.getFieldValue('sourcetran');
		nlapiLogExecution('DEBUG', '', 'VBillId: '+vbId);

		var schNumId=record.getFieldValue('schedulenumber');
		nlapiLogExecution('DEBUG', '', 'Scheduled Number Id: '+schNumId);

		var projectId=source_projectId(vbId,schNumId);
		nlapiLogExecution('DEBUG', '', 'Project ID: '+projectId);

		if(!projectId){
			nlapiLogExecution('DEBUG', '', 'Project Id Found.');
			//record.setFieldValue('job',projectId);
		}
		else{
			nlapiLogExecution('DEBUG', '', 'Project Id Not Found.');
			//record.setFieldValue('job','');	
		}
		
		nlapiLogExecution('DEBUG', '', '* Script Ends *');

			}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Occurred on Record Id: ' + nlapiGetRecordId() + ', Record Type: ' + nlapiGetRecordType() + ' Error : ' + e + ' Error Details: ' + e.message);
	}

}

function source_projectId(){
	var vendorbillSearch = nlapiSearchRecord("vendorbill",null,
			[
			   ["type","anyof","VendBill"], 
			   "AND", 
			   ["internalid","anyof",vBillId], 
			   "AND", 
			   ["amortizationschedule.internalid","anyof",schId]
			], 
			[
			   new nlobjSearchColumn("internalid","amortizationSchedule",null), 
			   new nlobjSearchColumn("internalid","job",null)
			]
			);
	
	var projectId=vendorbillSearch[0].getValue( new nlobjSearchColumn("internalid","job",null));
	
	return projectId;

}