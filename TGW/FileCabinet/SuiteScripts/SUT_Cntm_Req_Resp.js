/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Aug 2019     Prashant Kumar
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
var context = nlapiGetContext();

// Credentials
var credentials = {
	api_key : context.getSetting('SCRIPT', 'custscript_cntm_api_key'),
	code : context.getSetting('SCRIPT', 'custscript_cntm_code')
};

var singleImportUrl = context.getSetting('SCRIPT', 'custscript_cntm_single_order_url');
var bulkImportUrl = context.getSetting('SCRIPT', 'custscript_cntm_bulk_orders_url');
var exportActivityUrl = context.getSetting('SCRIPT', 'custscript_cntm_export_activity_url');

function req_resp_suitelet(request, response)
{

	try
	{
		var form = nlapiCreateForm('Search Order');
		var dateField = form.addField('custpage_date', 'date', 'Date', null, null);
		dateField.setMandatory(true);
		dateField.setDefaultValue(new Date());
		var orderIdField = form.addField('custpage_orderid', 'text', 'Enter Order Id', null, null);
		var responseField = form.addField('custpage_response', 'textarea', 'Response', null, null);
		form.addSubmitButton('Submit');
		if (request.getMethod() == 'POST')
		{
			var date = request.getParameter('custpage_date');

			var parameters = {};
			nlapiLogExecution('DEBUG', '', 'Date: ' + date);

			var orderId = request.getParameter('custpage_orderid');

			nlapiLogExecution('DEBUG', '', 'Order Id: ' + orderId);

			parameters.service_order_id = orderId;
			parameters.date = date;
			
			var data=serviceOrders();
			nlapiLogExecution('AUDIT', '', 'Data: ' + data);
			
			responseField.setDefaultValue(exportXML(credentials, parameters));//importSingleOrder(credentials, data)
			dateField.setDefaultValue(date);
			orderIdField.setDefaultValue(orderId);
		}
		response.writePage(form);
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);
	}
}

// Export Activity from Dispatch Track
function exportXML(credentials, parameters)
{
	var postData = {
		api_key : credentials.api_key,
		code : credentials.code,
		date : parameters.date,
		account : parameters.account,
		service_order_id : parameters.service_order_id,
		schedule_date : parameters.schedule_date,
		service_unit : parameters.service_unit,
		status : parameters.status,
		service_type : parameters.service_type
	}
	var resp = nlapiRequestURL(exportActivityUrl, postData, null, null, 'POST');

	// nlapiLogExecution('DEBUG', '', 'XML Body: ' + resp.body);
	var xml = nlapiStringToXML(resp.body);
	return JSON.stringify(xmlToJson(xml));
}

// Import Bulk Orders
// It will be executed at once per day. If try to execute more than once in a day then It will replace the same day order with new one.
function importBulkOrders(credentials, data)
{
	var postData = {
		api_key : credentials.api_key,
		code : credentials.code,
		data : data
	}
	var resp = nlapiRequestURL(bulkImportUrl, postData, null, null, 'POST');
	return JSON.stringify(resp.body);
}

// Import Single Order
// This can be used to add more than one order at a day.
function importSingleOrder(credentials, data)
{
	var postData = {
		api_key : credentials.api_key,
		code : credentials.code,
		data : data
	}
	var resp = nlapiRequestURL(singleImportUrl, postData, null, null, 'POST');
	return JSON.stringify(resp.body);
}

// 
function dateFormatter(date)
{
	var cDate = date.getDate();
	var cMonth = date.getMonth();
	var cYear = date.getFullYear();

	return cYear + '-' + cmonth + 1 + '-' + cDate;

}

function xmlToJson(xml)
{
	// Create the return object
	var obj = {};

	if (xml.nodeType == 1)
	{ // element
		// do attributes
		if (xml.attributes.length > 0)
		{
			obj["@attributes"] = {};
			for (var j = 0; j < xml.attributes.length; j++)
			{
				var attribute = xml.attributes.item(j);
				obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
			}
		}
	}
	else if (xml.nodeType == 3)
	{ // text
		obj = xml.nodeValue;
	}

	// do children
	if (xml.hasChildNodes())
	{
		for (var i = 0; i < xml.childNodes.length; i++)
		{
			var item = xml.childNodes.item(i);
			var nodeName = item.nodeName;
			if (typeof (obj[nodeName]) == "undefined")
			{
					obj[nodeName] = xmlToJson(item);

			}
			else
			{
				if (typeof (obj[nodeName].push) == "undefined")
				{
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}
	return obj;
};

function data(){
	
}
