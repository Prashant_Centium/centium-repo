/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function ues_cntm_add_cf_field_afterSubmit(type, form, request){
	var poIds=nlapiGetFieldValue('custbody_cntm_po_ids');
	poIds=poIds.split(',');
	var record=nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
	
	for(var linenum=1;linenum<=record.getLineItemCount('item');linenum++){
		var orderLine=record.getLineItemValue('item', 'orderline', linenum);
		for(var po_linenum=0;po_linenum<poIds.length;po_linenum++){
			var po_record=nlapiLoadRecord('purchase', id, initializeValues)
		}
	}
}

function fetchLineNum(poIds){
	var purchaseorderSearch = nlapiSearchRecord("purchaseorder",null,
			[
			   ["type","anyof","PurchOrd"], 
			   "AND", 
			   ["linesequencenumber","greaterthan","0"],
			   "AND", 
			   ["internalid","is",poIds]
			], 
			[
			   new nlobjSearchColumn("item"), 
			   new nlobjSearchColumn("linesequencenumber"), 
			   new nlobjSearchColumn("internalid"), 
			   new nlobjSearchColumn("tranid")
			]
			);
	nlapiLogExecution('DEBUG', '', 'Length: '+purchaseorderSearch.length);
	return purchaseorderSearch;
}