/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Sep 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */

function processPO_beforeLoad(type, form, request)
{
	if(nlapiGetRecordType()!='purchaseorder'){
		
		if(nlapiGetFieldValue('custrecord_cntm_record_status')!='Complete')
			form.addButton('custpage_refresh_btn', 'Refresh', 'window.location.reload()');

		var hideField=form.addField('custpage_hide', 'inlinehtml', '', null, null);
		//hideField.setDisplayType('hidden');
		hideField.setDefaultValue("<script>window.load=jQuery('#tbl_back,#_back').hide();jQuery( document ).ready(function() {jQuery('input[type=\"button\"][value=\"Back\"]').hide();jQuery('input[type=\"button\"][value=\"Attach\"]').hide();jQuery('input[type=\"button\"][value=\"Customize View\"]').hide();jQuery('#tbl__back').hide(); jQuery('#tbl_secondary_back').hide();jQuery('#tbl_attach').hide(); jQuery('#tbl_customize').hide();jQuery('#recmachcustrecord_cntm_so_creation_record_main_form').hide(); })</script>");
		
	}
	
	if(type=="copy" && nlapiGetRecordType()=='purchaseorder'){
		nlapiSetFieldValue('custbody_cntm_created_so','');
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
/*function process_beforeSubmit(type){
	var count = nlapiGetLineItemCount('custpage_po_details');
	var msg = new Object();
	// Selected records list
	for (var i = 1; i <= count; i++)
	{
		if (nlapiGetLineItemValue('custpage_po_details', 'custpage_select', i) == 'T')
		{
			var poId = nlapiGetLineItemValue('custpage_po_details', 'custpage_internalid', i);
			poIds += poId + ",";
			msg[poId] = nlapiGetLineItemValue('custpage_po_details', 'custpage_reason', i);
		}
	}
	nlapiLogExecution('AUDIT', '', 'JSON String: ' + JSON.stringify(msg));
	// Store the reasons fields in File Cabinet
	var tempFile = nlapiCreateFile('msgList', 'PLAINTEXT', JSON.stringify(msg));
	var folderId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_msg_folderId');

	tempFile.setFolder(folderId);
	var fileId = nlapiSubmitFile(tempFile);

}*/