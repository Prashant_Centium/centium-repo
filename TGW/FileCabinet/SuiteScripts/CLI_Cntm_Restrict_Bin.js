/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Jun 2019     Prashant Kumar   
 * 2.00       11 july         Shashank Rao      Bug resolving on save record
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @returns {Boolean} True to continue save, false to abort save
 */
var restrictBinIds;
var restrictedBins;
var restrictedBinRecordId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_restricted_bins');
var orderType;
function restrictBin_pageInit()
{
	
	nlapiLogExecution('DEBUG', '', 'Restrict Multiple Bins Configuration Record Id: ' + restrictedBinRecordId);

	restrictBinIds = nlapiLookupField('customrecord_cntm_restict_multiple_bins', restrictedBinRecordId, 'custrecord_cntm_bins');
	restrictedBins = nlapiLookupField('customrecord_cntm_restict_multiple_bins', restrictedBinRecordId, 'custrecord_cntm_bins', true);
	


	
	restrictBinIds = restrictBinIds.split(',');
	nlapiLogExecution('DEBUG', '', 'Restricted Bin Ids: ' + restrictBinIds);
	orderType = nlapiGetFieldValue('ordertype');
	//alert('orderType : '+orderType);

	nlapiLogExecution('DEBUG', '', 'Order Type: ' + orderType);
}
function restrictBin_saveRecord()
{
	try
	{
		// Only for Customer Return Authorization
		//if (orderType == 'VendAuth')
		//{
			//alert('restrictBinIds : '+JSON.stringify(restrictBinIds));
			//alert('restrictedBins : '+JSON.stringify(restrictedBins));

			var count = nlapiGetLineItemCount('item');
			nlapiLogExecution('DEBUG', '', 'Record Line Count: ' + count);

			for (var linenum = 1; linenum <= count; linenum++)
			{
				var subrecord = nlapiViewLineItemSubrecord('item', 'inventorydetail', linenum);
				if(subrecord){
					var subcount = subrecord.getLineItemCount('inventoryassignment');
					nlapiLogExecution('DEBUG', '', 'Sub Record Line Count: ' + subcount);

					for (var subLinenum = 1; subLinenum <= subcount; subLinenum++)
					{
						subrecord.selectLineItem('inventoryassignment', subLinenum);

						if (restrictBinIds.indexOf(subrecord.getCurrentLineItemValue('inventoryassignment', 'binnumber')) > -1)
						{
							var bin = subrecord.getCurrentLineItemValue('inventoryassignment', 'binnumber');
							alert('Bins (' + restrictedBins + ') are not permitted to be selected on Inventory Details.\nPlease correct the respective enrties to save this form.');
							return false;
						}

					}

				}
			}

		//}
		return true;

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);
	}
}
