/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function cli_cntm_fetchMemo_clientPageInit(type){
	/*Description
	 * ---Logic---
	 * On PageInit
	 * Get SO Id from created from field and AssemblyItem Id
	 * Load SO and fetch SO Internal Line memo for Assembly Item
	 * Check for AssemblyItem from WO in SO line level, Get SO Line Internal Memo and SO line Qty
	 * Set Qty from SO to WO SO Line Qty field 
	 * Set memo from SO to WO SO Line Memo field 
	 */
	try{
		// Get SO Id
		var soId=nlapiGetFieldValue('createdfrom');
		nlapiLogExecution('DEBUG', 'Sales Order Id', soId);
		//Get AssemblyItem Id
		var itemId=nlapiGetFieldValue('assemblyitem');
		nlapiLogExecution('DEBUG', 'Assembly Item Id', itemId);
		
		if(!(soId==null || soId==undefined || soId=='') && !(itemId==null || itemId==undefined || itemId=='')){
			//Load SO
			var recordSO=nlapiLoadRecord('salesorder', soId);
			for(var linenum=1;linenum<=recordSO.getLineItemCount('item');linenum++){
				// Check for AssemblyItem from WO in SO line
				if(itemId==recordSO.getLineItemValue('item','item',linenum)){
					// Get SO Line Internal Memo
					var memoSO=recordSO.getLineItemValue('item','custcol_tgw_so_line_internal_memo',linenum);
					nlapiLogExecution('DEBUG', 'SO Line Memo', memoSO);
					// Get SO Qty
					var qty=recordSO.getLineItemValue('item','quantity',linenum);
					nlapiLogExecution('DEBUG', 'SO Line Qty', qty);
					// Set memo from SO to WO SO Line Memo field 
					nlapiSetFieldValue('custbody_cntm_so_line_memo', memoSO);
					// Set Qty from SO to WO SO Line Qty field 
					nlapiSetFieldValue('custbody_cntm_so_line_qty', qty);
					break;
				}
			}
		}
		
	}catch(e){
		nlapiLogExecution('ERROR', 'Error Occurred..', e.message);
	}
}
