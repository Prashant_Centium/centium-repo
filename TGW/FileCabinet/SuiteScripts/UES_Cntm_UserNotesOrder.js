/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Apr 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function ues_cntm_userNotesOrder_beforeLoad(type, form, request){
	try{//working
		nlapiLogExecution('DEBUG','Call in Before Load',type);
		form.setScript('customscript_interchange');
		var alert_value = "<script type='text/javascript'>window.load=setTimeout(function(){jQuery('#usernoteslnk').insertBefore('#activitieslnk');},500)</script>";
        var field = form.addField('custpage_alertfield' ,  'inlinehtml','',null);
        field.setDefaultValue(alert_value);
        }catch(e){
		nlapiLogExecution('ERROR', 'Error Occurred!', e.message);
	}
}
