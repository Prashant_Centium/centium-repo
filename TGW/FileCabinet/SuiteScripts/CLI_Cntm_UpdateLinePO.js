/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Aug 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @returns {Void}
 */
var linePOId="";
function updateLinePO_lineInit(type) {
	linePOId=nlapiGetCurrentLineItemValue('item', 'poid');
	nlapiLogExecution('AUDIT', 'On Line Init', 'PO Id: '+linePOId);	
     
}


function updateLinePO_fieldChanged(type,name,linenum){
	
	if(name=='custcol_scm_customerpartnumber' && linePOId){
		nlapiLogExecution('AUDIT', 'On Field Changed', 'PO Id: '+linePOId);
		//var record=nlapiLoadRecord('purchaseorder', linePOId);
		nlapiSetCurrentLineItemValue('item', 'custcol_cntm_linked_spec_po', linePOId, false, false);
		
		
	}
}


function copyLine(lineNum){
	var lineKeyValuePair = new Array();

	for(var i = 1; i<= lineFields.length; i++){

	lineKeyValuePair[lineFields[i]] = transaction.getLineItemValue('item',lineFields[i],j);
}
}