/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Jul 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function print_picking_ticket_suitelet(request,response){
	
	if (request.getMethod() == "GET") {
		try{
			// For default Logo
			var urlId=nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_url_id');
          	nlapiLogExecution('DEBUG','URL ID',urlId);

          	var base=fetchURL(urlId);
          	nlapiLogExecution('DEBUG','Base URL',base);

          	var recordId=request.getParameter('recordId');
          	nlapiLogExecution('DEBUG','recID',recordId);

          	var record;
          	if(recordId){
          		record=nlapiLoadRecord('salesorder',recordId);
          	}
          	var collect='';
          	var shipMethod='';
          	var shipCharge='';
          	var shipInstructions='';
          	if(record){
          		var customerRecord=nlapiLoadRecord('customer', record.getFieldValue('entity'));
          		collect=customerRecord.getFieldValue('thirdpartyacct');
          		nlapiLogExecution('DEBUG','Collect',collect);
          		
          		shipMethod=customerRecord.getFieldText('thirdpartycarrier');
          		nlapiLogExecution('DEBUG','Ship Via',shipMethod);
          		
          		shipCharge=customerRecord.getFieldText('custentity_wmk_cust_ship_charge');
          		nlapiLogExecution('DEBUG','Shipping Charge',shipCharge);

          		shipInstructions=customerRecord.getFieldValue('custentity_wmk_ship_instructions');
          		nlapiLogExecution('DEBUG','Shipping Instructions',shipInstructions);          		
          	}
			
			var xml= '<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd">'
				+'<pdf>'
				+'<head>'
				+'<macrolist>'
				+'<macro id="nlheader">'
				+'<table class="header" style="width: 100%;"><tr>'
				+'<td colspan="2" rowspan="3">';
				if(base){
					xml+='<img src="'+base+'" style="float: left; margin: 5px ;height:70px; width:200px;"/>';

				}
				xml+='</td><td align="right" valign="middle">'
				+'<table>'
				+'<tr><td><span class="title"><b>Picking Ticket</b></span></td></tr>'
				+'</table>'
				+'</td>'
				+'</tr></table>'
				+'</macro>'
				+'<macro id="nlfooter">'
				+'<table class="footer" style="width: 100%;">'
				+'<tr>'
				+'<td align="right"><pagenumber/> of <totalpages/></td>'
				+'</tr>'
				+'<tr>'
				+'<td align="center">For Our Current Terms And Conditions Of Sale, Please Refer To Our Website <i>www.tgwglobal.com</i><br/><br/><b>VAT NO. GB 534 1011 02 </b>Ref: FM02 Version: 2.0 Date 01/05/2019 Registered In England No. 2532135</td>'
				+'</tr>'
				+'</table>'
				+'</macro>'
				+'</macrolist>'
				+'<style type="text/css">'
				+'table{'
				+'font-size: 9pt;'
				+'table-layout: fixed;'
				+'}'
				+'th{'
				+'font-weight: bold;'
				+'font-size: 8pt;'
				+'vertical-align: middle;'
				+'padding: 5px 6px 3px;'
				+'background-color: #e3e3e3;'
				+'color: #333333;'
				+'}'
				+'td{'
				+'padding: 4px 6px;'
				+'vertical-align:center;'
				+'}'
				+'td p { align:left }'
				+'b{'
				+'font-weight: bold;'
				+'color: #333333;'
				+'}'
				+'table.header td{'
				+'padding: 0;'
				+'font-size: 10pt;'
				+'}'
				+'table.footer td{'
				+'padding: 0;'
				+'font-size: 8pt;'
				+'}'
				+'table.itemtable th {'
				+'padding-bottom: 10px;'
				+'padding-top: 10px;'
				+'}'
				+'table.body td{'
				+'padding-top: 2px;'
				+'}'
				+'td.addressheader{'
				+'font-size: 8pt;'
				+'font-weight: bold;'
				+'padding-top: 6px;'
				+'padding-bottom: 2px;'
				+'}'
				+'td.address{'
				+'padding-top: 0;'
				+'}'
				+'span.title{'
				+'font-size: 20pt;'
				+'}'
				+'span.number{'
				+'font-size: 16pt;'
				+'}'
				+'span.itemname{'
				+'font-weight: bold;'
				+'line-height: 150%;'
				+'}'
				+'div.returnform{'
				+'width: 100%;'
				+'/* To ensure minimal height of return form */'
				+'height: 200pt;'
				+'page-break-inside: avoid;'
				+'page-break-after: avoid;'
				+'}'
				+'hr{'
				+'border-top: 1px dashed #d3d3d3;'
				+'width: 100%;'
				+'color: #ffffff;'
				+'background-color: #ffffff;'
				+'height: 1px;'
				+'}'
				+'</style>'
				+'</head>'
				+'<body header="nlheader" header-height="7%" footer="nlfooter" footer-height="20pt" padding="0.5in 0.5in 0.5in 0.5in" size="A4">'
				+'<table style="width: 100%; margin-top: 0px;">'
				+'<tr>'
				+'<td class="address" colspan="18" >Wolstenholme Machine Knives Limited<br/>Clough Bank Works<br/>Carlisle St East, 1 Downgate Drive<br/>Sheffield S Yorkshire S4 8BT<br/>United Kingdom</td>'
				+'</tr></table>'
				+'<table width="100%"><tr>'
				+'<td colspan="5">'
				+'<table width="50mm">'
				+'<tr><th border="1 solid black">Ship To</th></tr>'
				+'<tr><td height="20mm" style="border-left:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">'
				+(record?hasValue(record.getFieldValue('shipaddress')):'<br/><br/><br/><br/>')
				+'</td>'
				+'</tr>'
				+'</table>'
				+'</td>'
				+'<td colspan="12">'
				+'<table align="right" width="60%">'
				+'<tr><th border="1 solid black" colspan="4">Ship Date</th>'
				+'<td colspan="8" style="border-top:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">'+(record?record.getFieldValue('trandate'):'')+'</td></tr>'
				+'<tr><th colspan="4" style="border-left:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">SO #</th>'
				+'<td colspan="8" style="border-bottom:1 solid black;border-right:1 solid black;">'+(record?record.getFieldValue('tranid'):'')+'</td></tr>'
				+'<tr>'
				+'<th colspan="4" style="border-left:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">Ship Via</th>'
				+'<td colspan="8"  style="border-bottom:1 solid black;border-right:1 solid black;">'+hasValue(shipMethod)+'</td></tr>'
				+'<tr>'
				+'<th colspan="4" style="border-left:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">Collect #</th>'
				+'<td colspan="8"  style="border-bottom:1 solid black;border-right:1 solid black;">'+hasValue(collect)+'</td></tr>'
				+'</table></td>'
				+'</tr>'
				+'</table>';
				
				var count=record?record.getLineItemCount('item'):0;
				
				if(count>0){
					xml+='<table class="itemtable" style="width: 100%; margin-top: 10px;">'
					+'<thead>'
					+'<tr>'
					+'<th colspan="4" align="center" border="1 solid black">Item<br/>And<br/>Descriptions</th>'
					+'<th colspan="3" align="center" style="border-top:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">WMK#/<br/>DWG#</th>'
					+'<th colspan="2" align="center" style="margin:0px;padding:0px;border-top:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">Expected<br/>Ship<br/>Date</th>'
					+'<th colspan="2" align="center" style="border-top:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">Bin</th>'
					+'<th colspan="2" align="center" style="border-top:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">Order</th>'
					+'<th colspan="3" align="center" style="border-top:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">Shipping/<br/>Carriage<br/>Charge</th>'
					+'<th colspan="3" align="center" style="border-top:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">Shipping/<br/>Carriage<br/>Charge<br/>Instructions</th>'
					+'<th colspan="5" align="center" style="border-top:1 solid black;border-bottom:1 solid black;border-right:1 solid black;">SO Line<br/>Internal Memo</th>'
					+'</tr>'
					+'</thead>';
					for(var linenum=1;linenum<=count;linenum++){
						if(record.getLineItemValue('item', 'itempicked', linenum)=='F'){
							var expectedshipdate=record.getLineItemValue('item', 'expectedshipdate', linenum);
							if(expectedshipdate){
								expectedshipdate=expectedshipdate.split('/');
								expectedshipdate=expectedshipdate[0]+'/'+expectedshipdate[1]+'/'+expectedshipdate[2]%100;
									
							}
							
							xml+='<tr>'
							+'<td colspan="4" valign="middle" align="left" style="border-left:1 solid black;border-bottom:1 solid black;border-right:1 solid black;"><b>'
							+hasValue(record.getLineItemValue('item', 'item_display', linenum))+'</b><br/>'
							+hasValue(record.getLineItemValue('item', 'description', linenum))+'</td>'
							+'<td colspan="3" align="center" valign="middle"  style="border-bottom:1 solid black;border-right:1 solid black;">'
							+hasValue(record.getLineItemValue('item', 'custcol_wmk_cntm', linenum))+'<br/><br/>'
							+hasValue(record.getLineItemValue('item', 'custcol_dwg_cntm', linenum))+'</td>'
							+'<td colspan="2"  valign="middle"  style="border-bottom:1 solid black;border-right:1 solid black;">'+(expectedshipdate?expectedshipdate:'')+'</td>'
							+'<td colspan="2" align="center" valign="middle"  style="border-bottom:1 solid black;border-right:1 solid black;">'+hasValue(record.viewLineItemSubrecord('item', 'inventorydetail', linenum))+'</td>'
							+'<td colspan="2" align="center" valign="middle"  style="border-bottom:1 solid black;border-right:1 solid black;">'+hasValue(record.getLineItemValue('item', 'quantity', linenum))+'</td>'
							+'<td colspan="3" align="center" valign="middle"  style="border-bottom:1 solid black;border-right:1 solid black;">'+hasValue(shipCharge)+'</td>'
							+'<td colspan="3" align="center" valign="middle"  style="border-bottom:1 solid black;border-right:1 solid black;">'+hasValue(shipInstructions)+'</td>'
							+'<td colspan="5" align="center" valign="middle"  style="border-bottom:1 solid black;border-right:1 solid black;">'+hasValue(record.getLineItemValue('item', 'custcol_tgw_so_line_internal_memo', linenum))+'</td>'
							+'</tr>';
						}
						
					}	
					xml+='</table>';
				}			
				xml+='</body>'
				+'</pdf>';
			
			
	var file = nlapiXMLToPDF(xml);
			response.setContentType('PDF', 'Picking_Ticket.pdf', 'inline');
			response.write(file.getValue());
		}
		catch(e){
			response.write('Error occured while printing  Picking Ticket. NetSuite error: '+e.message);
		}
	}
}

function fetchURL(Id){
	var search=nlapiSearchRecord("file",null,[new nlobjSearchFilter("internalid",null,"is", Id)], [new nlobjSearchColumn("url")]);
  	var url=search[0].getValue('url');
  	return nlapiEscapeXML(url);
}
function hasValue(variable){
	if(variable==undefined || variable==null || variable=='')
		return ' ';
	return nlapiEscapeXML(variable);
}