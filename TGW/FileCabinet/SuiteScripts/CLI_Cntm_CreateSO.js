/**
 * Module Description
 * 
 * Version Date Author Remarks 
 * 1.00 23 Sep 2019 Prashant Kumar
 * 
 */
/**
 * fetch the checked POs and pass them to suitelet
 */
function process(type)
{
	nlapiLogExecution('AUDIT', 'In Client Script', '-----Start------');
	try
	{
		onbeforeunload = null;
		var poIds = "";
		var count = nlapiGetLineItemCount('custpage_po_details');
		var msg = new Object();
		var checkCnt = 0;
		var commentCnt = 0;
		// Selected records list
		for (var i = 1; i <= count; i++)
		{
			if (nlapiGetLineItemValue('custpage_po_details', 'custpage_select', i) == 'T')
			{
				var poId = nlapiGetLineItemValue('custpage_po_details', 'custpage_internalid', i);
				poIds += poId + ",";
				msg[poId] = nlapiGetLineItemValue('custpage_po_details', 'custpage_comments', i);
				checkCnt++;
				var comment = nlapiGetLineItemValue('custpage_po_details', 'custpage_comments', i);
				nlapiLogExecution('AUDIT', '', 'Comment: ' + comment);
				
				// check if type=reject then make the comments mandatory.
				if (type == 'reject' && comment && comment.length > 1)
				{
					commentCnt++;

				}
			}
		}
		nlapiLogExecution('AUDIT', '', 'JSON String: ' + JSON.stringify(msg));
		/*
		 * // Store the reasons fields in File Cabinate var tempFile = nlapiCreateFile('msgList', 'PLAINTEXT', JSON.stringify(msg)); var folderId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_msg_folderId');
		 * 
		 * tempFile.setFolder(folderId); var fileId = nlapiSubmitFile(tempFile);
		 */

		// Check for selection
		if (!poIds)
		{
			alert("Please select at least one record to be processed.");
			return false;
		}
		nlapiLogExecution('AUDIT', '', 'Comment vs Check: ' + commentCnt + " " + checkCnt);

		if (type == 'reject' && checkCnt != commentCnt)
		{
			alert("Please provide a rejection reason in the comments section.");
			return false;
		}
		// Call the Suitelet based on request type
		var url = nlapiResolveURL('SUITELET', 'customscript_cntm_create_so_sut', 'customdeploy_cntm_create_so_sut') + '&type=' + type + "&poIds=" + poIds + '&msg=' + JSON.stringify(msg);
		nlapiLogExecution('AUDIT', '', 'Type: ' + type);

		window.location.replace(url);

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);

	}
	nlapiLogExecution('AUDIT', 'In Client Script', '-----End------');
}
/**
 * makes the PO from field mandatory for specified vendor to decide for which customer SO wil be created.  
 * @returns {Boolean}
 */
/*function fieldMandate()
{
	try
	{
		var vendor = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_vendorid');

		if (nlapiGetFieldValue('entity') == vendor)
		{
			if (!nlapiGetFieldValue('custbody_cntm_po_from'))
			{
				alert("Please Choose the Subsidiary for PO From.");
				return false;
			}
			
		}
		return true;
		
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);
	}

}
*/

