/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 Nov 2018     Yogesh Jagdale   Initial Development
 * 1.10       5  June 2019    Prashant Kumar   Added code for PrintTtraveler and Print BOM button
 * 1.20		  30 Sep 2019     Prashant Kuar    Added code for India Subsidiary
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request)
{
	try
	{
		if (type == 'view')
		{
			form.setScript('customscript_custom_fields_on_wo');
			var btnName = '';
			var label = '';
			var functionName = '';
			nlapiLogExecution('DEBUG', '', 'Subsidiary Id: '+nlapiGetFieldValue('subsidiary'));
			if (nlapiGetFieldValue('subsidiary') != 24 && nlapiGetFieldValue('subsidiary')!=27)
			{
				btnName = 'custpage_print_bom';
				label = 'Print BOM'
				functionName = 'printBOM()';
			}
			else
			{
				btnName = 'custpage_print_job_traveler';
				label = 'Print Traveler';
				functionName = 'printTraveler()';
			}
			form.getButton('printbom').setVisible(false);
			form.addButton(btnName, label, functionName);

		}
		var vendorAddressField = form.addField('custpage_vendoraddress', 'textarea', '').setDisplayType('hidden');
		var notes = form.addField('custpage_usernotes', 'text', '').setDisplayType('hidden');
		var workorderSearch = nlapiSearchRecord("workorder", null, [ [ "type", "anyof", "WorkOrd" ], "AND", [ "internalid", "anyof", nlapiGetRecordId() ], "AND", [ "mainline", "is", "T" ] ], [ new nlobjSearchColumn("internalid", "userNotes", null).setSort(true), new nlobjSearchColumn("note", "userNotes", null) ]);
		var userNote = workorderSearch[0].getValue(workorderSearch[0].getAllColumns()[1]);
		vendorAddressField.setDisplayType('hidden');
		notes.setDisplayType('hidden');
		notes.setDefaultValue(userNote);
		var id = nlapiGetFieldValue('location');
		if (id != "")
		{
			var record = nlapiLoadRecord('location', id);
			var address = record.getFieldValue('mainaddress_text');
			vendorAddressField.setDefaultValue(address.split('\n').join('<br/>'));
		}
	}

	catch (e)
	{
		nlapiLogExecution('ERROR', 'Catch', e.message);
	}
}
