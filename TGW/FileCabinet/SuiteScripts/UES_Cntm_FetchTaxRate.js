/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 May 2019     Prashant Kumar
 * 1.1        30 July 2019    Prashant Kumar  Added Code for Picking Template Button
 * 1.2		  12 July 2019    Prashant Kumar  Added Code for Custom field collect and ship carrier to source value from customer record 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function fetchTaxRate_beforeLoad(type, form, request)
{

	try
	{
		// For WMK Subsidiary Only add the button on SO to print picking template
		if (type == 'view' && nlapiGetFieldValue('subsidiary') == 24 && nlapiGetRecordType() == 'salesorder')
		{
			form.setScript('customscript_default_location');
			form.addButton('custpage_pick_tckt', 'Print Picking Ticket', 'printPickTicket()');
		}
		//

		var taxCodeId = nlapiGetFieldValue('custbody_cntm_tax_code');
		var taxCodeName = nlapiGetFieldText('custbody_cntm_tax_code');

		nlapiLogExecution('DEBUG', '', ' Tax Code Name: ' + taxCodeName);

		var taxRate = nlapiGetFieldValue('custbody_cntm_tax_rate');
		nlapiLogExecution('DEBUG', '', ' Tax Rate: ' + taxRate);

		if (!taxRate && taxCodeId)
		{
			var taxObj = nlapiLookupField('taxgroup', taxCodeId, [ 'rate', 'itemid' ]);
			var rate;
			var name;
			if (taxObj)
			{
				rate = taxObj['rate'];
				name = taxObj['itemid'];
				nlapiLogExecution('DEBUG', '', ' Tax Code present in  Tax Group: ' + name);

			}

			if (taxCodeName != name)
			{
				taxObj = nlapiLookupField('salestaxitem', taxCodeId, [ 'rate', 'itemid' ]);
				rate = taxObj['rate'];
				name = taxObj['itemid'];
				nlapiLogExecution('DEBUG', '', ' Tax Code present in  Sales Tax Item: ' + name);

			}
			nlapiSetFieldValue('custbody_cntm_tax_rate', rate, false, false)
		}

		// Set Custom Field Value for collect and ship carrier
		var customerId = nlapiGetFieldValue('entity');

		var custRecord = nlapiLoadRecord('customer', customerId);
		var collect = custRecord.getFieldValue('thirdpartyacct');
		var shipcarrier = custRecord.getFieldValue('thirdpartycarrier');

		nlapiLogExecution('AUDIT', '', 'Collect: ' + collect + ' Ship Carrier: ' + shipcarrier);
		
		nlapiSetFieldValue('custbody_cntm_collect', collect,false, false);
		nlapiSetFieldValue('custbody_cntm_ship_carrier', shipcarrier),false, false;
		//
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', ' Error Details: ' + e.message);

	}

}
