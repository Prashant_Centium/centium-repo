/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Jun 2019     Prashant Kumar
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

var searchId = nlapiGetContext().getSetting('Script', 'custscript_cntm_ss_item');

function itemWithout_preferredBin_pdf_suitelet(request, response){
		try{
			
			nlapiLogExecution('DEBUG', '', 'Parameter: ' + searchId);

          	var comrec=nlapiLoadConfiguration('companyinformation');
          	var search=nlapiSearchRecord("file",null,[new nlobjSearchFilter("internalid",null,"is", comrec.getFieldValue('formlogo'))], [new nlobjSearchColumn("url")]);
			var base=search[0].getValue('url');
			nlapiLogExecution('DEBUG', 'Base Url', base);
          	//var name=nlapiGetContext().name;
          	var xml = '<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd"><pdf><head><macrolist><macro id="nlheader">'
          			+'<table class="header" style="width: 100%;"><tr><td colspan="2" rowspan="3"><img src="'
					+ nlapiEscapeXML(base)
					+ '" style="float: left; margin: 5px ;height:70px; width:200;" /></td><td colspan="4" align="right" margin-top="10px"><span class="title">'+"Items Without Preferred Bins"+'</span></td></tr></table></macro>'
					+'<macro id="nlfooter">'
					+'<table class="footer" style="width: 100%;"><tr><td align="right"><pagenumber/> of <totalpages/></td></tr>'
					+'<tr><td align="center">Tel. 859-647-7383 &bull; Toll Free 1-800-407-0173 &bull; 5 Braco International Blvd. &bull; Wilder, Ky 41076 &bull; sales@tgwint.com &bull; www.tgwint.com</td></tr></table>'
					+'</macro></macrolist><style type="text/css">table {font-size: 9pt;table-layout: fixed;}th {font-weight: bold;font-size: 8pt;vertical-align: middle;padding: 5px 6px 3px;background-color: #e3e3e3;color: #333333;}'
					+'td {padding: 4px 6px;}td p{align:left}b {font-weight: bold;}table td th{border-collapse: collapse;}table.header td{padding: 0;font-size: 10pt;}table.footer td{padding: 0;font-size: 9pt;}table.itemtable th{padding-bottom: 10px;padding-top: 10px;}'
					+'table.body td{padding-top: 1px;}table.total{page-break-inside: avoid;}tr.totalrow{background-color: #e3e3e3;line-height: 150%;}td.totalboxtop{font-size: 12pt;background-color: #e3e3e3;}'
					+'td.addressheader{font-size: 9pt;padding-top: 6px;padding-bottom: 2px;}td.address{padding-top: 0;}td.totalboxmid{font-size: 28pt;padding-top: 20px;background-color: #e3e3e3;}td.totalboxbot{background-color: #e3e3e3;font-weight: bold;}'
					+'span.title{font-size: 15pt;font-weight: bold;}span.number {font-size: 15pt;}span.itemname{font-weight: bold;line-height: 150%;}hr{width: 100%;color: #d3d3d3;background-color: #d3d3d3;height: 1px;}</style></head>'
					+'<body header="nlheader" header-height="10%" footer="nlfooter" footer-height="40pt" padding="0.5in 0.5in 0.5in 0.5in" size="Letter">'
					+'<table  style="width:100%;" cellspacing="0px" cellpadding="3px"><tr><td colspan="18">'
					+'<!--<table><tr><th style="border:1px solid black;">Location</th>'
					+'<td style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"></td>'
					+'</tr></table></td><td colspan="8"><table class="headerright" width="100%"><tr><th style="border:1px solid black;">Print Date</th>'
					+'<td style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">'
					//+ usDate
					+ '</td></tr><tr><th style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Print Time</th>        <td style="border-right:1px solid black;border-bottom:1px solid black;">'
					//+ time
					+ '</td></tr><tr><th style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Author</th>        <td style="border-right:1px solid black;border-bottom:1px solid black;">'
					//+ name
					+ '</td></tr></table> --></td></tr></table><br/>'
					+'<table class="itemtable" style="width: 100%;"><thead><tr>'
					+'<th align="center" colspan="5" border="1px solid black;">Name</th>'
					+'<th align="center" colspan="9" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Description</th>'
					+'<th align="center" colspan="5" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Type</th>'
					+'<th align="center" colspan="5" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Loaction</th>'
					+'<th align="center" colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;white-space:nowrap;">OnHand<br/>Qty</th>'
					+'<th align="center" colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Bin</th></tr></thead>'
					//+'<th align="center" colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;white-space:nowrap;">Preferred<br/>Bin</th>';
          	var lines=getSavedSearchObject();
          	if(lines && lines.length){
               	nlapiLogExecution('Debug','lines',lines.length);
			
               	for(var i=0;i<lines.length;i++){
               		xml+='<tr><td colspan="5" style="border-right:1px solid black;border-bottom:1px solid black;border-left:1px solid black;">'+nlapiEscapeXML(lines[i].name)+'</td>';
               		xml+='<td colspan="9" style="border-right:1px solid black;border-bottom:1px solid black;" >'+nlapiEscapeXML(lines[i].description)+'</td>';
               		xml+='<td colspan="5" style="border-right:1px solid black;border-bottom:1px solid black;" >'+lines[i].type+'</td>';
               		xml+='<td colspan="5" style="border-right:1px solid black;border-bottom:1px solid black;" >'+nlapiEscapeXML(lines[i].location)+'</td>';
               		xml+='<td colspan="3" align="center" style="border-right:1px solid black;border-bottom:1px solid black;" >'+lines[i].onhandqty+'</td>';
               		xml+='<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;" >'+lines[i].bin+'</td></tr>';
               		//xml+='<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;" ><!--'+nlapiEscapeXML(lines[i].preferredBin)+'--></td>';
  
               	}
               	xml+='</table><hr /></body></pdf>';
               	var file = nlapiXMLToPDF(xml);
               	response.setContentType('PDF', "Today's_ItemReceipt_", 'inline');
               	
               	response.write(file.getValue());
         	}
          else{
        	  response.write("<center><h1 style='color:red;'> No Data Available For Printing</h1></center>");
          }
		}catch(e)
		{
			response.write('Error Occured while generating Pdf. NetSuite error: '+e.message);
		}
	}


function getSavedSearchObject(){
	var results=loadRecords(null,searchId,null,null);
	
	var dataObjectArr=new Array();
	for(var linenum=0;linenum<results.length;linenum++){
		//nlapiLogExecution('DEBUG', '','LineNum: '+linenum);
		var dataObject={};
		dataObject.name=results[linenum].getValue(new nlobjSearchColumn("itemid").setSort(false));
		dataObject.description=results[linenum].getValue(new nlobjSearchColumn("salesdescription"));
		dataObject.type=results[linenum].getText(new nlobjSearchColumn("type"));
		dataObject.location=results[linenum].getValue(new nlobjSearchColumn("name","inventoryLocation",null));
		dataObject.onhandqty=results[linenum].getValue(new nlobjSearchColumn("locationquantityonhand").setSort(false));
		dataObject.bin=results[linenum].getValue(new nlobjSearchColumn("binnumber"));
		dataObjectArr.push(dataObject);
	}
	
	return dataObjectArr;
}
