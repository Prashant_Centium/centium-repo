/**
 * Mapping for Dispatch Track Import
 */

function serviceOrder(order)
{
	nlapiLogExecution('AUDIT', '', 'In ServiceOrder');
	var customer = fetchCustomerDetails(order.getFieldValue('entity'));
	var serviceOrder = '<service_order>' + '<number>' + order.getFieldValue('tranid') + '</number>'// {Unique Number of the order}
			+ '<account></account>'// '+{Account ID}+'
			+ '<service_type></service_type>'// '+{[Delivery, Pickup, Service - Should match the names on the service types]}+'
			+ '<description><![CDATA[]]></description>'// '+{description}+'
			+ '<customer>' + '<customer_id>' + customer.id + '</customer_id>'// {Database ID of the customer in your System}
			+ '<first_name>' + customer.firstname + '</first_name>'// {Customer Firstname}
			+ '<last_name>' + customer.lastname + '</last_name>'// {Customer Lastname}
			+ '<email>' + customer.email + '</email>'// {Customer email address}
			+ '<phone1>' + customer.phone1 + '</phone1>'// {Customer primary phone number}
			+ '<phone2>' + customer.phone2 + '</phone2>'// {Customer secondary phone number}
			+ '<phone3>' + customer.phone3 + '</phone3>'// {Customer Tertiary Phone Number}
			+ '<address1>' + customer.addr1 + '</address1>'// {Customer Ship Address Line1}
			+ '<address2>' + customer.addr2 + '</address2>'// {Customer Ship Address Line2}
			+ '<city>' + customer.city + '</city>'// {Customer Ship City}
			+ '<state>' + customer.state + '</state>'// {Customer Ship State}
			+ '<zip>' + customer.zip + '</zip>'// {Customer Ship Zip}
			+ '<latitude>' + customer.lattitude + '</latitude>'// {Customer Geographic latitude (optional)}
			+ '<longitude>' + customer.longitude + '</longitude>'// {Customer Geographic longitude (optional)}
			+ '<preferred_contact_method>' + customer.pref_con_method + '</preferred_contact_method>'// {ALL/EMAIL/TEXT/VOICE/NONE}
			+ '</customer>'
			/*
			 * +'<notes count="'+{NUMBER_OF_NOTES}+'">' +'<note created_at="'+{DATE_TIMESTAMP}+'" author="'+{USER/SERVICE_UNIT_LOGIN}+' note_type '+{Public/Private [optional]}+'">' +'<![CDATA['+{test note}+']]>' +'</note>' +'<note created_at="'+{DATE_TIMESTAMP}+'" author="'+{USER/SERVICE_UNIT_LOGIN}+'note_type '+{Public/Private [optional]}+'">' +'</note>' +'</notes>'
			 */+ '<items>'// Can have multiple Items
			+ getItems(order) + '</items>' + '<amount>' + order.getFieldValue('total') + '</amount>'// {Total amount on the invoice (optional)}
			// +'<cod_amount>'+order.getFieldValue('')+'</cod_amount>'//{COD Amount on delivery/service (optional)}
			/*
			 * +'<service_unit>'+order.getFieldValue('')+'</service_unit>'//{name of the resource/route (optional)} +'<delivery_date>'+order.getFieldValue('')+'</delivery_date>'//{delivery/service date} +'<request_delivery_date>'+order.getFieldValue('')+'</request_delivery_date>'//{sale date} +'<driver_id>'+order.getFieldValue('')+'</driver_id>'//{driver ID if pre-assigned (optional)} +'<truck_id>'+order.getFieldValue('')+'</truck_id>'//{vehicle ID if pre-assigned (optional)} +'<stop_number>'+order.getFieldValue('')+'</stop_number>'//{Manifest Stop number (optional)} +'<stop_time>'+order.getFieldValue('')+'</stop_time>'//{Manifest Stop time (optional)} +'<service_time>'+order.getFieldValue('')+'</service_time>'//{Total time at the stop (optional)} +'<request_time_window_start>'+order.getFieldValue('')+'</request_time_window_start>'//{Servicerequest Start Time } +'<request_time_window_end>'+order.getFieldValue('')+'</request_time_window_end>'//{Service request End Time} +'<delivery_time_window_start>'+order.getFieldValue('')+'</delivery_time_window_start>'//{Window request Start Time} +'<delivery_time_window_end>'+order.getFieldValue('')+'</delivery_time_window_end>'//{Window request End Time} +'<delivery_charge>'+order.getFieldValue('')+'</delivery_charge>'//{ Delivery charges for the particular invoice (optional)} +'<taxes>'+order.getFieldValue('')+'</taxes>'//{ Total Taxes invoice (optional)} +'<store_code>'+order.getFieldValue('')+'</store_code>'//{Warehouse# for Stop Pickup Item - (optional)} +'<extra>' +'<custom_field_1>'+order.getFieldValue('')+'</custom_field_1>'//{value1} +'<custom_field_2>'+order.getFieldValue('')+'</custom_field_2>'//{value2} +'<custom_field_3>'+order.getFieldValue('')+'</custom_field_3>'//{value3} +'</extra>' +'<custom_fields>' +'<custom_field_1>'+order.getFieldValue('')+'</custom_field_1>'//{value1} +'<custom_field_2>'+order.getFieldValue('')+'</custom_field_2>'//{value2} +'<custom_field_3>'+order.getFieldValue('')+'</custom_field_3>'//{value3} +'</custom_fields>'
			 */+ '</service_order>';

	return serviceOrder;
}

function serviceOrders()
{
	nlapiLogExecution('AUDIT', '', 'In ServiceOrders');

	var serviceorder = "";

	for (var i = 785; i < 785+5; i++)
	{
		var order = nlapiLoadRecord('salesorder', i);// hardcoded
		serviceorder += serviceOrder(order);
	}

	var serviceOrders = '<service_orders>' + serviceorder + '</service_orders>';

	return serviceOrders;

}

function xmlToJosnConverter(xml)
{
	var Obj = {};
	if (xml.nodeType == 1)
	{// Element
		if (xml.attributes.length > 0)
		{
			Obj['@attribute'] = new Array();
			for (var i = 0; i < xml.attributes.length; i++)
			{
				var attribute = xml.attributes.item(i);
				var attObj = {};
				attObj[attribute.nodeName] = attribute.nodeValue
				Obj['@attribute'].push(attObj);
			}
		}

	}
	else if (xml.nodeType == 3 && xml.nodeValue.trim() != "")
	{// Text
		Obj = xml.nodeValue;
	}

	for (var j = 0; j < xml.childNodes.length; j++)
	{
		var child = xml.childNodes.item(j);
		if (!(child.nodeName in Obj))
		{
			Obj[child.nodeName] = xmlToJosnConverter(child);
		}
		else
		{
			var arr = new Array();
			arr.push(Obj[child.nodeName]);
			arr.push(xmlToJosnConverter(child));
			Obj[child.nodeName] = arr;
		}
	}
	return Obj;
}

function jsonToXmlConverter(json, node)
{

	for ( var key in json)
	{

		if (key == '@attribute')
		{

			for ( var attr_num in json[key])
			{
				for ( var attr in json[key][attr_num])
					node.setAttribute(attr, json[key][attr_num][attr]);
			}

		}
		else if (key == '#text')
		{
			node.createTextNode(json[x]);

		}
		else
		{
			if (!node)
			{
				node = nlapiStringToXML('<root></root>');
			}
			var childNode = node.createElement(key);
			node.appendChild(jsonToXmlConverter(json[key], childNode));
		}
	}
	return node;
}

function getItems(order)
{
	nlapiLogExecution('AUDIT', '', 'In Get Items');
	var count = order.getLineItemCount('item');
	var items = '';

	for (var line = 1; line <= count; line++)
	{
		items += '<item>' + '<sale_sequence>' + order.getLineItemValue('item', 'line', line) + '</sale_sequence>'// {Line Item sequence on the invoice}
				+ '<item_id>' + order.getLineItemText('item', 'item', line) + '</item_id>'// {Model number}
				+ '<serial_number></serial_number>'// '+{Serial Number}+'
				+ '<description>' + '<![CDATA[' + order.getLineItemValue('item', 'description', line) + ']]>'// {description}
				+ '</description>' + '<line_item_notes></line_item_notes>'// '+{Any additional information can be imported to this field}+'
				+ '<number></number>'// '+{Barcode Code number}+'
				+ '<quantity>' + order.getLineItemValue('item', 'quantity', line) + '</quantity>'// {Ship Quantity}
				+ '<location>' + order.getLineItemValue('item', 'location', line) + '</location>'// {Location in the Warehouse}
				+ '<cube></cube>'// '+{Item Volume (optional)}+'
				+ '<setup_time></setup_time>'// '+{Item Setup Time(optional)}+'
				+ '<weight></weight>'// '+{Item Weight (optional)}+'
				+ '<price></price>'// '+{Item Price (optional)}+'
				+ '<countable></countable>'// '+{true/false � default (true) � (optional)}+'
				+ '<store_code></store_code>'// '+{Warehouse# for Store Pickup - (optional)}+'
				+ '</item>'
	}

	nlapiLogExecution('AUDIT', '', 'Items ' + JSON.stringify(items));

	return items;
}

function fetchCustomerDetails(id)
{
	nlapiLogExecution('AUDIT', '', 'In Get Customer with Id ' + id);
	var customer = new Object();
	var record = nlapiLoadRecord('customer', id);
	var address = fetchAddress(record);
	customer.id = id;
	customer.firstname = record.getFieldValue('firstname');
	customer.lastname = record.getFieldValue('lastname');
	customer.email = record.getFieldValue('email');
	customer.phone1 = record.getFieldValue('phone');
	customer.phone2 = "";
	customer.phone3 = "";
	customer.addr1 = address.addr1;
	customer.addr2 = address.addr2;
	customer.city = address.city;
	customer.state = address.state;
	customer.zip = address.zip;
	customer.lattitude = ""
	customer.longitude = "";
	customer.pref_con_method = "";

	nlapiLogExecution('AUDIT', '', 'Customer ' + JSON.stringify(customer));

	return customer;

}

function fetchAddress(customerRec)
{
	nlapiLogExecution('AUDIT', '', 'In Get Address');
	var address = new Object();

	for (var line = 1; line <= customerRec.getLineItemCount('addressbook'); line++)
	{

		if (customerRec.getLineItemValue('addressbook', 'defaultshipping', line) == 'T')
		{

			address.addr1 = customerRec.getLineItemValue('addressbook', 'addr1', line);
			address.addr2 = customerRec.getLineItemValue('addressbook', 'addr2', line);
			address.city = customerRec.getLineItemValue('addressbook', 'city', line);
			address.state = customerRec.getLineItemValue('addressbook', 'state', line);
			address.zip = customerRec.getLineItemValue('addressbook', 'zip', line);

		}

	}

	nlapiLogExecution('AUDIT', '', 'Address ' + JSON.stringify(address));
	return address;
}
