/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Jun 2019     Prashant Kumar
 * 2.00       11 july 2019    Shashank Rao     Adding Location Filter in suitelet
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
var searchId = nlapiGetContext().getSetting('Script', 'custscript_cntm_ss_item_receipt');
function itemreceipts_pdf_suitelet(request, response){
	//var method=request.getParameter('method');
	//nlapiLogExecution('DEBUG', '', 'Method: ' + method);
	
	
	nlapiLogExecution('DEBUG', '', 'Parameter: ' + searchId);
	if(request.getMethod()=='POST'){
		try{
          	var comrec=nlapiLoadConfiguration('companyinformation');
          	var search=nlapiSearchRecord("file",null,[new nlobjSearchFilter("internalid",null,"is", comrec.getFieldValue('formlogo'))], [new nlobjSearchColumn("url")]);
			var base=search[0].getValue('url');
			nlapiLogExecution('DEBUG', 'Base Url', base);
          	//var name=nlapiGetContext().name;
			var fromDate = request.getParameter('custpage_fromdate');
    		nlapiLogExecution('DEBUG', '', 'In Suitelet From Date: ' + fromDate);

    		var toDate = request.getParameter('custpage_todate');
    		nlapiLogExecution('DEBUG', '', 'In Suitelet To Date: ' + toDate);
    		/*-------------------------------------------------------------------------------------------------------*/
    		//Added By Shashank on 11-07-2019
    		
    		var locationList = request.getParameterValues('custpage_location');
    		
    		nlapiLogExecution('DEBUG', 'Location', JSON.stringify(locationList));
    		var lines=getSavedSearchObject(fromDate,toDate,locationList);
          
    		/*-------------------------------------------------------------------------------------------------------*/

    		var xml = '<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd"><pdf><head><macrolist><macro id="nlheader">'
          			+'<table class="header" style="width: 100%;"><tr><td colspan="2" rowspan="3"><img src="'
					+ nlapiEscapeXML(base)
					+ '" style="float: left; margin: 5px ;height:70px; width:200;" /></td><td colspan="4" align="right" margin-top="10px"><table><tr><td><span class="title">'+"Item Receipts"+'</span></td></tr><tr><td margin-top="10px" align="center">[ '+(fromDate?fromDate:'N/A')+' - '+(toDate?toDate:'N/A')+' ]</td></tr></table></td></tr></table></macro>'
					+'<macro id="nlfooter">'
					+'<table class="footer" style="width: 100%;"><tr><td align="right"><pagenumber/> of <totalpages/></td></tr>'
					+'<tr><td align="center">Tel. 859-647-7383 &bull; Toll Free 1-800-407-0173 &bull; 5 Braco International Blvd. &bull; Wilder, Ky 41076 &bull; sales@tgwint.com &bull; www.tgwint.com</td></tr></table>'
					+'</macro></macrolist><style type="text/css">table {font-size: 9pt;table-layout: fixed;}th {font-weight: bold;font-size: 8pt;vertical-align: middle;padding: 5px 6px 3px;background-color: #e3e3e3;color: #333333;}'
					+'td {padding: 4px 6px;}td p{align:left}b {font-weight: bold;}table td th{border-collapse: collapse;}table.header td{padding: 0;font-size: 10pt;}table.footer td{padding: 0;font-size: 9pt;}table.itemtable th{padding-bottom: 10px;padding-top: 10px;}'
					+'table.body td{padding-top: 1px;}table.total{page-break-inside: avoid;}tr.totalrow{background-color: #e3e3e3;line-height: 150%;}td.totalboxtop{font-size: 12pt;background-color: #e3e3e3;}'
					+'td.addressheader{font-size: 9pt;padding-top: 6px;padding-bottom: 2px;}td.address{padding-top: 0;}td.totalboxmid{font-size: 28pt;padding-top: 20px;background-color: #e3e3e3;}td.totalboxbot{background-color: #e3e3e3;font-weight: bold;}'
					+'span.title{font-size: 15pt;font-weight: bold;}span.number {font-size: 15pt;}span.itemname{font-weight: bold;line-height: 150%;}hr{width: 100%;color: #d3d3d3;background-color: #d3d3d3;height: 1px;}</style></head>'
					+'<body header="nlheader" header-height="10%" footer="nlfooter" footer-height="40pt" padding="0.5in 0.5in 0.5in 0.5in" size="Letter">'
					+'<table  style="width:100%;" cellspacing="0px" cellpadding="3px"><tr><td colspan="18">'
					+'<!--<table><tr><th style="border:1px solid black;">Location</th>'
					+'<td style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"></td>'
					+'</tr></table>--></td><td colspan="8"><table class="headerright" align="right" width="50%" margin-top="-20px" margin-bottom="5px"><tr><th style="border:1px solid black;" align="center">Total</th>'
					+'<td style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">'
					+ lines.length
					+ '</td></tr><!--<tr><th style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Print Time</th><td style="border-right:1px solid black;border-bottom:1px solid black;">'
					//+ time
					+ '</td></tr><tr><th style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Author</th><td style="border-right:1px solid black;border-bottom:1px solid black;">'
					//+ name
					+ '</td></tr>--></table></td></tr></table>'
					+'<table class="itemtable" style="width: 100%;"><thead><tr>'
					+'<th align="center" colspan="7" border="1px solid black;">Vendor</th>'
					+'<th align="center" colspan="4" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Date</th>'
					//+'<th align="center" colspan="7" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Receipt</th>'
					+'<th align="center" colspan="6" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Item</th>'
					+'<th align="center" colspan="7" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">TGW Legacy#</th>'
					+'<th align="center" colspan="7" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">WMK Legacy#</th>'
					+'<th align="center" colspan="7" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Drawing#</th>'		
					+'<th align="center" colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;white-space:nowrap;">Quantity</th>'
					+'<th align="center" colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Bin</th></tr></thead>'
					//+'<th align="center" colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;white-space:nowrap;">Preferred<br/>Bin</th>';
          	
          	
          	if(lines && lines.length){
               	//nlapiLogExecution('Debug','lines',lines.length);
			
               	for(var i=0;i<lines.length;i++){
               		xml+='<tr><td colspan="7" style="border-right:1px solid black;border-bottom:1px solid black;border-left:1px solid black;">'+nlapiEscapeXML(lines[i].vendor)+'</td>';
               		xml+='<td colspan="4" style="border-right:1px solid black;border-bottom:1px solid black;" >'+nlapiEscapeXML(lines[i].date)+'</td>';
               		//xml+='<td colspan="7" style="border-right:1px solid black;border-bottom:1px solid black;" >'+lines[i].receipt+'</td>';
               		xml+='<td colspan="6" style="border-right:1px solid black;border-bottom:1px solid black;" >'+nlapiEscapeXML(lines[i].item)+'</td>';
               		xml+='<td colspan="7" style="border-right:1px solid black;border-bottom:1px solid black;" >'+lines[i].legacyNumber+'</td>';
               		xml+='<td colspan="7" style="border-right:1px solid black;border-bottom:1px solid black;" >'+lines[i].wmkLegacyNumber+'</td>';
               		xml+='<td colspan="7" style="border-right:1px solid black;border-bottom:1px solid black;" >'+lines[i].drawingNumber+'</td>';
               		xml+='<td colspan="3" align="center" style="border-right:1px solid black;border-bottom:1px solid black;" >'+lines[i].qty+'</td>';
               		xml+='<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;" >'+lines[i].bin+'</td></tr>';
               		//xml+='<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;" ><!--'+nlapiEscapeXML(lines[i].preferredBin)+'--></td>';
  
               	}
               	xml+='</table><hr /></body></pdf>';
               	var file = nlapiXMLToPDF(xml);
               	response.setContentType('PDF', "Today's_ItemReceipt_", 'inline');
               	
               	response.write(file.getValue());
         	}
          else{
        	  response.write("<center><h1 style='color:red;'> No Data Available For Printing</h1></center>");
          }
		}catch(e)
		{
			response.write('Error Occured while generating Pdf. NetSuite error: '+e.message);
		}

	}else {
		var form=nlapiCreateForm("Filter Item Receipts");
		/*-------------------------------------------------------------------------------------------------------*/
		//Added and modifed By Shashank on 11-07-2019
		// filters group tab and location multi select field
		form.addFieldGroup('custpage_filters', 'Availabe Filters');
		form.addField('custpage_fromdate', 'date', 'From',null,'custpage_filters').setDefaultValue(new Date());
		form.addField('custpage_todate', 'date', 'To',null,'custpage_filters').setDefaultValue(new Date());
		form.addField('custpage_location', 'multiselect', 'Location', 'location','custpage_filters');
		/*-------------------------------------------------------------------------------------------------------*/

		
		form.addSubmitButton('Print Receipt');
		//form.setScript('customscript_cntm_setmandatory_cli');
		//form.addButton('custpage_filtered_receipts', 'Submit','generatePDF()');
		response.writePage(form);
	}
}


function getSavedSearchObject(fromDate,ToDate,locationList){
	
	var filters=[];
	filters.push(new nlobjSearchFilter("trandate",null,"within",fromDate,ToDate));
	/*------------------------Added By Shashank 11-07-2019 ----------------------------------------------------------*/
	if(locationList){
		nlapiLogExecution('DEBUG', 'Adding Location Filter', '***');
		filters.push(new nlobjSearchFilter("location",null,"anyof",locationList));
		nlapiLogExecution('DEBUG', 'FILTERS ****', JSON.stringify(filters));
	}
	/*-----------------------------------------------------------------------------------------------------------------*/
	var results=loadRecords(null,searchId,filters,null);
	
	var dataObjectArr=new Array();
	for(var linenum=0;linenum<results.length;linenum++){
		//nlapiLogExecution('DEBUG', '','LineNum: '+linenum);
		var dataObject={};
		dataObject.vendor=results[linenum].getValue(new nlobjSearchColumn("altname","vendor","GROUP"));
		dataObject.date=results[linenum].getValue(new nlobjSearchColumn("trandate",null,"GROUP").setSort(false));
		dataObject.receipt=results[linenum].getValue(new nlobjSearchColumn("tranid",null,"GROUP"));
		dataObject.item=results[linenum].getText(new nlobjSearchColumn("item",null,"GROUP"));
		dataObject.qty=results[linenum].getValue(new nlobjSearchColumn("quantity",null,"GROUP"));
		dataObject.bin=results[linenum].getValue(new nlobjSearchColumn("binnumber","item","GROUP"));
		dataObject.preferredBin=results[linenum].getValue(new nlobjSearchColumn("preferredbin","item","GROUP"));
		dataObject.drawingNumber=results[linenum].getValue(new nlobjSearchColumn("custitem_tgw_drawing_number","item","GROUP"));
		dataObject.legacyNumber=results[linenum].getValue(new nlobjSearchColumn("custitem_tgw_legacy_item","item","GROUP"));
		dataObject.wmkLegacyNumber=results[linenum].getValue(new nlobjSearchColumn("custitem_cnt_tgw_wmklegacynumber","item","GROUP"));

		  
		dataObjectArr.push(dataObject);
	}
	
	return dataObjectArr;
	
}
