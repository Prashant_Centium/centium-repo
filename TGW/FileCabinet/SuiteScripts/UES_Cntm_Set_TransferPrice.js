/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Jun 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @returns {Void}
 */
function set_transferPrice_afterSubmit(type)
{
	try
	{

		var recordId = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', '', 'Record Id: ' + recordId);

		var recordType = nlapiGetRecordType();
		nlapiLogExecution('DEBUG', '', 'Record Type: ' + recordType);

		var record = nlapiLoadRecord(recordType, recordId);

		var transferPrice = record.getFieldValue('transferprice');
		nlapiLogExecution('DEBUG', '', 'Transfer Price Intially: ' + transferPrice);

		var stdCost = loadStdCost(recordId);
		nlapiLogExecution('DEBUG', '', 'Std Cost: ' + stdCost);

		if (!transferPrice || transferPrice != stdCost)
		{

			if (stdCost)
			{
				record.setFieldValue('transferprice', stdCost);
				nlapiSubmitRecord(record);
			}
		}
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);

	}
}

function loadStdCost(internalId)
{
	var iSearch = nlapiSearchRecord("itemlocationconfiguration", null, [ [ "item", "anyof", internalId ], "AND", [ "location", "anyof", "1" ] ],// For Cincinnati Location Only
	[ new nlobjSearchColumn("name").setSort(false), new nlobjSearchColumn("item"), new nlobjSearchColumn("cost") ]);
	if (iSearch !== null && iSearch !== undefined && iSearch !== '')
	{
		return iSearch[0].getValue(new nlobjSearchColumn("cost"));
	}
	return;
}
