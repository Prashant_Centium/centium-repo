/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 May 2019     Prashant Kumar
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function upadteHistoricalPO(type)
{

	var po_results = loadPOIds();
	for (var line = 0; line < po_results.length; line++)
	{
		var id = po_results[line].getValue(new nlobjSearchColumn('internalid'));
		nlapiLogExecution('DEBUG', '', 'Processing PO: ' + id);
		try
		{
			var record = nlapiLoadRecord('purchaseorder', id);
			nlapiSubmitRecord(record);
		}
		catch (e)
		{
			nlapiLogExecution('ERROR', '', 'Record Id: ' + id + ' could not process due to error ' + e.message, e.code);
			continue;

		}
		var remain = nlapiGetContext().getRemainingUsage();
		nlapiLogExecution('DEBUG', '2 Usage Remaining', remain);
		if (remain < 100)
		{
			// ...yield the script
			var state = nlapiYieldScript();
			// Throw an error or log the yield results
			if (state.status == 'FAILURE')
				throw "Failed to yield script";
			else if (state.status == 'RESUME')
				nlapiLogExecution('DEBUG', 'Resuming script');
		}
	
	}

}

function loadPOIds()
{
	var savedSearchObj = nlapiLoadSearch(null, 'customsearch_cntm_trans_search_on_po');
	var searchResults = savedSearchObj.runSearch();
	var counter = 0;
	var results;
	var resultIndex = 0;
	var resultStep = 1000;
	var resultSet;

	// gather data
	do
	{
		resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
		if (resultIndex == 0)
		{
			results = resultSet;
		}
		else if (resultSet)
		{
			results = results.concat(resultSet);
		}
		resultIndex = resultIndex + resultStep;

		// To yield script
		var remain = nlapiGetContext().getRemainingUsage();
		nlapiLogExecution('DEBUG', '2 Usage Remaining', remain);
		if (remain < 100)
		{
			// ...yield the script
			var state = nlapiYieldScript();
			// Throw an error or log the yield results
			if (state.status == 'FAILURE')
				throw "Failed to yield script";
			else if (state.status == 'RESUME')
				nlapiLogExecution('DEBUG', 'Resuming script');
		}
	}
	while (resultSet.length > 0);
	{
		nlapiLogExecution('DEBUG', 'Length', results.length);
	}

	return results;

}
