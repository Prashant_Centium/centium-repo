/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 Sep 2019     Prashant Kumar
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function processSO_scheduled(type)
{
	try
	{
		var poIds = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_po_ids');
		var reqType = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_type');
		var recordId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_parentId');
		nlapiLogExecution('AUDIT', '', 'Type: ' + type + ' POIds: ' + poIds);

		var msg = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_msg');
		msg = JSON.parse(msg);
		nlapiLogExecution('AUDIT', '', 'Msg: ' + JSON.stringify(msg));

		if (reqType == 'approve')
		{
			createSO(poIds, recordId, msg);
		}
		else if (reqType == "reject")
		{
			rejectPO(poIds, recordId, msg);
		}

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);

	}

}

function rejectPO(poIds, parentRecordId, msg)
{

	poIds = poIds.split(',');
	// nlapiLogExecution('AUDIT', '', 'In Reject: ' + poIds);

	for (var i = 0; i < poIds.length - 1; i++)// poIds has last element as comma
	{
		try
		{
			nlapiLogExecution('AUDIT', '', 'Processed PO: ' + poIds[i]);
			var childRecord = nlapiCreateRecord('customrecord_cntm_icom_so_cr_status');
			childRecord.setFieldValue('custrecord_cntm_so_creation_record', parentRecordId);
			childRecord.setFieldValue('custrecord_cntm_source_transaction', poIds[i]);

			if (nlapiLookupField('purchaseorder', poIds[i], 'approvalstatus', false) == 2)
			{
				nlapiSubmitField('purchaseorder', poIds[i], [ 'approvalstatus' ], [ 1 ], null);
				createNote(poIds[i], msg[poIds[i]]);
				var record = nlapiLoadRecord('purchaseorder', poIds[i]);
				var createdBy = record.getFieldValue('recordcreatedby');
				nlapiLogExecution('AUDIT', '', 'Created By: ' + createdBy);
				var tranId = record.getFieldValue('tranid');
				nlapiLogExecution('AUDIT', '', 'Tran Id: ' + tranId);

				var creator = nlapiLookupField('employee', createdBy, [ 'email', 'supervisor' ], false);
				nlapiLogExecution('AUDIT', '', 'Creator: ' + JSON.stringify(creator));

				var approver = {
					email : "",
					supervisor : ""
				};
				if (creator['supervisor'])
					approver = nlapiLookupField('employee', creator['supervisor'], [ 'email' ], false);

				nlapiSendEmail(nlapiGetContext().getUser(), [ creator['email'], approver['email'] ], 'Approval Status', tranId + ' has been rejected by India Reviewer. Please take corrective actions accordingly.')
				childRecord.setFieldValue('custrecord_cntm_status', 3);
			}
			else
			{
				nlapiLogExecution('AUDIT', '', 'already done');

				childRecord.setFieldValue('custrecord_cntm_status', 4);
			}

			nlapiSubmitRecord(childRecord);

		}
		catch (e)
		{
			childRecord.setFieldValue('custrecord_cntm_error_notes', e.message);
			childRecord.setFieldValue('custrecord_cntm_status', 5);
			nlapiSubmitRecord(childRecord);
		}

	}
	nlapiSubmitField('customrecord_cntm_so_creation_list', parentRecordId, 'custrecord_cntm_record_status', 'Complete', null);

}

function createSO(poIds, parentRecordId, msg)
{

	poIds = poIds.split(',');
	// nlapiLogExecution('AUDIT', '', 'In Approve: ' + poIds);
	for (var i = 0; i < poIds.length - 1; i++)// poIds has last element as comma
	{
		nlapiLogExecution('AUDIT', '', 'Processed PO : ' + poIds[i]);
		processSO(poIds[i], parentRecordId, msg[poIds[i]]);
		// createNote(poIds[i], msg[poIds[i]]);

	}
	nlapiSubmitField('customrecord_cntm_so_creation_list', parentRecordId, 'custrecord_cntm_record_status', 'Complete', null);

}

function processSO(recordId, parentRecordId, msg)
{
	try
	{
		// nlapiLogExecution('AUDIT', '', 'Processing PO: ' + recordId);
		var childRecord = nlapiCreateRecord('customrecord_cntm_icom_so_cr_status');
		childRecord.setFieldValue('custrecord_cntm_so_creation_record', parentRecordId);
		childRecord.setFieldValue('custrecord_cntm_source_transaction', recordId);
		childRecord.setFieldValue('custrecord_cntm_status', 1);

		var record = nlapiLoadRecord('purchaseorder', recordId);
		var entity=record.getFieldValue('entity');
		var customer=nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_uk_customer');
		if(entity!=8297){//UK Vendor Internal Id hardcoded
			customer=nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_us_customer');
		}
		

		if (record.getFieldValue('approvalstatus') == 2 && !record.getFieldValue('custbody_cntm_created_so'))
		{
			var count = record.getLineItemCount('item');
			nlapiLogExecution('AUDIT', '', 'Count: ' + count);

			var salesorder = nlapiCreateRecord('salesorder');
			
			salesorder.setFieldValue('entity', customer);
			salesorder.setFieldValue('subsidiary', nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_subsidiary'));
			salesorder.setFieldValue('location', nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_location'));
			salesorder.setFieldValue('otherrefnum', record.getFieldValue('tranid'));
			// salesorder.setFieldValue('orderstatus', 'B');

			for (var linenum = 1; linenum <= count; linenum++)
			{
				salesorder.selectNewLineItem('item');
				salesorder.setCurrentLineItemValue('item', 'item', record.getLineItemValue('item', 'item', linenum));
				salesorder.setCurrentLineItemValue('item', 'rate', record.getLineItemValue('item', 'rate', linenum));
				salesorder.setCurrentLineItemValue('item', 'quantity', record.getLineItemValue('item', 'quantity', linenum));
				salesorder.setCurrentLineItemValue('item', 'custcol_tgw_so_line_internal_memo', record.getLineItemValue('item', 'custcol_tgw_so_line_internal_memo', linenum));

				salesorder.setCurrentLineItemValue('item', 'createwo', 'T');

				salesorder.commitLineItem('item');

			}

			var id = nlapiSubmitRecord(salesorder);
			if (id)
			{
				// nlapiLogExecution('AUDIT', '', 'Record Created with Id: ' + id);
				nlapiSubmitField('purchaseorder', recordId, [ 'custbody_cntm_created_so' ], [ id ], null);
				nlapiLogExecution('AUDIT', '', 'Sales Order Created Successfully.');

				childRecord.setFieldValue('custrecord_cntm_target_transaction', id);
				childRecord.setFieldValue('custrecord_cntm_status', 2);

			}

		}
		else
		{
			nlapiLogExecution('AUDIT', '', 'N/A');
			childRecord.setFieldValue('custrecord_cntm_status', 4);

		}
		nlapiSubmitRecord(childRecord);

	}
	catch (e)
	{
		childRecord.setFieldValue('custrecord_cntm_error_notes', e.message);
		childRecord.setFieldValue('custrecord_cntm_status', 5);
		nlapiSubmitRecord(childRecord);
	}
}

function createNote(poId, msg)
{
	// Create a Note
	var noterecord = nlapiCreateRecord('note');
	noterecord.setFieldValue('title', 'Comments');
	noterecord.setFieldValue('note', msg);
	noterecord.setFieldValue('entity', nlapiGetContext().getUser());
	noterecord.setFieldValue('transaction', poId);
	nlapiSubmitRecord(noterecord);
}
