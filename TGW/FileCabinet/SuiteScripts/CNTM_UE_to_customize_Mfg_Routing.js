/**
 * Module Description
 * CNTM_UE_to_customize_Mfg_Routing
 * Version    Date            Author           Remarks
 * 1.00       13 May 2019     Shashank Rao    Handled the Manufacturing Routing via a User event
 * 2.00       12-07-2019      Shashank Rao    Copy Instruction while doing make copy
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
// Create two line item Fields on Routing Step Line levels
function userEventBeforeLoad(type, form, request)
{
	var executionContext = nlapiGetContext().getExecutionContext();

	if (executionContext == 'userinterface')
	{
		if (type == 'create' || type == 'copy')
		{

			nlapiLogExecution('DEBUG', '********** userEventBeforeLoad', '');

			nlapiLogExecution('DEBUG', 'in logs', '');

			form.getSubList('routingstep').addField('custpage_name', 'select', 'Operation Name', 'customlist_cnt_router_ops').setMandatory(true);
			form.getSubList('routingstep').addField('custpage_instruction', 'text', 'Instructions');

			var lineField = nlapiGetLineItemField('routingstep', 'operationname');
			lineField.setDisplayType('hidden');

			if (type == 'copy')
			{
				var idToCopyFrom = request.getParameter('id');
//				var idToCopyscrollid = request.getParameter('scrollid');

//				nlapiLogExecution('DEBUG', '**** COPY', 'idToCopyFrom : '+idToCopyFrom + ' || idToCopyscrollid : '+idToCopyscrollid);
				
				var lineCount = nlapiGetLineItemCount('routingstep');
				
				var listMap = {};
				listMap = loadListNameMap();
				for (var index_a = 1; index_a <= lineCount; index_a++)
				{
					var operName = nlapiGetLineItemValue('routingstep', 'operationname', index_a);
					nlapiLogExecution('DEBUG', 'OperName in type ' + type, operName);
					if (listMap[operName])
						nlapiSetLineItemValue('routingstep', 'custpage_name', index_a, listMap[operName]);

				}
				if(idToCopyFrom){
					getDetails(idToCopyFrom);
				}
				
			}
		}
		if (type == 'view')
		{
			var mfgRecId = nlapiGetRecordId();
			form.getSubList('routingstep').addField('custpage_name', 'select', 'Operation Name', 'customlist_cnt_router_ops').setMandatory(true).setDisplayType('hidden');

			form.getSubList('routingstep').addField('custpage_instruction', 'text', 'Instructions');
			// var lineField = nlapiGetLineItemField('routingstep', 'operationname');
			getDetails(mfgRecId);

			// lineField.setDisplayType('hidden');
		}
		if (type == 'edit')
		{
			var mfgRecId = nlapiGetRecordId();
			form.getSubList('routingstep').addField('custpage_name', 'select', 'Operation Name', 'customlist_cnt_router_ops').setMandatory(true);

			form.getSubList('routingstep').addField('custpage_instruction', 'text', 'Instructions');
			var lineField = nlapiGetLineItemField('routingstep', 'operationname');
			getDetails(mfgRecId);
			lineField.setDisplayType('hidden');

		}
	}
	else
	{
	}
	// {
	// if (type == 'create')
	// {
	//
	// nlapiLogExecution('DEBUG', '********** userEventBeforeLoad', '');
	//
	// nlapiLogExecution('DEBUG', 'in logs', '');
	//
	// form.getSubList('routingstep').addField('custpage_name', 'select', 'Operation Name', 'customlist_cnt_router_ops');
	// form.getSubList('routingstep').addField('custpage_instruction', 'text', 'Instructions');
	//
	// var lineField = nlapiGetLineItemField('routingstep', 'operationname');
	// lineField.setDisplayType('hidden');
	// }
	// }

}

// if record is already there than get the data as per the Manufucturing Routing from custom record and set it to line level
function getDetails(recId)
{
	var customRecordSearch = nlapiSearchRecord("customrecord_cntm_line_level_routing_mfg", null, [ [ "custrecord_cntm_mfg_routing_rec_reff.internalid", "anyof", recId ] ], [ new nlobjSearchColumn("custrecord_cntm_mfg_routing_rec_reff"), new nlobjSearchColumn("custrecord_cntm_routing_step_sequence").setSort(false), new nlobjSearchColumn("custrecord_cntm__routing_operation_name"), new nlobjSearchColumn("custrecord_cntm_mfg_rout_instructions") ]);

	if (customRecordSearch)
	{
		if (customRecordSearch.length > 0)
		{
			var map = {};
			for (var index = 0; index < customRecordSearch.length; index++)
			{
				var obj = new Object();
				obj.seq = customRecordSearch[index].getValue('custrecord_cntm_routing_step_sequence');
				obj.operationName = customRecordSearch[index].getValue('custrecord_cntm__routing_operation_name');
				obj.instruction = customRecordSearch[index].getValue('custrecord_cntm_mfg_rout_instructions');

				map[obj.seq] = obj;

			}

			var lineCount = nlapiGetLineItemCount('routingstep');
			
			
			nlapiLogExecution('DEBUG', '','lineCount: '+lineCount);
			
			if (lineCount > 0)
			{
				for (var index_i = 1; index_i <= lineCount; index_i++)
				{
					var seq = nlapiGetLineItemValue('routingstep', 'operationsequence', index_i);
					var obj = map[seq];
                  nlapiLogExecution('AUDIT', '','Seq: '+seq+'Obj: '+JSON.stringify(obj));
					if (obj)
					{
						nlapiSetLineItemValue('routingstep', 'custpage_name', index_i, obj.operationName);
						nlapiSetLineItemValue('routingstep', 'custpage_instruction', index_i, obj.instruction);
					}
					else
					{
						// nlapiLogExecution('DEBUG', 'No Object Found to set on line level', '');
						var operNameMap = {};
						operNameMap = loadListNameMap();
						var recType = nlapiGetRecordType();
						var recId = nlapiGetRecordId();
						
						//
						nlapiLogExecution('DEBUG', '','Record Type: '+recType+'  Record Id: '+recId);
						var mfgRoutRec = nlapiLoadRecord(recType, recId);
						mfgRoutRec.selectLineItem('routingstep', index_i);

						// var operName = mfgRoutRec.getLineItemText('routingstep', 'operationname', index_i);
						var operName = mfgRoutRec.getCurrentLineItemValue('routingstep', 'operationname');

						// var operName = nlapiGetCurrentLineItemText('routingstep', 'operationname');
						nlapiLogExecution('DEBUG', 'No Object Found to set on line level', '' + operName);

						nlapiSetLineItemValue('routingstep', 'custpage_name', index_i, operNameMap[operName]);
						nlapiSetLineItemValue('routingstep', 'custpage_instruction', index_i, '');
						// nlapiSelectLineItem('routingstep', index_i);
						// // var oprName = nlapiGetLineItemText('routingstep', 'operationname', index_i);
						//						
						// var oprName = nlapiGetCurrentLineItemText('routingstep', 'operationname');
						// nlapiLogExecution('DEBUG', 'oprName', oprName);
						// nlapiSetLineItemValue('routingstep', 'custpage_name', index_i, oprName);
						// nlapiSetCurrentLineItemText('routingstep', 'custpage_name', operName, true, false);
						// nlapiSetCurrentLineItemText('routingstep', 'custpage_instruction', '', true, false);
						nlapiCommitLineItem('routingstep');
						// nlapiSetLineItemValue('routingstep', 'custpage_instruction', index_i, '');
					}
				}
				// nlapiRefreshLineItems('routingstep');
			}
		}
	}
	else
	{
		// var lineCount = nlapiGetLineItemCount('routingstep')
		var lineCount = nlapiGetLineItemCount('routingstep');
		if (lineCount > 0)
		{
			for (var index_j = 1; index_j <= lineCount; index_j++)
			{
				var operNameMap = {};
				operNameMap = loadListNameMap();
				var recType = nlapiGetRecordType();
				var recId = nlapiGetRecordId();
				var mfgRoutRec = nlapiLoadRecord(recType, recId);
				mfgRoutRec.selectLineItem('routingstep', index_j);

				var operName = mfgRoutRec.getCurrentLineItemValue('routingstep', 'operationname');

				nlapiLogExecution('DEBUG', 'IN ELSE OF BEFORE LOAD ', '' + operName);

				nlapiSetLineItemValue('routingstep', 'custpage_name', index_j, operNameMap[operName]);
				nlapiSetLineItemValue('routingstep', 'custpage_instruction', index_j, '');

				nlapiCommitLineItem('routingstep');
			}
		}
	}

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @returns {Void}
 */

// Set the standard hidden text field like operation name from our custom select field
function userEventBeforeSubmit(type)
{
	nlapiLogExecution('DEBUG', '********** userEventBeforeSubmit', '');
	var executionContext = nlapiGetContext().getExecutionContext();

	if (executionContext == 'userinterface')
	{
		if (type == 'create')
		{
			var lineCount = nlapiGetLineItemCount('routingstep');
			nlapiLogExecution('DEBUG', 'Lines', 'lineCount : ' + lineCount);
			var listMap = loadCustomList();
			if (lineCount > 0)
			{
				for (var line = 1; line <= lineCount; line++)
				{
					nlapiSelectLineItem('routingstep', line);
					var operationValue = nlapiGetCurrentLineItemValue('routingstep', 'custpage_name');
					var operationName = nlapiGetCurrentLineItemValue('routingstep', 'custpage_name');
					var custpageInstruction = nlapiGetCurrentLineItemValue('routingstep', 'custpage_instruction');

					nlapiLogExecution('DEBUG', 'Value', 'operationValue : ' + operationValue + ' , operationName :' + operationName + ' , custpageInstruction:' + custpageInstruction)
					nlapiSetCurrentLineItemValue('routingstep', 'operationname', listMap[operationName]);
					// var record = nlapiCreateRecord('customrecord_cntm_line_level_routing_mfg', {recordmode:'Dynamic'});
					// rec

					nlapiCommitLineItem('routingstep');
				}
			}
		}
		if (type == 'edit')
		{
			var mgfRecId = nlapiGetRecordId();
			getRecordToBeUpdated(mgfRecId);
		}
		if (type == 'delete')
		{
			var mgfRecId = nlapiGetRecordId();

			var customRecordSearch = nlapiSearchRecord("customrecord_cntm_line_level_routing_mfg", null, [ [ "custrecord_cntm_mfg_routing_rec_reff", "anyof", mgfRecId ] ], [ new nlobjSearchColumn("custrecord_cntm_mfg_routing_rec_reff"), new nlobjSearchColumn("custrecord_cntm_routing_step_sequence").setSort(false), new nlobjSearchColumn("custrecord_cntm__routing_operation_name"), new nlobjSearchColumn("custrecord_cntm_mfg_rout_instructions") ]);
			if (customRecordSearch)
			{
				if (customRecordSearch.length > 0)
				{
					for (var index = 0; index < customRecordSearch.length; index++)
					{
						var recId = customRecordSearch[index].getId();
						nlapiDeleteRecord('customrecord_cntm_line_level_routing_mfg', recId);
					}
				}
			}
		}
	}
	else if (executionContext == 'scheduled' && type == 'edit')
	{
		// var mgfRecId = nlapiGetRecordId();
		// getRecordToBeUpdated(mgfRecId);
	}

}

// In edit mode if record change than edit the custom record
function getRecordToBeUpdated(recId)
{

	// try{

	nlapiLogExecution('DEBUG', '*****getRecordToBeUpdated*****', 'recId : ' + recId);
	var listMap = loadCustomList();

	var customRecordSearch = nlapiSearchRecord("customrecord_cntm_line_level_routing_mfg", null, [ [ "custrecord_cntm_mfg_routing_rec_reff", "anyof", recId ] ], [ new nlobjSearchColumn("custrecord_cntm_mfg_routing_rec_reff"), new nlobjSearchColumn("custrecord_cntm_routing_step_sequence").setSort(false), new nlobjSearchColumn("custrecord_cntm__routing_operation_name"), new nlobjSearchColumn("custrecord_cntm_mfg_rout_instructions") ]);

	if (customRecordSearch)
	{
		if (customRecordSearch.length > 0)
		{
			var map = {};
			for (var index = 0; index < customRecordSearch.length; index++)
			{
				var obj = new Object();
				obj.recId = customRecordSearch[index].getId();
				obj.seq = customRecordSearch[index].getValue('custrecord_cntm_routing_step_sequence');
				obj.operationName = customRecordSearch[index].getValue('custrecord_cntm__routing_operation_name');
				obj.instruction = customRecordSearch[index].getValue('custrecord_cntm_mfg_rout_instructions');
				map[obj.seq] = obj;
			}

			nlapiLogExecution('DEBUG', 'Map', JSON.stringify(map));
			var lineCount = nlapiGetLineItemCount('routingstep');
			nlapiLogExecution('DEBUG', 'lineCount' + lineCount, '');
			if (lineCount > 0)
			{
				var seqMap = {};
				for (var index_j = 1; index_j <= lineCount; index_j++)
				{
					var seq = nlapiGetLineItemValue('routingstep', 'operationsequence', index_j);
					seqMap[seq] = seq;
					
					var setupTime,runRate;
					var setupTime = nlapiGetLineItemValue('routingstep', 'setuptime', index_j);
					var runRate = nlapiGetLineItemValue('routingstep', 'runrate', index_j);
					if(setupTime){
						
					}else{
						setupTime =0;
						
					}
					if(runRate){
						
					}else{
						runRate=0
					}
					

					
					var operationName = nlapiGetLineItemValue('routingstep', 'custpage_name', index_j);
					if (operationName)
					{
						nlapiSetLineItemValue('routingstep', 'operationname', index_j, listMap[operationName]);

					}
					else
					{
						operationName = nlapiGetLineItemValue('routingstep', 'operationname', index_j);
					}
					var instruction = nlapiGetLineItemValue('routingstep', 'custpage_instruction', index_j);
					if (map[seq])
					{
						nlapiLogExecution('DEBUG', 'Map', JSON.stringify(map[seq]));
						nlapiSubmitField('customrecord_cntm_line_level_routing_mfg', map[seq].recId, [ 'custrecord_cntm_routing_step_sequence', 'custrecord_cntm__routing_operation_name', 'custrecord_cntm_mfg_rout_instructions','custrecord_cntm_setup_time_mfg','custrecord_cntm_runtime_mfg' ], [ seq, operationName, instruction, setupTime,runRate ], true);
					}
					else
					{
						nlapiLogExecution('DEBUG', 'Creating new record', '');
						var rec = nlapiCreateRecord('customrecord_cntm_line_level_routing_mfg', {
							recordmode : 'dynamic'
						});

						var obj1 = new Object();
						obj1.recId = recId;
						obj1.seq = seq;
						obj1.operationName = operationName;
						obj1.instruction = instruction;
						map[obj1.seq] = obj1;

						
						
						rec.setFieldValue('custrecord_cntm_mfg_routing_rec_reff', recId);
						rec.setFieldValue('custrecord_cntm_routing_step_sequence', parseInt(seq));
						rec.setFieldValue('custrecord_cntm__routing_operation_name', operationName);
						rec.setFieldValue('custrecord_cntm_mfg_rout_instructions', instruction);
						
						rec.setFieldValue('custrecord_cntm_setup_time_mfg', setupTime);
						rec.setFieldValue('custrecord_cntm_runtime_mfg', runRate);

						
						var id = nlapiSubmitRecord(rec, true, true);
						nlapiLogExecution('DEBUG', 'New Record Created', id);
					}
				}
				var key = new Array();
				key = Object.keys(map);
				var seqKey = new Array();
				seqKey = Object.keys(seqMap);
				var deleteArray = [];
				deleteArray = key.diff(seqKey);

				nlapiLogExecution('DEBUG', 'deletearray', JSON.stringify(deleteArray));
				if (deleteArray)
				{
					for (var index_m = 0; index_m < deleteArray.length; index_m++)
					{
						var obj = new Object();
						obj = map[deleteArray[index_m]];
						nlapiLogExecution('DEBUG', 'deleting rec', JSON.stringify(map[deleteArray[index_m]]));
						nlapiDeleteRecord('customrecord_cntm_line_level_routing_mfg', obj.recId);

					}
				}

			}

		}

	}
	else
	{
		var lineCount = nlapiGetLineItemCount('routingstep');
		nlapiLogExecution('DEBUG', 'lineCount' + lineCount, '');

		if (lineCount > 0)
		{
			var seqMap = {};
			seqMap = loadListNameMap();
			var mgfRoutingRec = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

			for (var index_k = 1; index_k <= lineCount; index_k++)
			{
				
				
//				var seq = nlapiGetLineItemText('routingstep', 'operationsequence', index_k);
				var seq = nlapiGetLineItemValue('routingstep', 'operationsequence', index_k);
				
				var operationName = nlapiGetLineItemValue('routingstep', 'custpage_name', index_k);
				var instruction = nlapiGetLineItemText('routingstep', 'custpage_instruction', index_k);

				nlapiLogExecution('DEBUG', '', 'seq : ' + seq);
//				nlapiLogExecution('DEBUG', '', 'seq1 : ' + seq1);

				nlapiLogExecution('DEBUG', '', 'operationName :' + operationName);
				nlapiLogExecution('DEBUG', '', 'instruction : ' + instruction);

				if (operationName)
				{

					nlapiSetLineItemValue('routingstep', 'operationname', index_k, listMap[operationName]);
				}
				else
				{
					nlapiLogExecution('DEBUG', 'In ELSE', '');
					var sta_OperationName = nlapiGetLineItemText('routingstep', 'operationname', index_k);
					operationName = seqMap[sta_OperationName];
					nlapiLogExecution('DEBUG', 'In ELSE', 'sta_OperationName : ' + sta_OperationName + ' , operationName :' + operationName);

				}
				

				var recId = nlapiGetRecordId();

				var rec = nlapiCreateRecord('customrecord_cntm_line_level_routing_mfg', {
					recordmode : 'dynamic'
				});
				var setupTime,runRate;
				var setupTime = nlapiGetLineItemValue('routingstep', 'setuptime', index_k);
				var runRate = nlapiGetLineItemValue('routingstep', 'runrate', index_k);
				if(setupTime){
					
				}else{
					setupTime =0;
					
				}
				if(runRate){
					
				}else{
					runRate=0
				}
				
				
				rec.setFieldValue('custrecord_cntm_mfg_routing_rec_reff', recId);

				rec.setFieldValue('custrecord_cntm_routing_step_sequence', parseInt(seq));
				rec.setFieldValue('custrecord_cntm__routing_operation_name', operationName);
				rec.setFieldValue('custrecord_cntm_mfg_rout_instructions', instruction);
				
				rec.setFieldValue('custrecord_cntm_setup_time_mfg', setupTime);
				rec.setFieldValue('custrecord_cntm_runtime_mfg', runRate);

				var id = nlapiSubmitRecord(rec, true, true);

				nlapiLogExecution('DEBUG', 'New Record Created while edit because no records found', id);
			}
		}
	}
	// }catch(e){
	// var msg = e.message;
	// nlapiLogExecution('ERROR', 'error', e.message);
	// }

}
// get the difference of two arrays
Array.prototype.diff = function(a)
{
	return this.filter(function(i)
	{
		return a.indexOf(i) < 0;
	});
};
// Load custom List for manufacturing Operation name
function loadCustomList()// get the list values of routing operation
{
	var map = {};
	var col = new Array();
	col[0] = new nlobjSearchColumn('name');
	col[1] = new nlobjSearchColumn('internalId');
	var results = nlapiSearchRecord('customlist_cnt_router_ops', null, null, col);
	for (var i = 0; results != null && i < results.length; i++)
	{
		var res = results[i];
		var listValue = res.getValue('name');
		var listID = res.getValue('internalId');
		nlapiLogExecution('DEBUG', (listValue + ", " + listID));
		map[listID] = listValue;
	}
	return map;

}
// Load custom List for manufacturing Operation name

function loadListNameMap()// get the list values of routing operation
{
	var map = {};
	var col = new Array();
	col[0] = new nlobjSearchColumn('name');
	col[1] = new nlobjSearchColumn('internalId');
	var results = nlapiSearchRecord('customlist_cnt_router_ops', null, null, col);
	for (var i = 0; results != null && i < results.length; i++)
	{
		var res = results[i];
		var listValue = res.getValue('name');
		var listID = res.getValue('internalId');
		// nlapiLogExecution('DEBUG', (listValue + ", " + listID));
		map[listValue] = listID;
	}
	return map;

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve, cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only) dropship, specialorder, orderitems (PO only) paybills (vendor payments)
 * @returns {Void}
 */

// if type is create than create the custom record for it only if it is created from user interface
function userEventAfterSubmit(type)
{
	nlapiLogExecution('DEBUG', '********** userEventAfterSubmit', '');
	var recId = nlapiGetRecordId();
	var executionContext = nlapiGetContext().getExecutionContext();

	nlapiLogExecution('DEBUG', 'recId : ' + recId, '');
	if (type == 'create' && executionContext == 'userinterface')
	{
		var lineCount = nlapiGetLineItemCount('routingstep');
		nlapiLogExecution('DEBUG', 'Lines', 'lineCount : ' + lineCount);

		if (lineCount > 0)
		{
			for (var line = 1; line <= lineCount; line++)
			{
				var operationSeq = nlapiGetLineItemValue('routingstep', 'operationsequence', line);
				var operation = nlapiGetLineItemValue('routingstep', 'operationname', line);
				var operationName = nlapiGetLineItemValue('routingstep', 'custpage_name', line);
				
				
				
				// nlapiSetLineItemValue('routingstep', 'operationname', line, operationName);
				var instruction = nlapiGetLineItemValue('routingstep', 'custpage_instruction', line);
				nlapiLogExecution('DEBUG', 'record Details', 'operationSeq : ' + operationSeq + ' ,operation: ' + operation + ' , operationName:' + operationName + ' , instruction ' + instruction);

				var rec = nlapiCreateRecord('customrecord_cntm_line_level_routing_mfg', {
					recordmode : 'dynamic'
				});
				// rec
				var setupTime,runRate;
				var setupTime = nlapiGetLineItemValue('routingstep', 'setuptime', line);
				var runRate = nlapiGetLineItemValue('routingstep', 'runrate', line);
				if(setupTime){
					
				}else{
					setupTime =0;
					
				}
				if(runRate){
					
				}else{
					runRate=0
				}
				

				rec.setFieldValue('custrecord_cntm_mfg_routing_rec_reff', recId);
				rec.setFieldValue('custrecord_cntm_routing_step_sequence', parseInt(operationSeq));
				rec.setFieldValue('custrecord_cntm__routing_operation_name', operationName);
				rec.setFieldValue('custrecord_cntm_mfg_rout_instructions', instruction);
				// rec.setFieldValue('custrecord_cntm_line_number', line);
				rec.setFieldValue('custrecord_cntm_setup_time_mfg', setupTime);
				rec.setFieldValue('custrecord_cntm_runtime_mfg', runRate);

				var cust_recordId = nlapiSubmitRecord(rec, true, true);
				nlapiLogExecution('DEBUG', 'Record ID ', '-->' + cust_recordId);
			}
		}
	}
	else if (type == 'create' && executionContext == 'csvimport')
	{
	}

	else if (type == 'edit' && executionContext == 'scheduled')
	{
		updateAfterRecordSaved(nlapiGetRecordId());
	}
	// {
	// var mfgRecId = nlapiGetRecordId();
	// var mfgRecType = nlapiGetRecordType();
	// var mfgRoutingRec = nlapiLoadRecord(mfgRecType, mfgRecId);
	// var lineCount = nlapiGetLineItemCount('routingstep');
	// var customListMap = {};
	// customListMap = loadListNameMap();
	// if (lineCount > 0)
	// {
	// for (var line = 1; line <= lineCount; line++)
	// {
	// var operSeq = nlapiGetLineItemValue('routingstep', 'operationsequence', line);
	// var operation = nlapiGetLineItemValue('routingstep', 'operationname', line);
	//				
	// var rec = nlapiCreateRecord('customrecord_cntm_line_level_routing_mfg', {
	// recordmode : 'dynamic'
	// });
	// // rec
	//
	// rec.setFieldValue('custrecord_cntm_mfg_routing_rec_reff', mfgRecId);
	// rec.setFieldValue('custrecord_cntm_routing_step_sequence', parseInt(operSeq));
	// rec.setFieldValue('custrecord_cntm__routing_operation_name', customListMap[operationName]);
	// rec.setFieldValue('custrecord_cntm_mfg_rout_instructions', instruction);
	// // rec.setFieldValue('custrecord_cntm_line_number', line);
	//
	// var cust_recordId = nlapiSubmitRecord(rec, true, true);
	//				
	// }
	//			
	// }
	//		
	//		
	// }

	// {
	// var recId = nlapiGetRecordId();
	// var manufacturingRoutingRec = nlapiLoadRecord('manufacturingrouting', recId);
	// var lineCount = manufacturingRoutingRec.getLineItemCount('routingstep');
	// if(lineCount>0){
	// for(var index=1;index<lineCount;index++){
	// manufacturingRoutingRec.selectLineItem('routingstep', index);
	// var operationSequence = manufacturingRoutingRec.getCurrentLineItemValue('routingstep', 'operationsequence');
	// var standardOperationName = manufacturingRoutingRec.getCurrentLineItemValue('routingstep', 'operationname');
	// // var customInstructions = nlapiGetLineItemText('routingstep', 'custpage_instruction', index);
	//				
	// nlapiLogExecution('DEBUG', 'Line Number : '+index, 'standardOperationName : '+standardOperationName+' , customInstructions : '+customInstructions);
	//			
	// var rec = nlapiCreateRecord('customrecord_cntm_line_level_routing_mfg', {
	// recordmode : 'dynamic'
	// });
	// // rec
	//
	// rec.setFieldValue('custrecord_cntm_mfg_routing_rec_reff', recId);
	// rec.setFieldValue('custrecord_cntm_routing_step_sequence', parseInt(operationSequence));
	// rec.setFieldValue('custrecord_cntm__routing_operation_name', standardOperationName);
	// // rec.setFieldText('custrecord_cntm_mfg_rout_instructions', customInstructions);
	//				
	// // rec.setFieldValue('custrecord_cntm_line_number', line);
	//
	// var cust_recordId = nlapiSubmitRecord(rec, true, true);
	// nlapiLogExecution('DEBUG', 'Record ID Created through CSV', '-->' + cust_recordId);
	//			
	// }
	//			
	// }
	//		
	//		
	// }
}
function updateAfterRecordSaved(recId)
{
	nlapiLogExecution('DEBUG', '*****getRecordToBeUpdated*****', 'recId : ' + recId);
	var listMap = loadCustomList();

	var customRecordSearch = nlapiSearchRecord("customrecord_cntm_line_level_routing_mfg", null, [ [ "custrecord_cntm_mfg_routing_rec_reff", "anyof", recId ] ], [ new nlobjSearchColumn("custrecord_cntm_mfg_routing_rec_reff"), new nlobjSearchColumn("custrecord_cntm_routing_step_sequence").setSort(false), new nlobjSearchColumn("custrecord_cntm__routing_operation_name"), new nlobjSearchColumn("custrecord_cntm_mfg_rout_instructions") ]);

	if (customRecordSearch)
	{
	}
	else
	{
		var mgfRoutingRec = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

		var lineCount = mgfRoutingRec.getLineItemCount('routingstep');
		nlapiLogExecution('DEBUG', 'lineCount' + lineCount, '');

		if (lineCount > 0)
		{
			var seqMap = {};
			seqMap = loadListNameMap();

			for (var index_k = 1; index_k <= lineCount; index_k++)
			{
				// // var seq = nlapiGetLineItemText('routingstep', 'operationsequence', index_k);
				// var operationName = nlapiGetLineItemValue('routingstep', 'custpage_name', index_k);
				// var instruction = nlapiGetLineItemText('routingstep', 'custpage_instruction', index_k);
				var seq, operationName, instruction = '';
				seq = mgfRoutingRec.getLineItemValue('routingstep', 'operationsequence', index_k);
				operationName = mgfRoutingRec.getLineItemValue('routingstep', 'operationname', index_k);
				// instruction = mgfRoutingRec.getLineItemValue('routingstep', 'operationsequence', index_k);

				nlapiLogExecution('DEBUG', '', 'seq' + seq);
				nlapiLogExecution('DEBUG', '', 'operationName' + operationName);
				nlapiLogExecution('DEBUG', '', 'instruction' + instruction);

				// if (operationName)
				// {
				// nlapiSetLineItemValue('routingstep', 'operationname', index_k, listMap[operationName]);
				// }
				// else
				// {
				// nlapiLogExecution('DEBUG', 'In ELSE', '');
				// var sta_OperationName = nlapiGetLineItemText('routingstep', 'operationname', index_k);
				// operationName = seqMap[sta_OperationName];
				// nlapiLogExecution('DEBUG', 'In ELSE', 'sta_OperationName : ' + sta_OperationName + ' , operationName :' + operationName);
				//
				// }
				
				var setupTime,runRate;
				var setupTime = nlapiGetLineItemValue('routingstep', 'setuptime', index_k);
				var runRate = nlapiGetLineItemValue('routingstep', 'runrate', index_k);
				if(setupTime){
					
				}else{
					setupTime =0;
					
				}
				if(runRate){
					
				}else{
					runRate=0
				}
				

				var recId = nlapiGetRecordId();
				var rec = nlapiCreateRecord('customrecord_cntm_line_level_routing_mfg', {
					recordmode : 'dynamic'
				});

				rec.setFieldValue('custrecord_cntm_mfg_routing_rec_reff', recId);
				rec.setFieldValue('custrecord_cntm_routing_step_sequence', parseInt(seq));
				rec.setFieldValue('custrecord_cntm__routing_operation_name', seqMap[operationName]);
				rec.setFieldValue('custrecord_cntm_mfg_rout_instructions', instruction);
				
				rec.setFieldValue('custrecord_cntm_setup_time_mfg', setupTime);
				rec.setFieldValue('custrecord_cntm_runtime_mfg', runRate);

				var id = nlapiSubmitRecord(rec, true, true);
				nlapiLogExecution('DEBUG', 'New Record Created while edit because no records found', id);
			}
		}
	}
	// }catch(e){
	// var msg = e.message;
	// nlapiLogExecution('ERROR', 'error', e.message);
	// }
}
