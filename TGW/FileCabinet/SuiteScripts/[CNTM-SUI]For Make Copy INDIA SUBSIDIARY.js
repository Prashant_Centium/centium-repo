/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Oct 2019     Shashank Rao	BACK END SUITELET FOR SUPPORT FOR MAKE COPY FUNCTIONALITY
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */


function suitelet(request, response){
	try{
		var SUBSIDIARY, LOCATION ,MANUFACTURING_WORK_CENTER, MANUFACTURING_COST_TEMPLATE;

		var context = nlapiGetContext();
		SUBSIDIARY = context.getSetting('SCRIPT', 'custscript_cntm_sub_sui_mk_cpy');
		LOCATION = context.getSetting('SCRIPT', 'custscript_cntm_location_fr_mk_cpy');
//		MANUFACTURING_WORK_CENTER = context.getSetting('SCRIPT', 'custscript_cntm_wrk_cntr_fr_cpy');
		MANUFACTURING_COST_TEMPLATE = context.getSetting('SCRIPT', 'custscript_cntm_mfg_cost_temp_fr_cpy');
		
		nlapiLogExecution('DEBUG', 'PARAM', 'SUBSIDIARY '+SUBSIDIARY+'\n LOCATION: '+LOCATION+'\n MANUFACTURING_WORK_CENTER :'+MANUFACTURING_WORK_CENTER+'\n MANUFACTURING_COST_TEMPLATE:'+MANUFACTURING_COST_TEMPLATE);
		var US_UK_WORKCENTER_MAP = {};
		US_UK_WORKCENTER_MAP = getUS_Uk_Work_Center();

		var INDIA_WORKCENTER_MAP = {};
		INDIA_WORKCENTER_MAP = get_IND_SUBSIDIARY_WORK_CENTER();

		
		var routeId = request.getParameter('recordId');
		
		if(routeId){
			
		}else{
			routeId =request.getParameter('custscript_cntm_mfg_rec_id');
		}
		var oldMfgRoutRec = nlapiLoadRecord('manufacturingrouting', routeId);
		
		
		var newMfgRoutRec = nlapiCreateRecord('manufacturingrouting');
		newMfgRoutRec.setFieldValue('subsidiary', SUBSIDIARY);
		newMfgRoutRec.setFieldValue('name', oldMfgRoutRec.getFieldText('item')+' Ind(NP)');
		newMfgRoutRec.setFieldValue('item', oldMfgRoutRec.getFieldValue('item'));
		newMfgRoutRec.setFieldValue('location', LOCATION);
		newMfgRoutRec.setFieldValue('memo', oldMfgRoutRec.getFieldValue('memo'));
		
		newMfgRoutRec.setFieldValue('isdefault', oldMfgRoutRec.getFieldValue('isdefault'));
		newMfgRoutRec.setFieldValue('isinactive', oldMfgRoutRec.getFieldValue('isinactive'));
		newMfgRoutRec.setFieldValue('autocalculatelag', oldMfgRoutRec.getFieldValue('autocalculatelag'));


		var lineItem = oldMfgRoutRec.getLineItemCount('routingstep');
		for(var j=1 ; j<=lineItem;j++){
			
//			newMfgRoutRec.setLineItemValue('routingstep', 'operationsequence', i, oldMfgRoutRec.getLineItemValue('routingstep', 'operationsequence', i));
////			newMfgRoutRec.setLineItemValue('routingstep', 'manufacturingworkcenter', i, MANUFACTURING_WORK_CENTER);
//			
//			
//			
//			newMfgRoutRec.setLineItemValue('routingstep', 'manufacturingcosttemplate', i,MANUFACTURING_COST_TEMPLATE);
//			newMfgRoutRec.setLineItemValue('routingstep', 'setuptime', i, oldMfgRoutRec.getLineItemValue('routingstep', 'setuptime', i));
//			newMfgRoutRec.setLineItemValue('routingstep', 'runrate', i, oldMfgRoutRec.getLineItemValue('routingstep', 'runrate', i));
//			newMfgRoutRec.setLineItemValue('routingstep', 'operationname', i, oldMfgRoutRec.getLineItemValue('routingstep', 'operationname', i));
	//	
	//	
		
		
		
			newMfgRoutRec.selectNewLineItem('routingstep');
			var usukWrkCenter = US_UK_WORKCENTER_MAP[(oldMfgRoutRec.getLineItemValue('routingstep', 'manufacturingworkcenter', j))];
			var indWrkCenter = INDIA_WORKCENTER_MAP[usukWrkCenter + ' Ind(NP)'];
			nlapiLogExecution('DEBUG', '', indWrkCenter);

			var obj = new Object();
			obj.seq = oldMfgRoutRec.getLineItemValue('routingstep', 'operationsequence', j);
			obj.WrkCenter = indWrkCenter;

			obj.costTemp = MANUFACTURING_COST_TEMPLATE;
			obj.setuptime = oldMfgRoutRec.getLineItemValue('routingstep', 'setuptime', j);
			obj.runRate = oldMfgRoutRec.getLineItemValue('routingstep', 'runrate', j);
			obj.operationName = oldMfgRoutRec.getLineItemValue('routingstep', 'operationname', j);

			nlapiLogExecution('DEBUG', 'OBJ', JSON.stringify(obj));
			newMfgRoutRec.setCurrentLineItemValue('routingstep', 'operationsequence', obj.seq);
			newMfgRoutRec.setCurrentLineItemValue('routingstep', 'operationname', obj.operationName);

			newMfgRoutRec.setCurrentLineItemValue('routingstep', 'manufacturingworkcenter', obj.WrkCenter);
			newMfgRoutRec.setCurrentLineItemValue('routingstep', 'manufacturingcosttemplate', obj.costTemp);
			newMfgRoutRec.setCurrentLineItemValue('routingstep', 'setuptime', obj.setuptime);
			newMfgRoutRec.setCurrentLineItemValue('routingstep', 'runrate', obj.runRate);

			newMfgRoutRec.commitLineItem('routingstep', true);
		
		
		
		
		}
		
		
		var newMfgRoutId = nlapiSubmitRecord(newMfgRoutRec, true, true);
		
		var loadNewRec = nlapiLoadRecord('manufacturingrouting', newMfgRoutId, {recordmode:'dynamic'});

		var routingComponentLines =oldMfgRoutRec.getLineItemCount('routingcomponent');
		nlapiLogExecution('DEBUG', 'LINE COUNT', routingComponentLines);
		for(var k=1;k<=routingComponentLines;k++){
			loadNewRec.setLineItemValue('routingcomponent', 'operationsequencenumber', k, oldMfgRoutRec.getLineItemValue('routingcomponent', 'operationsequencenumber', k));
		}
		var sameRecId = nlapiSubmitRecord(loadNewRec, true, true);

		
		
		
		createCopyInstructionOnLine(routeId,newMfgRoutId);
		nlapiSetRedirectURL('RECORD','manufacturingrouting',sameRecId);

	}catch(e){
		nlapiLogExecution('ERROR', 'Details', 'Error Message: ' + e.message+' Error Code: '+e.code);
		if(e.message=='The Manufacturing Routing name must be unique for this item. Please choose a different name.'){
			throw nlapiCreateError('101', 'The Manufacturing Routing for this record has already been created.', false);
		}
		throw e;

	}
	
}



function createCopyInstructionOnLine(oldRecId,newMfgRoutId){
	var customRecForLineLevel = nlapiSearchRecord("customrecord_cntm_line_level_routing_mfg",null,
			[
			   ["custrecord_cntm_mfg_routing_rec_reff","anyof",oldRecId]
			], 
			[
			   new nlobjSearchColumn("custrecord_cntm_mfg_routing_rec_reff"), 
			   new nlobjSearchColumn("custrecord_cntm_routing_step_sequence"), 
			   new nlobjSearchColumn("custrecord_cntm__routing_operation_name"), 
			   new nlobjSearchColumn("custrecord_cntm_mfg_rout_instructions"), 
			   new nlobjSearchColumn("custrecord_cntm_setup_time_mfg"), 
			   new nlobjSearchColumn("custrecord_cntm_runtime_mfg")
			]
			);
	
	if(customRecForLineLevel)
	for(var i=0;i<customRecForLineLevel.length;i++){
		var rec = nlapiCopyRecord('customrecord_cntm_line_level_routing_mfg', customRecForLineLevel[i].getId());
		rec.setFieldValue('custrecord_cntm_mfg_routing_rec_reff', newMfgRoutId);
		var recId = nlapiSubmitRecord(rec, true, true);
	}
	
}

/**
 * Function used to :GET WORK CENTER MAP FOR US AND UK FIRST GET THE NAME OF WORK CENTER FROM INTERNAL ID
 */
function getUS_Uk_Work_Center()
{
	var map = {};
	var entitygroupSearch = nlapiSearchRecord("entitygroup", null, [ [ "subsidiary", "anyof", "26", "24" ], "AND", [ "ismanufacturingworkcenter", "is", "T" ] ], [ new nlobjSearchColumn("internalid"), new nlobjSearchColumn("groupname").setSort(false), new nlobjSearchColumn("grouptype") ]);

	for (var i = 0; i < entitygroupSearch.length; i++)
	{
		map[entitygroupSearch[i].getId()] = entitygroupSearch[i].getValue('groupname');
	}
	nlapiLogExecution('DEBUG', 'US & UK Work Center ', JSON.stringify(map));
	return map;
}
/**
 * Function used to : GET WORK CENTER MAP FOR US AND UK FIRST GET THE NAME OF WORK CENTER FROM INTERNAL ID
 */
function get_IND_SUBSIDIARY_WORK_CENTER()
{
	var map = {};

	var entitygroupSearch = nlapiSearchRecord("entitygroup", null, [ [ "subsidiary", "anyof", "27" ], "AND", [ "ismanufacturingworkcenter", "is", "T" ] ], [ new nlobjSearchColumn("internalid"), new nlobjSearchColumn("groupname").setSort(false), new nlobjSearchColumn("grouptype") ]);
	for (var i = 0; i < entitygroupSearch.length; i++)
	{
		map[entitygroupSearch[i].getValue('groupname')] = entitygroupSearch[i].getId();

	}
	nlapiLogExecution('DEBUG', 'IND_SUBSIDIARY_WORK_CENTER', JSON.stringify(map));
	return map;

}


