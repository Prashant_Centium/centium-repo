/**
 * Module Description
 * 
 * Version    Date            Author            Remarks
 * 1.00       03 Jul 2019     Prashant Kumar    Initial 
 * 2.00       11 july 2019    Shashank Rao      Change the message 
 * 3.00       19 july 2019    Prashant Kumar    Customized the Pop Up 
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
/**
 * This function shows the pop-up for specific vendors.
 */

//This varaiable contains list of vendors for which pop-up will work. 
var vendorListId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_vendor_list_param');

function imposedTariff_beforeLoad(type, form, request)
{
	try
	{

		nlapiLogExecution('DEBUG', 'Call in Before Load', type);
		nlapiLogExecution('DEBUG', '', 'Vendor List Id: ' + vendorListId);

		var vendorList = nlapiLookupField('customrecord_cntm_impose_tariff', vendorListId, 'custrecord_cntm_vendor_list');
		nlapiLogExecution('DEBUG', '', 'Vendor List: ' + vendorList);
		vendorList = vendorList.split(',');
		if (type == 'view' || type == 'edit' || type == 'copy')
		{
			var recordId = nlapiGetRecordId();
			nlapiLogExecution('DEBUG', '', 'Record Id: ' + recordId);

			var recordType = nlapiGetRecordType();
			nlapiLogExecution('DEBUG', '', 'Record Type: ' + recordType);

			var record = nlapiLoadRecord(recordType, recordId);

			var count = record.getLineItemCount('itemvendor');
			// var alert_value = "<script type='text/javascript'>window.load=setTimeout(function(){alert('Item subject to tariff. Please consult with Sales management before quoting customer pricing.');},500)</script>";
			/*---------------------------------------------------------------------------------------------------*/
			
			/* Message added by shashank
			var alert_value = "<script type='text/javascript'>var 0x386f=['\x6D\x69\x6E\x57\x69\x64\x74\x68','\x4D\x73\x67','\x41\x6C\x65\x72\x74','\x61\x6C\x65\x72\x74'];function customAlert(_0xa428x2,_0xa428x3){Ext[_0x386f[1]][_0x386f[0]]= 400;_0xa428x3= 0xa428x3|| _0x386f[2];Ext[_0x386f[1]][_0x386f[3]](_0xa428x3,_0xa428x2)}window.load=setTimeout(function(){customAlert('NOTE FOR <b>US</b> SALES: Inform customer that\"This item is impacted by changes driven by new US tariff and trade rules. While we appeal for exemption, we are required to pass on a portion of the costs to our customer. This policy will be adjusted upon further approval and exemptions potentially granted by government agencies.\"');},500)</script>";
			---------------------------------------------------------------------------------------------------*/

			// Message added by Prashant
			var alert_value = "<script type='text/javascript'>function customAlert(content,title){Ext.Msg.minWidth=400;title=title||'Alert';Ext.Msg.alert(title,content);}window.load=jQuery( document ).ready(function() {setTimeout(function(){customAlert('NOTE FOR <b>US</b> SALES: Inform customer that \"This item is impacted by changes driven by new US tariff and trade rules. While we appeal for exemption, we are required to pass on a portion of the costs to our customer. This policy will be adjusted upon further approval and exemptions potentially granted by government agencies.\"','Information');},5590)})</script>";
			/*---------------------------------------------------------------------------------------------------*/

			var field = form.addField('custpage_inline_alert', 'inlinehtml', '', null);

			for (var linenum = 1; linenum <= count; linenum++)
			{
				var preferredBin = record.getLineItemValue('itemvendor', 'preferredvendor', linenum);
				var vendorId = record.getLineItemValue('itemvendor', 'vendor', linenum);

				nlapiLogExecution('DEBUG', '', 'Vendor Id on Record: ' + vendorId);

				if (preferredBin == 'T' && vendorList.indexOf(vendorId) > -1)
				{
					field.setDefaultValue(alert_value);
					break;

				}

			}

		}

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', 'Error Occurred!', e.message);
	}
}
