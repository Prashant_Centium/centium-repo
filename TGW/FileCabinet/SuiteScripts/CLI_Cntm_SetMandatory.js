/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @param {String}
 *            name Field internal id
 * @param {Number}
 *            linenum Optional line item number, starts from 1
 * @returns {Boolean} True to continue changing field value, false to abort value change
 */
/*
 * function setMandatory_saveRecord(type, name, linenum) { try { // debugger;
 * 
 * var completeQty = nlapiGetFieldValue('completedquantity'); nlapiLogExecution('DEBUG', '', 'Complete Qty: ' + completeQty);
 * 
 * if (!completeQty) { alert('Please enter value(s) for: Completed Quantity'); return false; } return true; } catch (e) { nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);
 *  }
 *  }
 */
function generatePDF()
{

	try
	{

		var fromDate = nlapiGetFieldValue('custpage_fromdate');
		nlapiLogExecution('DEBUG', '', 'From Date: ' + fromDate);

		var toDate = nlapiGetFieldValue('custpage_todate');
		nlapiLogExecution('DEBUG', '', 'To Date: ' + toDate);

		var url = nlapiResolveURL('SUITELET', 'customscript_cntm_itemreceipts_pdf', 'customdeploy_cntm_itemreceipts_pdf') + '&fromDate=' + fromDate + '&toDate=' + toDate + '&method=post';
		
		window.location.href=url;
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);

	}

}
