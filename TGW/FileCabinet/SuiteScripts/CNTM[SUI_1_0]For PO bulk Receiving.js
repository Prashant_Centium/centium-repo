/**
 * Module Description
 * CNTM[SUI_1_0]For PO bulk Receiving
 * Version    Date            Author           Remarks
 * 1.00       15 Jul 2019     Shashank Rao
 * 1.1		  25 Jul 2019     Prashant Kumar   New Changes made
 * 1.2		  13 Aug 2019	  Prashant Kumar   Update for Order Line Number			
 * 1.3		  20 Aug 2019     Prashant Kumar   New Changes for Order Type	
 * 1.4		  05 Sep 2019	  Prashant Kumar   Added Memo field	
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response)
{
	if (request.getMethod() == 'GET')
	{
		try
		{
			nlapiLogExecution('DEBUG', '**************IN GET', 'EXECUTION STARTS');
			var vendorNameParam = request.getParameter('vendorName');
			var poId_param = request.getParameter('poId');
			var fromDateParam = request.getParameter('fromDate');
			var toDateParam = request.getParameter('toDate');

			nlapiLogExecution('DEBUG', 'params', 'vendorNameParam : ' + vendorNameParam/* + ' ,dateParam: ' + dateParam + ' ,postingPeriodParam: ' + postingPeriodParam */)

			var form = nlapiCreateForm('TGW BULK RECEIVING');
			form.addFieldGroup('custpage_filters', 'FILTERS');

			form.setScript('customscript_cntm_cli_for_po_bulk_rcv');

			var vendorField = form.addField('custpage_vendor', 'select', 'VENDOR', 'vendor', 'custpage_filters').setLayoutType('startrow', 'startcol');

			var fromDate = form.addField('custpage_fromdate', 'date', 'FROM DATE', null, 'custpage_filters').setLayoutType('startrow', 'startcol').setDefaultValue(fromDateParam);

			var selectOrderNumber = form.addField('custpage_select_ordernumber', 'text', 'SELECT ORDER NUMBER', 'custpage_filters');

			var toDate = form.addField('custpage_todate', 'date', 'TO DATE', null, 'custpage_filters').setLayoutType('startrow', 'startcol').setDefaultValue(toDateParam);

			if (vendorNameParam)
			{
				vendorField.setDefaultValue(vendorNameParam);
			}
			var vendorSelected;
			if (vendorNameParam || poId_param || fromDateParam || toDateParam)
			{

				var arrayList = getData(vendorNameParam, poId_param, fromDateParam, toDateParam);

				if (arrayList)
				{
					form.addButton('custpage_reset', 'Reset', 'resets()');
					form.addSubmitButton('Submit');

					var subTab = form.addSubTab('custpage_purchase_order_details', 'Purchase Order Details');

					/* =========================================== */
					var sublist = form.addSubList('custpage_po_sublist', 'list', 'Purchase Order Details', 'custpage_purchase_order_details');
					sublist.addMarkAllButtons();
					sublist.addField('custpage_internal_id', 'text', 'InternalId').setDisplayType('hidden');
					sublist.addField('custpage_receive', 'checkbox', 'RECEIVE');

					// New Field added for In-Transit
					sublist.addField('custpage_intransit', 'checkbox', 'IN TRANSIT').setDefaultValue('F');

					sublist.addField('custpage_date', 'Date', 'DATE ').setDisplayType('inline');
					//
					sublist.addField('custpage_shipdate', 'Date', 'VENDOR SHIP DATE ').setDisplayType('inline');

					sublist.addField('custpage_item', 'select', 'ITEM', 'item').setDisplayType('inline');
					sublist.addField('custpage_po', 'text', 'PO #').setDisplayType('inline');
					// New Change to make PO# as Link
					// sublist.addField('custpage_po', 'select', 'PO #','purchaseorder').setDisplayType('normal');
					sublist.addField('custpage_url', 'url', 'Link').setLinkText('VIEW');

					sublist.addField('custpage_created_from', 'text', 'Created From').setDisplayType('inline');

					// New Change For Order Type
					sublist.addField('custpage_ordertype', 'text', 'Order Type').setDisplayType('inline');
					
					sublist.addField('custpage_externalid', 'text', 'Vantage PO #').setDisplayType('inline');

					sublist.addField('custpage_linenumber', 'text', 'Line Number').setDisplayType('hidden');
					sublist.addField('custpage_vendorname', 'select', 'VENDOR NAME ', 'vendor').setDisplayType('inline');
					sublist.addField('custpage_bill_to', 'textarea', 'BILL TO ').setDisplayType('inline');
					//
					sublist.addField('custpage_part_description', 'textarea', 'Part Description').setDisplayType('inline');
					sublist.addField('custpage_currency', 'select', 'CURRENCY', 'currency').setDisplayType('inline');

					sublist.addField('custpage_line_amount', 'currency', 'Line Amount').setDisplayType('inline');
					sublist.addField('custpage_available_qty', 'text', 'Available Quantity').setDisplayType('inline');
					sublist.addField('custpage_qty', 'integer', 'Quantity').setDisplayType('entry');
					
					// Added memo here
					sublist.addField('custpage_memo', 'text', 'Memo').setDisplayType('entry');


					sublist.setLineItemValues(arrayList);
				}
				else
				{
					form = nlapiCreateForm('NOTICE');
					form.addField('custpage_notice', 'textarea', 'NOTICE').setDefaultValue('NO DATA FOUND FOR THIS VENDOR.').setDisplayType('inline');
				}

			}

			response.writePage(form);

			nlapiLogExecution('DEBUG', '**************IN GET', 'EXECUTION ENDS*****');

		}
		catch (e)
		{
			response.write('IN GET : ' + e.message);
		}
	}
	else
	{
		try
		{
			nlapiLogExecution('DEBUG', '**************IN POST', 'EXECUTION STARTS');

			var po_Id_map = {};
			var po_id_list = [];
			var vendorName = request.getParameter('custpage_vendor');

			var lineCount = request.getLineItemCount('custpage_po_sublist');

			var bulk_Rcv_Rec = nlapiCreateRecord('customrecord_cntm_bulk_rcv_reff');

			bulk_Rcv_Rec.setFieldValue('custrecord_cntm_blk_ref_created_by', nlapiGetUser());

			if (vendorName)
			{
				bulk_Rcv_Rec.setFieldValue('custrecord_cntm_blk_rcv_vendor_name', vendorName);
			}
			bulk_Rcv_Rec.setFieldValue('custrecord_cntm_blk_rcv_status', 1);

			var bulk_Rcv_Rec_Id = nlapiSubmitRecord(bulk_Rcv_Rec, true, true);

			if (lineCount > 0)
			{
				for (var index = 1; index <= lineCount; index++)
				{
					var checkBox = request.getLineItemValue('custpage_po_sublist', 'custpage_receive', index);

					if (checkBox == 'T')
					{
						var obj = new Object();
						var po_Id = request.getLineItemValue('custpage_po_sublist', 'custpage_internal_id', index);
						obj.po_Id = po_Id;
						obj.item_Id = request.getLineItemValue('custpage_po_sublist', 'custpage_item', index);
						obj.line = request.getLineItemValue('custpage_po_sublist', 'custpage_linenumber', index);
						obj.qty = request.getLineItemValue('custpage_po_sublist', 'custpage_qty', index);
						
						// Added memo here
						obj.memo = request.getLineItemValue('custpage_po_sublist', 'custpage_memo', index);

						
						// New change to add In Transit Checkbox
						obj.intransit = request.getLineItemValue('custpage_po_sublist', 'custpage_intransit', index);
						
						po_id_list.push(obj);

					}
				}
			}

			po_Id_map[bulk_Rcv_Rec_Id] = po_id_list;

			var tempFile = nlapiCreateFile('Bulk_Po_Receiving', 'PLAINTEXT', JSON.stringify(po_Id_map));
			var folderId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_folder_id_for_blk_rcv');

			// tempFile.setFolder(94766);
			tempFile.setFolder(folderId);
			var fileId = nlapiSubmitFile(tempFile);
			nlapiLogExecution('DEBUG', 'MAP in POST', JSON.stringify(po_Id_map));

			nlapiLogExecution('DEBUG', 'fileId', fileId);

			nlapiScheduleScript('customscript_cntm_bulk_receiving_from_po', 'customdeploy_cntm_for_bulk_rcving', {
				custscript_cntm_po_id_list : fileId
			});

			nlapiSetRedirectURL('RECORD', 'customrecord_cntm_bulk_rcv_reff', bulk_Rcv_Rec_Id);
			nlapiLogExecution('DEBUG', '**************IN POST', 'EXECUTION ENDS*****');
		}
		catch (e)
		{
			response.write('IN POST : ' + e.message);

		}

	}

}

function getPeriod(postingPeriod)
{
	var purchaseorderSearch = nlapiSearchRecord("purchaseorder", null, [ [ "type", "anyof", "PurchOrd" ] ], [ new nlobjSearchColumn("postingperiod", null, "GROUP") ]);

	if (purchaseorderSearch)
	{
		for (var index = 0; index < purchaseorderSearch.length; index++)
		{
			var postingPeriodValue = purchaseorderSearch[index].getValue("postingperiod", null, "GROUP");
			var postingPeriodText = purchaseorderSearch[index].getText("postingperiod", null, "GROUP");

			postingPeriod.addSelectOption(postingPeriodValue, postingPeriodText);

		}
	}

}

function getData(vendor, poId, fromDate, toDate)
{

	/*
	 * 
	 * new nlobjSearchColumn("type"), new nlobjSearchColumn("trandate"), new nlobjSearchColumn("shipdate"), new nlobjSearchColumn("entity"), new nlobjSearchColumn("tranid").setSort(false), new nlobjSearchColumn("externalid"), new nlobjSearchColumn("linesequencenumber").setSort(false), new nlobjSearchColumn("item"), new nlobjSearchColumn("quantity"), new nlobjSearchColumn("quantityshiprecv"), new nlobjSearchColumn("quantityuom"), new nlobjSearchColumn("shiprecvstatusline"), new nlobjSearchColumn("billaddress"), new nlobjSearchColumn("memo"), new nlobjSearchColumn("total"), new nlobjSearchColumn("currency"), new nlobjSearchColumn("postingperiod"), new nlobjSearchColumn("internalid","vendor",null), new nlobjSearchColumn("salesdescription","item",null), new nlobjSearchColumn("grossamount"), new nlobjSearchColumn("fxamount")
	 * 
	 */

	var arrayList = [];
	var filters = new Array();
	var col = new Array();

	var loadSavedSearch = nlapiLoadSearch(null, 'customsearch_cntm_po_search_for_bulk_rcv');// NEW 1 SEARCH : - customsearch_new_vendbill_vendcrdt_srch //NEW SEARCH : - customsearch_vendbill_vendcrdt_srch // OLD SEARCH : - customsearch_cntm_vendor_bill_savesearch
	if (vendor)
	{
		filters.push(nlobjSearchFilter('name', null, 'anyof', vendor));
	}
	if (poId)
	{
		filters.push(nlobjSearchFilter('internalid', null, 'anyof', poId));

	}
	if (fromDate)
	{
		filters.push(nlobjSearchFilter('trandate', null, 'onorafter', fromDate));

	}
	if (toDate)
	{
		filters.push(nlobjSearchFilter('trandate', null, 'onorbefore', toDate));

	}
	else
	{
		filters.push(nlobjSearchFilter('trandate', null, 'onorbefore', new Date()));
	}

	loadSavedSearch.addFilters(filters);

	var searchResults = loadSavedSearch.runSearch();

	var resultIndex = 0;
	var resultStep = 1000;

	var resultSet = [];
	do
	{
		var temp = []; // temporary variable used to store the result set
		temp = searchResults.getResults(resultIndex, resultIndex + resultStep);

		resultSet = resultSet.concat(temp);
		resultIndex = resultIndex + resultStep;
	}
	while (temp.length > 0);
	nlapiLogExecution('DEBUG', 'result', resultSet.length);
	for (var counter_i = 0; counter_i < resultSet.length; counter_i++)
	{
		var obj = new Object();
		obj.custpage_internal_id = resultSet[counter_i].getId();
		obj.custpage_date = resultSet[counter_i].getValue('trandate');

		obj.custpage_shipdate = resultSet[counter_i].getValue('shipdate');
		obj.custpage_externalid = resultSet[counter_i].getValue('externalid');

		obj.custpage_po = resultSet[counter_i].getValue('tranid');
		// New Change to make PO# Link
		// obj.custpage_po = '<a href="'+ nlapiResolveURL('RECORD', 'purchaseorder', resultSet[counter_i].getId()) + '">'+resultSet[counter_i].getValue('tranid') +'</a>';
		obj.custpage_url = nlapiResolveURL('RECORD', 'purchaseorder', resultSet[counter_i].getId());
		obj.custpage_item = resultSet[counter_i].getValue('item');
		//obj.custpage_linenumber = resultSet[counter_i].getValue('linesequencenumber');
		
		// New Change to resolve line level error due to line number inconsistency
		obj.custpage_linenumber = resultSet[counter_i].getValue('line');
		
		// New Change added for SO order type
		obj.custpage_ordertype=resultSet[counter_i].getText(new nlobjSearchColumn("custbodycustom_tgw_order_type","createdFrom",null));

		var rcvQty = parseInt(resultSet[counter_i].getValue('quantityshiprecv'));

		var qtyInTransUnits = parseInt(resultSet[counter_i].getValue('quantityuom'));

		var multple = parseInt(resultSet[counter_i].getValue('quantity')) / qtyInTransUnits;

		obj.custpage_available_qty = parseInt((parseInt(resultSet[counter_i].getValue('quantity')) - rcvQty) / multple).toFixed(0);
		obj.custpage_qty = parseInt((parseInt(resultSet[counter_i].getValue('quantity')) - rcvQty) / multple).toFixed(0);

		// obj.custpage_vendorname = resultSet[counter_i].getValue('entity');

		obj.custpage_vendorname = resultSet[counter_i].getValue("internalid", "vendor", null);

		obj.custpage_bill_to = resultSet[counter_i].getValue('billaddress');
		obj.custpage_part_description = resultSet[counter_i].getValue("salesdescription", "item", null);
		obj.custpage_line_amount = resultSet[counter_i].getValue('fxamount');
		obj.custpage_currency = resultSet[counter_i].getValue('currency');
		obj.custpage_created_from = resultSet[counter_i].getText('createdfrom');

		if (obj.custpage_available_qty > 0)
		{
			arrayList.push(obj);

		}

	}
	nlapiLogExecution('DEBUG', 'arrayList : ' + arrayList.length, JSON.stringify(arrayList));
	return arrayList;

}
