/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 Jun 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function restrict_record_creation_beforeLoad(type, form, request)
{
	try
	{
		if (type == 'create' || type == 'copy')
		{// to prevent from creation and make copy of record if record exist

			var recordType = nlapiGetRecordType();

			// For Multiple Bins Configuration
			var result = nlapiSearchRecord(recordType, null);
			validateForCreation(result, recordType)

			// For Impose Tariff
			result = nlapiSearchRecord(recordType, null);
			validateForCreation(result, recordType)

		}

	}
	catch (e)
	{
		if (e.code == '001')
		{
			throw e;
		}
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);

	}

}

function validateForCreation(result, recordType)
{
	nlapiLogExecution('DEBUG', '', 'Record Length: ' + result.length + ' Record Type: ' + recordType);

	if (result)
	{
		var error = nlapiCreateError('001', 'Configuration Record already exists.', false);
		throw error;
	}
}
