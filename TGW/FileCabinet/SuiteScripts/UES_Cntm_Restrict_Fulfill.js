/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 Aug 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function restrict_fulfill_beforeLoad(type, form, request)
{
	try
	{
		var count=nlapiGetLineItemCount('item');
		
		for(var line=1;line<=count;line++){
			var itemText=nlapiGetLineItemText('item', 'item', line);
			var itemValue=nlapiGetLineItemValue('item', 'item', line);
			
			if(line){//check(itemValue)
				// Message added by Prashant
				var alert_value = "<script type='text/javascript'>function customAlert(content,title){Ext.Msg.minWidth=400;title=title||'Alert';Ext.Msg.alert(title,content);}window.load=jQuery( document ).ready(function() {setTimeout(function(){customAlert('Item " + itemText + " on this Sales Order has inventory that is in a restricted bin and cannot be fulfilled.','Information');},5590)})</script>";
				/*---------------------------------------------------------------------------------------------------*/

				var field = form.addField('custpage_inline_alert', 'inlinehtml', '', null);

			}

		}
		
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);
	}

}
