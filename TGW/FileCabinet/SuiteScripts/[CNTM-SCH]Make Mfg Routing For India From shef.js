/**[CNTM-SCH]Make Mfg Routing For India From shef
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Oct 2019     Shashank Rao		For Creating Bulk Manufacturing Routing For India Subsidiary
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted,
 *            skipped
 * @returns {Void}
 */

function scheduled(type) {
	try {

		/**
		 * Script Parameters
		 */
		var SUBSIDIARY, LOCATION, MANUFACTURING_COST_TEMPLATE;

		var context = nlapiGetContext();
		SUBSIDIARY = context.getSetting('SCRIPT',
				'custscript_cntm_india_sub_fr_copy');
		LOCATION = context.getSetting('SCRIPT',
				'custscript_cntm_location_mk_cpy');
		MANUFACTURING_COST_TEMPLATE = context.getSetting('SCRIPT',
				'custscript_cntm_mfg_cst_template_mk_cpy');

		var US_UK_WORKCENTER_MAP = {};
		US_UK_WORKCENTER_MAP = getUS_Uk_Work_Center();

		var INDIA_WORKCENTER_MAP = {};
		INDIA_WORKCENTER_MAP = get_IND_SUBSIDIARY_WORK_CENTER();

		var dataArray = [];

		dataArray = getSavedSearchResult();
		// dataArray.push(18438);//Hardcoded
		if (dataArray) {
			for (var i = 0; i < dataArray.length; i++)// dataArray.length
			{

				try {
					// routeId = dataArray[i];
					routeId = dataArray[i].getId();

					nlapiLogExecution('DEBUG', 'ROUT ID', routeId);
					if (routeId) {

						var oldMfgRoutRec = nlapiLoadRecord(
								'manufacturingrouting', routeId);

						var newMfgRoutRec = nlapiCreateRecord(
								'manufacturingrouting', {
									recordmode : 'dynamic'
								});

						newMfgRoutRec.setFieldValue('isdefault', oldMfgRoutRec
								.getFieldValue('isdefault'));
						newMfgRoutRec.setFieldValue('isinactive', oldMfgRoutRec
								.getFieldValue('isinactive'));
						newMfgRoutRec
								.setFieldValue(
										'autocalculatelag',
										oldMfgRoutRec
												.getFieldValue('autocalculatelag'));

						newMfgRoutRec.setFieldValue('subsidiary', SUBSIDIARY);
						newMfgRoutRec.setFieldValue('name', oldMfgRoutRec
								.getFieldText('item')
								+ ' Ind(NP)');
						newMfgRoutRec.setFieldValue('item', oldMfgRoutRec
								.getFieldValue('item'));

						newMfgRoutRec.setFieldValue('location', LOCATION);
						newMfgRoutRec.setFieldValue('memo', oldMfgRoutRec
								.getFieldValue('memo'));
						var lineItem = oldMfgRoutRec
								.getLineItemCount('routingstep');
						for (var j = 1; j <= lineItem; j++) {

							var usage = nlapiGetContext().getRemainingUsage();
							if (usage < 1000) {
								nlapiYieldScript();
							}
							newMfgRoutRec.selectNewLineItem('routingstep');
							var usukWrkCenter = US_UK_WORKCENTER_MAP[(oldMfgRoutRec
									.getLineItemValue('routingstep',
											'manufacturingworkcenter', j))];
							var indWrkCenter = INDIA_WORKCENTER_MAP[usukWrkCenter
									+ ' Ind(NP)'];
							nlapiLogExecution('DEBUG', '', indWrkCenter);

							var obj = new Object();
							obj.seq = oldMfgRoutRec.getLineItemValue(
									'routingstep', 'operationsequence', j);
							obj.WrkCenter = indWrkCenter;

							obj.costTemp = MANUFACTURING_COST_TEMPLATE;
							obj.setuptime = oldMfgRoutRec.getLineItemValue(
									'routingstep', 'setuptime', j);
							obj.runRate = oldMfgRoutRec.getLineItemValue(
									'routingstep', 'runrate', j);
							obj.operationName = oldMfgRoutRec.getLineItemValue(
									'routingstep', 'operationname', j);

							nlapiLogExecution('DEBUG', 'OBJ', JSON
									.stringify(obj));
							newMfgRoutRec
									.setCurrentLineItemValue('routingstep',
											'operationsequence', obj.seq);
							newMfgRoutRec.setCurrentLineItemValue(
									'routingstep', 'operationname',
									obj.operationName);

							newMfgRoutRec.setCurrentLineItemValue(
									'routingstep', 'manufacturingworkcenter',
									obj.WrkCenter);
							newMfgRoutRec.setCurrentLineItemValue(
									'routingstep', 'manufacturingcosttemplate',
									obj.costTemp);
							newMfgRoutRec.setCurrentLineItemValue(
									'routingstep', 'setuptime', obj.setuptime);
							newMfgRoutRec.setCurrentLineItemValue(
									'routingstep', 'runrate', obj.runRate);

							newMfgRoutRec.commitLineItem('routingstep', true);
						}

						var newMfgRoutId = nlapiSubmitRecord(newMfgRoutRec,
								true, true);
						createCopyInstructionOnLine(routeId, newMfgRoutId);
						var loadNewRec = nlapiLoadRecord(
								'manufacturingrouting', newMfgRoutId, {
									recordmode : 'dynamic'
								});

						var routingComponentLines = oldMfgRoutRec
								.getLineItemCount('routingcomponent');
						nlapiLogExecution('DEBUG', 'LINE COUNT',
								routingComponentLines);
						for (var k = 1; k <= routingComponentLines; k++) {
							loadNewRec.setLineItemValue('routingcomponent',
									'operationsequencenumber', k, oldMfgRoutRec
											.getLineItemValue(
													'routingcomponent',
													'operationsequencenumber',
													k));
						}
						var sameRecId = nlapiSubmitRecord(loadNewRec, true,
								true);

						nlapiLogExecution(
								'DEBUG',
								'SUCCESS FULLY CREATED MANUFACTURING ROUTING FOR INDIA SUBSIDIARY ****',
								'Internal ID = ' + sameRecId);
					}
				} catch (error) {
					nlapiLogExecution(
							'ERROR',
							'ERROR WHILE CREATING MANUFACTURING ROUTING COPY: ',
							error.message);
				}
			}
		}

	} catch (e) {

		nlapiLogExecution('DEBUG', 'ERROR', e.message);
	}

}
/**
 * Function used to :Used to create the custom record for Instruction on
 * manufacturing Routing
 */
function createCopyInstructionOnLine(oldRecId, newMfgRoutId) {
	var customRecForLineLevel = nlapiSearchRecord(
			"customrecord_cntm_line_level_routing_mfg", null,
			[ [ "custrecord_cntm_mfg_routing_rec_reff", "anyof", oldRecId ] ],
			[
					new nlobjSearchColumn(
							"custrecord_cntm_mfg_routing_rec_reff"),
					new nlobjSearchColumn(
							"custrecord_cntm_routing_step_sequence"),
					new nlobjSearchColumn(
							"custrecord_cntm__routing_operation_name"),
					new nlobjSearchColumn(
							"custrecord_cntm_mfg_rout_instructions"),
					new nlobjSearchColumn("custrecord_cntm_setup_time_mfg"),
					new nlobjSearchColumn("custrecord_cntm_runtime_mfg") ]);

	if (customRecForLineLevel)
		for (var i = 0; i < customRecForLineLevel.length; i++) {
			var rec = nlapiCopyRecord(
					'customrecord_cntm_line_level_routing_mfg',
					customRecForLineLevel[i].getId());
			rec.setFieldValue('custrecord_cntm_mfg_routing_rec_reff',
					newMfgRoutId);
			var recId = nlapiSubmitRecord(rec, true, true);
		}

}
/**
 * Function used to :GET WORK CENTER MAP FOR US AND UK FIRST GET THE NAME OF
 * WORK CENTER FROM INTERNAL ID
 */
function getUS_Uk_Work_Center() {
	var map = {};
	var entitygroupSearch = nlapiSearchRecord("entitygroup", null, [
			[ "subsidiary", "anyof", "26", "24" ], "AND",
			[ "ismanufacturingworkcenter", "is", "T" ] ], [
			new nlobjSearchColumn("internalid"),
			new nlobjSearchColumn("groupname").setSort(false),
			new nlobjSearchColumn("grouptype") ]);

	for (var i = 0; i < entitygroupSearch.length; i++) {
		map[entitygroupSearch[i].getId()] = entitygroupSearch[i]
				.getValue('groupname');
	}
	nlapiLogExecution('DEBUG', 'US & UK Work Center ', JSON.stringify(map));
	return map;
}
/**
 * Function used to : GET WORK CENTER MAP FOR US AND UK FIRST GET THE NAME OF
 * WORK CENTER FROM INTERNAL ID
 */
function get_IND_SUBSIDIARY_WORK_CENTER() {
	var map = {};

	var entitygroupSearch = nlapiSearchRecord("entitygroup", null, [
			[ "subsidiary", "anyof", "27" ], "AND",
			[ "ismanufacturingworkcenter", "is", "T" ] ], [
			new nlobjSearchColumn("internalid"),
			new nlobjSearchColumn("groupname").setSort(false),
			new nlobjSearchColumn("grouptype") ]);
	for (var i = 0; i < entitygroupSearch.length; i++) {
		map[entitygroupSearch[i].getValue('groupname')] = entitygroupSearch[i]
				.getId();

	}
	nlapiLogExecution('DEBUG', 'IND_SUBSIDIARY_WORK_CENTER', JSON
			.stringify(map));
	return map;

}

/**
 * Function used to : Get all the manufacturing routing for Particular location
 */
function getSavedSearchResult() {
	var filters = [];
	var col = [];
	col[col.length] = new nlobjSearchColumn("internalid");
	filters[filters.length] = new nlobjSearchFilter('location', null, 'is',
			'13');
	var search = nlapiCreateSearch('manufacturingrouting', filters, col);
	var searchResults = search.runSearch();
	var resultIndex = 0;
	var resultStep = 1000;
	var resultSet = [];

	do {
		var temp = []; // temporary variable used to store the result set
		temp = searchResults.getResults(resultIndex, resultIndex + resultStep);
		resultSet = resultSet.concat(temp);
		resultIndex = resultIndex + resultStep;
	} while (temp.length > 0);

	return resultSet;
}
