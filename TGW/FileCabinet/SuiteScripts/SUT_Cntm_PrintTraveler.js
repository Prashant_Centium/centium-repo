/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 May 2019     Prashant Kumar
 * 1.1        29 July 2019    Prashant Kumar Changes for MTO
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */

function sut_cntm_printTraveler(request, response){
	if (request.getMethod() == "GET") {
		try{
			// For default Logo
        	var comrec=nlapiLoadConfiguration('companyinformation');
          	var base=fetchURL(comrec.getFieldValue('formlogo'));
          	
          	var recordId=request.getParameter('recordId');
          	nlapiLogExecution('DEBUG','recID',recordId);

          	var recordType=request.getParameter('recordType');
          	var record=nlapiLoadRecord(recordType,recordId);
          	var printDate=getPrintDate();
          	var hostname=request.getParameter('hostname');
          	nlapiLogExecution('DEBUG','Hostname',hostname);
          	//nlapiLogExecution('DEBUG','Hostname from getDataCenterURL(); ',getDataCenterURL());
          	
          	// Added for MTO
          	var isMTO=record.getFieldValue('custbody9');
          	
          		
          	
          	var subsidiaryId=record.getFieldValue('subsidiary');
          	nlapiLogExecution('DEBUG','Subsidiary Id',subsidiaryId);

          	var subRecord=nlapiLoadRecord('subsidiary',subsidiaryId);
          	var logoId=subRecord.getFieldValue('logo');
          	nlapiLogExecution('DEBUG','Logo Id',logoId);

          	if(logoId){
	          	base=fetchURL(logoId);
	          	nlapiLogExecution('DEBUG','ImageURL on Subsidiary',base);
	        }
          	

          	var assemblyItemId=record.getFieldValue('assemblyitem');
          	nlapiLogExecution('DEBUG','Assembly Item Id',assemblyItemId);
          	
          	var assemblyRec=nlapiLoadRecord('assemblyItem', assemblyItemId);
          	
          	var itemImageId=assemblyRec.getFieldValue('custitem_atlas_item_image');
          	nlapiLogExecution('DEBUG','Item Image Id',itemImageId);
          	var wmkLegacyNum=assemblyRec.getFieldValue('custitem_cnt_tgw_wmklegacynumber');
          	nlapiLogExecution('DEBUG','WMK Legacy#',wmkLegacyNum);
          	
          	var itemImageUrl='';
          	if(itemImageId){
              	makeFileAvailOnline(itemImageId);
          		itemImageUrl=fetchURL(itemImageId);
	          	nlapiLogExecution('DEBUG','Item ImageURL',itemImageUrl);

          	}


          	
          	var location=record.getFieldText('location');
          	nlapiLogExecution('DEBUG','Location',location);
          	
          	var workorderDate=record.getFieldValue('trandate');
          	nlapiLogExecution('DEBUG','WO Creation Date',workorderDate);

          	var salesorderId=record.getFieldValue('createdfrom');
          	var salesorderRecord='';
          	var soLineQty='';
          	var expectedDate='';
          	var soLineMemo='';
          	var customer='';
          	//var soClass='';
          	var stockQty='';
          	if(salesorderId){
              	salesorderRecord=nlapiLoadRecord('salesorder', salesorderId);
              	customer=salesorderRecord.getFieldText('entity');
              	for(var linenum=1;linenum<=salesorderRecord.getLineItemCount('item');linenum++){
              		if(salesorderRecord.getLineItemValue('item','woid',linenum)==recordId){
              			soLineQty=salesorderRecord.getLineItemValue('item','quantity',linenum);
              			expectedDate=salesorderRecord.getLineItemValue('item','expectedshipdate',linenum);
              			soLineMemo=salesorderRecord.getLineItemValue('item','custcol_tgw_so_line_internal_memo',linenum);
              			//soClass=salesorderRecord.getLineItemText('item', 'class',linenum);
              			stockQty=salesorderRecord.getLineItemValue('item', 'quantityonhand',linenum);
              			break;
              		}
              	}
          	}
          	
          	
          	var xml = '<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd"><pdfset><pdf>'
          		+'<head><macrolist><macro id="nlheader">'
          		+'<table class="header" style="width: 100%;"><tr><td colspan="2" rowspan="3"><img src="'+base+'" style="float: left; margin: 5px ;height:70px; width:200;"/></td>';
          		
          	if(isMTO=='T'){
          		xml+='<td valign="middle" align="center" margin-top="20px"><span class="title">ROUTE CARD</span></td></tr><tr><td align="center" valign="bottom" style="color:red;font-weight:bold;font-size:40px;">MTO</td></tr>';	
          	}
          	else
          	{
          		xml+='<td valign="middle" align="center"><span class="title">ROUTE CARD</span></td></tr>';
          	}
          		
          		xml+='</table></macro>'
          		+'<macro id="nlfooter"><table class="footer" style="width: 100%;"><tr><td align="right"><pagenumber/> of <totalpages/></td></tr>'
          		+'<tr><td align="center">'
          		+'</td></tr>'
          		+'</table></macro></macrolist>'
          		+'<style type="text/css">'
          		+'table {font-size: 10pt;table-layout: fixed;page-break-inside:auto;}'
          		+'tr {page-break-inside:avoid;page-break-after:auto;}'
          		+'th {font-weight: bold;font-size: 10pt;vertical-align: middle;padding: 5px 6px 3px;background-color: #e3e3e3;color: #333333;}'
          		+'td {padding: 4px 6px;}'
          		+'td p { align:left }b {font-weight: bold;}table.header td {padding: 0;font-size: 10pt;}'
          		+'table.footer td {padding: 0;font-size: 9pt;}'
          		+'table.itemtable th {padding-bottom: 10px;padding-top: 10px;}table.body td {padding-top: 1px;}'
          		+'table.total {page-break-inside: avoid;}tr.totalrow {background-color: #e3e3e3; line-height: 150%;}'
          		+'td.totalboxtop {font-size: 12pt;background-color: #e3e3e3;}'
          		+'td.addressheader {font-size: 10pt;padding-top: 6px;padding-bottom: 2px;}'
          		+'td.address {padding-top: 0;}td.totalboxmid {font-size: 28pt;padding-top: 20px; background-color: #e3e3e3;}'
          		+'td.totalboxbot {background-color: #e3e3e3;font-weight: bold;}'
          		+'span.title {font-size: 18pt;font-weight: bold;}span.number {font-size: 15pt;}'
          		+'span.itemname {font-weight: bold;line-height: 150%;}'
          		+'hr {width: 100%;color: #d3d3d3;background-color: #d3d3d3;height: 1px;}</style></head>'
          		+'<body header="nlheader" header-height="10%" footer="nlfooter" footer-height="40pt" padding="0.5in 0.5in 0.5in 0.5in" size="A4">'
          		/*// Table#1
          		+'<table width="100%" border="1 solid black">'
          		+'<tr><td valign="middle" align="center" style="border-right:1 solid black"><u>Unit Price</u></td>'
          		+'<td valign="middle" align="center" style="border-right:1 solid black"></td>'
          		+'<td colspan="2" valign="middle" align="center" style="border-right:1 solid black"><b>Route Card</b></td>'
          		+'<td valign="middle" align="center" style="border-right:1 solid black"><u>Job Value</u></td>'
          		+'<td valign="middle" align="center"></td></tr></table><br/>'
          		// Table#2
          		+'<table width="100%" cellspacing="1">'
          		+'<tr><th valign="middle" align="center" colspan="2">Location</th>'
          		+'<td colspan="3"></td>'//'+hasValue(location)+'
          		+'<td colspan="2" ></td>'
          		+'<th valign="middle" align="center" colspan="2">Date Printed</th>'
          		+'<td valign="middle" colspan="4">'+getPrintDate()+'</td>'
          		+'</tr><tr><td valign="middle" align="center" colspan="2"></td>'
          		+'<td colspan="3"></td><td colspan="2"></td>'
          		+'<th valign="middle" align="center" colspan="2">Date Created</th>'
          		+'<td valign="middle" colspan="4">'+hasValue(workorderDate)+'</td></tr></table><hr/>'
          		*/
          		
          		// Table #1  Header
          		+'<table width="100%" class="section1" border="1 solid black">'
          		if(salesorderId){
          		xml+='<tr>'
          		+'<td valign="middle" align="center" style="border-right:1 solid black;border-bottom:1 solid black" colspan="1"><b>Customer</b></td>'
          	    +'<td valign="middle" align="center" style="border-right:1 solid black;border-bottom:1 solid black" colspan="2">'+nlapiEscapeXML(customer)+'</td>'
          	    +'<td valign="middle" align="center" style="border-right:1 solid black;border-bottom:1 solid black" colspan="1"><b>Unit Price</b></td>'
          	    +'<td valign="middle" align="center" style="border-right:1 solid black;border-bottom:1 solid black" colspan="1"></td>'
          	    +'<td valign="middle" align="center" style="border-right:1 solid black;border-bottom:1 solid black" colspan="1"><b>Job Value</b></td>'
          	    +'<td valign="middle" align="center" colspan="2" style="border-bottom:1 solid black"></td></tr>';
          	    	
          		}else{
          			xml+='<tr>'
                  		+'<td valign="middle" align="center" style="border-right:1 solid black;border-bottom:1 solid black" colspan="1"><b>Unit Price</b></td>'
                  	    +'<td valign="middle" align="center" style="border-right:1 solid black;border-bottom:1 solid black" colspan="4"></td>'
                  	    +'<td valign="middle" align="center" style="border-right:1 solid black;border-bottom:1 solid black" colspan="1"><b>Job Value</b></td>'
                  	    +'<td valign="middle" align="center" colspan="2" style="border-bottom:1 solid black"></td></tr>';	    	
          		}
          		xml+='<tr>'
          	    +'<td valign="middle" align="center" style="border-right:1 solid black" colspan="1"><b>Location</b></td>'
          	    +'<td valign="middle" align="center" style="border-right:1 solid black" colspan="2"></td>'
          	    +'<td valign="middle" align="center" style="border-right:1 solid black" colspan="1"><b>Date Created</b></td>'
          	    +'<td valign="middle" align="center" style="border-right:1 solid black" colspan="1">'+hasValue(workorderDate)+'</td>'
          	    +'<td valign="middle" align="center" style="border-right:1 solid black" colspan="1"><b>Date Printed</b></td>'
          	    +'<td valign="middle" align="center" colspan="2">'+hasValue(printDate)+'</td></tr>'
          	  	+'</table><hr/>'
          		// Table#2 BOM
          		+'<table width="100%" height="50%" border="1 solid black" cellspacing="1"><tr>'
          		// InnerTable#1
          		+'<td><table width="100%" >'
          		+'<tr><td><b>Descriptions:</b><br/>'+nlapiEscapeXML(hasValue(assemblyRec.getFieldValue('description')))+'<br/><br/><br/><br/></td></tr>'
          		+'<tr><td><b>Rev: </b>'+nlapiEscapeXML(hasValue(assemblyRec.getFieldValue('custitem_tgw_drawing_revision')))+'</td></tr>'
          		+'<tr><td><b>Drawing: </b>'+hasValue(assemblyRec.getFieldValue('custitem_tgw_drawing_number'))+'</td></tr></table></td>'
          		// InnerTable#2
          		+'<td><table width="100%">'
          		+'<tr><th border="1 solid black" colspan="10">CRUSH</th></tr>'
          		+'<tr><td style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" colspan="10"><u>PROGRAMMED FOR</u><br/><br/><br/><br/><br/></td></tr>'
          		+'<tr><td style="border-left:1px solid; border-bottom:1px solid black;" colspan="5"><u>BOOKED</u><br/><br/><br/><br/></td>'
          		+'<td style="border-right:1px solid black;border-bottom:1px solid black;" colspan="3"><u>DATE</u><br/><br/><br/><br/></td>'
          		+'<td style="border-right:1px solid black;border-bottom:1px solid black;" colspan="2"><u>QTY</u><br/><br/><br/><br/></td></tr></table></td>'
          		// InnerTable#3
          		+'<td><table width="100%" border="1 solid black">'
          		+'<tr><td colspan="6"><b>Job:</b></td><td colspan="6">'+hasValue(record.getFieldValue('tranid'))+'</td></tr>'
          		+'<tr><td colspan="6"><b>Part:</b></td><td colspan="6">'+hasValue(assemblyRec.getFieldValue('itemid'))+'</td></tr>'
          		+'<tr><td colspan="6"><b>WMK Legacy#:</b></td><td colspan="6">'+hasValue(wmkLegacyNum)+'</td></tr>'
          		+'<tr><td colspan="6"><b>Class Segment:</b></td><td colspan="6">'+nlapiEscapeXML(hasValue(assemblyRec.getFieldText('custitem_class_segment')))+'</td></tr>'
          		+'<tr><td colspan="6"><b>Required Qty:</b></td><td colspan="6">'+hasValue(record.getFieldValue('quantity'))+'</td></tr>'
          		+'<tr><td colspan="6"><b>Start:</b></td><td colspan="6">'+hasValue(record.getFieldValue('startdate'))+'</td></tr>'
          		+'<tr><td colspan="6"><b>Due:</b></td><td colspan="6">'+hasValue(record.getFieldValue('enddate'))+'</td></tr>'
          		+'<tr><td colspan="6"><b>Required By:</b></td><td colspan="6">'+hasValue(expectedDate)+'</td></tr>'
          		+'</table></td></tr></table><hr/>';
          		
          	if(salesorderId){
          		// Table#3 Sales Order
          		xml+='<table width="100%">'
          		+'<tr><th colspan="9">SHIPPING SCHEDULE:</th><th colspan="2">Order</th><th colspan="4" align="center">Qty From</th><th colspan="8"></th></tr>'
          		+'<tr><td colspan="3" border="1 solid black"><u>Date</u></td>'
          		+'<td colspan="4" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>SO#</u></td>'
          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>L</u></td>'
          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Qty</u></td>'
          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Job</u></td>'
          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Stock</u></td>'
          		+'<td colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Ship Via</u></td>'
          		+'<td colspan="5" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Ship To</u></td></tr>'
          		+'<tr><td colspan="3" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(expectedDate)+'</td>'
          		+'<td colspan="4" style="border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(salesorderRecord.getFieldValue('tranid'))+'</td>'
          		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;">'+linenum+'</td>'
          		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(soLineQty)+'</td>'
          		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(record.getFieldValue('quantity'))+'</td>'
          		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(stockQty)+'</td>'
          		+'<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;">'+nlapiEscapeXML(hasValue(salesorderRecord.getFieldText('shipmethod')))+'</td>'
          		+'<td colspan="5" style="border-right:1px solid black;border-bottom:1px solid black;">'+nlapiEscapeXML(hasValue(salesorderRecord.getFieldValue('shipaddress')))+'</td></tr></table><hr/>';
          	}             		
          	var count=record.getLineItemCount('item');
          	if(count>0){
          		for(var linenum=1;linenum<=count;linenum++){
          		// Table#4 Raw Materials
          			if(linenum==1){
		          		xml+='<table width="100%"><tr><th colspan="20">RAW MATERIAL COMPONENTS:</th></tr>'
		          		+'<tr><td colspan="6" border="1 solid black"><u>Part Number</u></td>'
		          		+'<td colspan="5" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Description</u></td>'
		          		+'<td colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Required Qty</u></td>'
		          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Unit</u></td>'
		          		+'<td colspan="4" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Whse</u></td></tr>';
          			}
	          		xml+='<tr><td colspan="6" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(record.getLineItemValue('item','item_display',linenum))
	          		+'<br/><b>WMK Legacy#: </b>'+hasValue(record.getLineItemValue('item','custcol_wmk_cntm',linenum))+'</td>'
	          		+'<td colspan="5" style="border-right:1px solid black;border-bottom:1px solid black;">'+nlapiEscapeXML(hasValue(record.getLineItemValue('item','description',linenum)))+'</td>'
	          		+'<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(record.getLineItemValue('item','quantity',linenum))+'</td>'
	          		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;">'+nlapiEscapeXML(hasValue(record.getLineItemValue('item','units_display',linenum)))+'</td>'
	          		+'<td colspan="4" style="border-right:1px solid black;border-bottom:1px solid black;">'+nlapiEscapeXML(hasValue(record.getLineItemValue('item','inventorydetails',linenum)))+'</td></tr>';
          		}xml+='</table>';
          	}
  
          	count=record.getLineItemCount('custpage_operation_instruction');
          	nlapiLogExecution('DEBUG','', 'Operations Count: '+count);
          	if(count>0){
          		for(var linenum=1;linenum<=count;linenum++){
          		// Table#5 Operations
          			if(linenum==1){
          			
          				/*if(isMTO=='F'){
          					xml+='<hr/><table width="100%"><thead>'
                          		+'<tr><th colspan="33">Operations:</th></tr><tr><td colspan="3" border="1 solid black"><u>Seq No.</u></td>'
                          		+'<td colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Oper.</u></td>'
                          		+'<td colspan="5" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Descriptions</u></td>'
                          		+'<td colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Mfg. Work Center</u></td>'
                          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Start Date</u></td>'
                          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>End Date</u></td>'
                          		+'<td colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Setup Time</u></td>'
                          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Run Time</u></td>'
                          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>1st Off</u></td>'
                          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Qty</u></td>'
                          		+'<td colspan="4" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Sign</u></td>'
                          		+'<td colspan="2" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Date</u></td></tr></thead><tbody>';
          				}
          				else{*/
          					xml+='<hr/><table width="100%"><thead>'
                          		+'<tr><th colspan="33">Operations:</th></tr><tr><td colspan="2" border="1 solid black"><u>Seq</u></td>'
                          		+'<td colspan="5" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Oper.</u></td>'
                          		+'<td colspan="10" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Instructions</u></td>'
                          		+'<td colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Run Time</u></td>'
                          		+'<td colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>1st Off</u></td>'
                          		+'<td colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Qty</u></td>'
                          		+'<td colspan="4" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Sign</u></td>'
                          		+'<td colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><u>Date</u></td></tr></thead><tbody>';
                          
          				//}
                  	}
          			
          			/*if(isMTO=='F'){
          				xml+='<tr><td colspan="3" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(record.getLineItemValue('custpage_operation_instruction','custpage_operation_sequence',linenum))+'</td>'
                  		+'<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;">'+nlapiEscapeXML(hasValue(record.getLineItemText('custpage_operation_instruction','custpage_operation_name',linenum)))+'</td>'
                  		+'<td colspan="5" style="border-right:1px solid black;border-bottom:1px solid black;">'+nlapiEscapeXML(hasValue(record.getLineItemValue('custpage_operation_instruction','custpage_operation_instructions',linenum)))+'</td>'
                  		+'<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;">'+nlapiEscapeXML(hasValue(record.getLineItemText('custpage_operation_instruction','custpage_mfg_work_center',linenum)))+'</td>'
                  		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(record.getLineItemValue('custpage_operation_instruction','custpage_startdatetime',linenum))+'</td>'
                  		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(record.getLineItemValue('custpage_operation_instruction','custpage_enddatetime',linenum))+'</td>'
                  		+'<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(record.getLineItemValue('custpage_operation_instruction','custpage_setuptime',linenum))+'</td>'
                  		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(record.getLineItemValue('custpage_operation_instruction','custpage_runtime',linenum))+'</td>'
                  		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;"></td>'
                  		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;"></td>'
                  		+'<td colspan="4" style="border-right:1px solid black;border-bottom:1px solid black;"></td>'
                  		+'<td colspan="2" style="border-right:1px solid black;border-bottom:1px solid black;"></td></tr>'
          			}
          			else{*/
          				xml+='<tr><td colspan="2" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">'+hasValue(record.getLineItemValue('custpage_operation_instruction','custpage_operation_sequence',linenum))+'</td>'
                  		+'<td colspan="5" style="border-right:1px solid black;border-bottom:1px solid black;">'+nlapiEscapeXML(hasValue(record.getLineItemText('custpage_operation_instruction','custpage_operation_name',linenum)))+'</td>'
                  		+'<td colspan="10" style="border-right:1px solid black;border-bottom:1px solid black;">'+nlapiEscapeXML(hasValue(record.getLineItemValue('custpage_operation_instruction','custpage_operation_instructions',linenum)))+'</td>'
                  		+'<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;">'/*hasValue(record.getLineItemValue('custpage_operation_instruction','custpage_runtime',linenum))*/+'</td>'
                  		+'<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;"></td>'
                  		+'<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;"></td>'
                  		+'<td colspan="4" style="border-right:1px solid black;border-bottom:1px solid black;"></td>'
                  		+'<td colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;"></td></tr>'
                  		
          			//}
          		}xml+='</tbody></table>';
          	}
          	// Table#6 SO Line Memo
          	if(soLineMemo){
          		xml+='<hr/><table width="100%" style="page-break-inside:avoid"><tr>'
                    +'<th>PICK LIST INSTRUCTIONS:</th></tr><tr>'
                    +' <td border="solid black 1">'+nlapiEscapeXML(hasValue(soLineMemo))+'<br/><br/></td></tr></table>';
                  
          	}
            
            if(itemImageId){
            	xml+='<br/><table width="100%" style="page-break-inside:avoid"><tr>'
                    +'<th>Item Image:</th></tr><tr>'
                    +' <td border="solid black 1"><img style="width:595px;height:642px" src="'+itemImageUrl+'"/></td></tr></table>';
            }
            
            xml+='</body></pdf>';

            var attachment='';
            count=assemblyRec.getLineItemCount('mediaitem');
          	nlapiLogExecution('DEBUG','', 'Files Count: '+count);

            if(count>0){
            	
            	for(var linenum=1;linenum<=count;linenum++){
            		if(assemblyRec.getLineItemValue('mediaitem','printwithbom',linenum)=="T"){
            			var attachId=assemblyRec.getLineItemValue('mediaitem','mediaitem',linenum);
                      	nlapiLogExecution('DEBUG','attchId',attachId);
                      	makeFileAvailOnline(attachId);
                      	var attachURL=fetchURL(attachId);
                      	nlapiLogExecution('DEBUG','attchURL',attachURL);
						// attachURL=nlapiEscapeXML(attachURL);
                    	attachment+='<pdf src="'+attachURL+'"/>';  


            		}
            	}nlapiLogExecution('DEBUG','XML',attachment);
              	xml+=attachment;  
            }
                	
          	
          	xml+='</pdfset>';

          	var file = nlapiXMLToPDF(xml);
			response.setContentType('PDF', 'Traveler Print.pdf', 'inline');
			response.write(file.getValue());
		}
		catch(e){
			response.write('Error occured while printing  Job Traveler. NetSuite error: '+e.message);
		}
	}
}

function hasValue(variable){
	if(variable==undefined || variable==null || variable=='')
		return ' ';
	return variable;
}
function getPrintDate(){
		var ukDate = new Date(new Date(new Date().toUTCString()).setHours( new Date(new Date().toUTCString()).getHours()+8));
		nlapiLogExecution('DEBUG','Date',ukDate);
		
  		var hours = ukDate.getHours();
		hours = hours % 12;
	    hours = hours ? hours : 12; // the hour '0' should be '12'
	    
		var minutes = ukDate.getMinutes();
		minutes = minutes < 10 ? '0'+minutes : minutes;
		
		var ampm = hours >= 12 ? 'PM' : 'AM';
	    var time=hours+':'+minutes+' '+ampm;
	    
	    ukDate=nlapiDateToString(ukDate);
		//nlapiLogExecution('DEBUG','Date ',usDate);

  	return ukDate+' '+time;
}

function fetchURL(Id){
	var search=nlapiSearchRecord("file",null,[new nlobjSearchFilter("internalid",null,"is", Id)], [new nlobjSearchColumn("url")]);
  	var url=search[0].getValue('url');
  	return nlapiEscapeXML(url);
}

function fetchExternalImageURL(Id){
	var search=nlapiSearchRecord("file",null,[new nlobjSearchFilter("internalid",null,"is", Id)], [new nlobjSearchColumn("url")]);
  	var url=search[0].getValue('url');
  	return url;
}

function makeFileAvailOnline(id){
  	if(id){
      var file=nlapiLoadFile(id);
	  file.setIsOnline(true);
	  nlapiSubmitFile(file);
    }
	
}