/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Jul 2019     Prashant Kumar
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */

var folderId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_folder_id');
function sch_cntm_updateTransferPrice(type)
{
	try
	{
		nlapiLogExecution('DEBUG', '', '*********Execution Started*********');

		var savedSearchId = "customsearch_cntm_update_transfer_price";// Constant
		var results = loadRecords(null, savedSearchId, null, null);
		var size = results.length;
		var logs = new Array();
		var column = new Array();
		column.push('Internal Id');
		column.push('Item Name');
		column.push('Error Details');

		logs.push(column);
		// temparory variable to update some items needs to be changed before moving into PROD.
		//var temp =0;
		var store = false;
		var errorMsg = '';
		for (var line = 0; line < results.length; line++)
		{
			nlapiLogExecution('DEBUG', '', 'LineNum: ' + line);

			if (store)// To Store Error Msg. [Note: This block was previously in Catch Block but it didn't work due to time limit.]
			{
				var tempArr = new Array();
				tempArr.push(recordId);
				tempArr.push(itemName);
				tempArr.push(errorMsg.replace(',', ''));
				logs.push(tempArr);
			}

			var remain = nlapiGetContext().getRemainingUsage();
			nlapiLogExecution('DEBUG', 'Usage Remaining', remain);
			if (remain < 100)
			{
				// ...yield the script
				var state = nlapiYieldScript();
				// Throw an error or log the yield results
				if (state.status == 'FAILURE')
					throw "Failed to yield script";
				else if (state.status == 'RESUME')
					nlapiLogExecution('DEBUG', 'Resuming script');
			}
			var recordId = results[line].getValue(new nlobjSearchColumn("internalid"));
			var recordType = getItemType(results[line].getValue(new nlobjSearchColumn("type")));
			nlapiLogExecution('DEBUG', '', 'Processed record Id: ' + recordId + ' record Type: ' + recordType);

			var record = nlapiLoadRecord(recordType, recordId);
			var itemName = record.getFieldValue('itemid');
			try
			{
				nlapiSubmitRecord(record);
			}
			catch (e)
			{
				nlapiLogExecution('ERROR', '', 'Record Id: ' + recordId + ' could not process due to error ' + e.message, e.code);
				errorMsg = e.message;
				store = true;
				continue;

			}
			store = false;
			errorMsg = '';

		}
		if (store){// If last item has an error
			var tempArr = new Array();
			tempArr.push(recordId);
			tempArr.push(itemName);
			tempArr.push(errorMsg.replace(',', ''));
			logs.push(tempArr);
		}
		
		var contents = '';
		for (var i = 0; i < logs.length; i++)
		{
			contents += logs[i].toString() + '\n';
		}
		var file = nlapiCreateFile('Error Logs_' + nlapiDateToString(new Date()), 'CSV', contents);
		file.setFolder(folderId);
		var fileId = nlapiSubmitFile(file);
		nlapiLogExecution('DEBUG', '', 'File Submitted with Id: ' + fileId);

		nlapiLogExecution('DEBUG', '', '*********Execution End*********');
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message + ' Processed record Id: ' + recordId + ' record Type: ' + recordType);
	}

}

function getItemType(memberItemType)
{
	var recordtype = '';
	switch (memberItemType)
	{ // Compare item type to its record type counterpart
		case 'InvtPart':
		recordtype = 'inventoryitem';
		break;
		case 'NonInvtPart':
		recordtype = 'noninventoryitem';
		break;
		case 'Service':
		recordtype = 'serviceitem';
		break;
		case 'Assembly':
		recordtype = 'assemblyitem';
		break;

		case 'GiftCert':
		recordtype = 'giftcertificateitem';
		break;
		default:
	}
	return recordtype;
}
