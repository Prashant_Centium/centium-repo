/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 Dec 2018     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
	try{
 /*  var startdate=nlapiGetFieldValue('custbody_custom_prod_sdate');
   var enddate=nlapiGetFieldValue('custbody_custom_prod_edate');	
   if(startdate=='' || startdate==null || startdate==undefined){
	   nlapiSetFieldValue('custbody_custom_prod_sdate',nlapiGetFieldValue('startdate'));
	  
   }
   if(enddate=='' || enddate==null || enddate==undefined){
	   nlapiSetFieldValue('custbody_custom_prod_edate',nlapiGetFieldValue('enddate'));
	  
   }	*/  // nlapiLogExecution('DEBUG','startdate: '+nlapiGetFieldValue('custbody_custom_prod_sdate'),'enddate: '+nlapiGetFieldValue('custbody_custom_prod_edate'));
      
      	var urlLink=new URL(location.href);
	var item=urlLink.searchParams.get('item');
	if(item!=null){
		var qty=urlLink.searchParams.get('qty');
		var sub=urlLink.searchParams.get('sub');
		nlapiSetFieldValue('subsidiary',sub);
      nlapiSetFieldValue('assemblyitem',item);
		//nlapiSelectLineItem('item');
		//nlapiSetCurrentLineItemValue('item','item',item);
	//	nlapiSetLineItemValue('item','quantity',1,qty);
	//	nlapiSetLineItemValue('item','class',1,urlLink.searchParams.get('class'));
	//	nlapiCommitLineItem('item');
		setTimeout(function(){
          //nlapiSetCurrentLineItemValue('item','item',item);
          nlapiSetFieldValue('assemblyitem',item,false);
          nlapiSetLineItemValue('item','quantity',1,qty);
nlapiSetLineItemValue('item','class',1,urlLink.searchParams.get('class'));
      //    nlapiCommitLineItem('item');
        }, 4000);
	}
}catch(e){
	   nlapiLogExecution('ERROR','Error occured!',e.messagae);
   }

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(){
		try{
	/*   var startdate=nlapiGetFieldValue('custbody_custom_prod_sdate');
	   var enddate=nlapiGetFieldValue('custbody_custom_prod_edate');	
	   if(startdate!==nlapiGetFieldValue('startdate')){
		   nlapiSetFieldValue('startdate',nlapiGetFieldValue('custbody_custom_prod_sdate'));

	   }
	   if(enddate!==nlapiGetFieldValue('enddate')){
		   nlapiSetFieldValue('enddate',nlapiGetFieldValue('custbody_custom_prod_sdate'));

	   }   //nlapiLogExecution('DEBUG','standard startdate: '+nlapiGetFieldValue('startdate'),'standard enddate: '+nlapiGetFieldValue('enddate'));
*/
    return true;
    }catch(e){
 	   nlapiLogExecution('ERROR','Error occured!',e.messagae);
 	   return true;
    }
}

function printBOM(){
    var url =nlapiResolveURL('SUITELET', 'customscript_bom_print', 'customdeploy_bom_print')+'&recordId='+nlapiGetRecordId()+'&recordType='+nlapiGetRecordType();
	window.open(url);
}

function printTraveler(){
    var url =nlapiResolveURL('SUITELET', 'customscript_cntm_print_traveler', 'customdeploy_cntm_print_traveler')+'&recordId='+nlapiGetRecordId()+'&recordType='+nlapiGetRecordType()+'&hostname='+location.hostname;
	window.open(url);
}