/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	try{
		var soId=nlapiGetFieldValue('createdfrom');
		nlapiLogExecution('DEBUG', 'Sales Order Id', soId);
		var itemId=nlapiGetFieldValue('assemblyitem');
		nlapiLogExecution('DEBUG', 'Assembly Item Id', itemId);
		var recordWO=nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
		var memoWO=recordWO.getFieldValue('custbody_cntm_so_line_memo');
		nlapiLogExecution('DEBUG', 'WO Memo Intially', memoWO);
		if((soId==null || soId==undefined || soId=='') && (itemId==null || itemId==undefined || itemId=='')){
			var recordSO=nlapiLoadRecord('salesorder', soId);
			for(var linenum=1;linenum<=recordSO.getLineItemCount('item');linenum++){
				if(itemId==recordSO.getLineItemValue('item','item',linenum)){
					var memoSO=recordSO.getLineItemValue('item','custcol_tgw_so_line_internal_memo',linenum);
					nlapiLogExecution('DEBUG', 'SO Line Memo', memoSO);
					recordWO.setFieldValue('custbody_cntm_so_line_memo', memoSO);
					nlapiSubmitRecord(recordWO);
					break;
				}
			}
		}
	}catch(e){
		nlapiLogExecution('ERROR', 'Error Occurred..', e.message);
	}

}
