/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jun 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve, cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only) dropship, specialorder, orderitems (PO only) paybills (vendor payments)
 * @returns {Void} To make file available without login
 */
function makeFileAvailable_afterSubmit(type)
{
	try
	{
		var record = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

		var imageId = record.getFieldValue('custitem_atlas_item_image');
		nlapiLogExecution('DEBUG', '', 'Image Id: ' + imageId);

		if (imageId)
		{
			var file = nlapiLoadFile(imageId); // internal ID of the file

			file.setIsOnline(true);

			nlapiSubmitFile(file);
		}

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);

	}
}
