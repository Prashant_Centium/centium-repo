/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       09 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Access mode: create, copy, edit
 * @returns {Void} Set the Created From Field on Transaction
 */
function cli_cntm_setCreatedFromField_pageInit(type)
{

	try
	{
		nlapiLogExecution('DEBUG', '', ' *Start* ');

		// URL Object of Transaction from which record is created
		var urlObj = new URL(location.href);
		var formType = urlObj.searchParams.get('transform');
		nlapiLogExecution('DEBUG', 'FormType', formType);

		var poIds = urlObj.searchParams.get('poids');
		nlapiLogExecution('DEBUG', 'PO Ids', JSON.stringify(poIds));

		var poIdsArr = poIds.split(',');
		var index = 0;
		var record = nlapiLoadRecord('purchaseorder', poIdsArr[index]);
		nlapiLogExecution('DEBUG', '', 'Processing PO: ' + poIdsArr[index]);

		var poLineNum = 1;
		var vb_count = nlapiGetLineItemCount('item');
		nlapiLogExecution('DEBUG', '', 'VBill Line Count: ' + vb_count);

		for (var bLineNum = 1; bLineNum <= vb_count;)
		{

			var po_count = record.getLineItemCount('item');
			nlapiLogExecution('DEBUG', '', 'PO Line Count: ' + po_count);

			for (poLineNum = 1; poLineNum <= po_count; poLineNum++)
			{
				var orderLine = nlapiGetLineItemValue('item', 'orderline', bLineNum);
				nlapiLogExecution('DEBUG', '', 'VBLineNum: ' + bLineNum + ' OrderLine: ' + orderLine);

				var line = record.getLineItemValue('item', 'line', poLineNum);
				nlapiLogExecution('DEBUG', '', 'POLineNum: ' + poLineNum + ' Line: ' + line);

				if (orderLine == line)
				{
					nlapiLogExecution('DEBUG', '', 'VBLineNum: ' + bLineNum + ' Processed..');

					nlapiLogExecution('DEBUG', '', 'PO Id: ' + poIdsArr[index]);
					nlapiSetLineItemValue('item', 'custcol_cntm_po_reference', bLineNum, poIdsArr[index]);
					bLineNum += 1;

				}

			}
			if (poLineNum > po_count)
			{

				index += 1;
				if (index < poIdsArr.length)
				{
					record = nlapiLoadRecord('purchaseorder', poIdsArr[index]);
					nlapiLogExecution('DEBUG', '', 'Next PO for processing: ' + poIdsArr[index]);

				}
				else
				{
					break;
				}

			}

		}

		// For only Purchase Order
		/*
		 * if (id && formType == 'purchord') { nlapiSetFieldValue('custbody_cntm_created_from', id); }
		 */

		nlapiLogExecution('DEBUG', '', '*End*');
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', 'Error Occurred..' + e.message, e.code);

	}

}
