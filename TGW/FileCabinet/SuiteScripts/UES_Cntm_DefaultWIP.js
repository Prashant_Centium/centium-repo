/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       01 Oct 2019     Prashant Kumar
 *
 */
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function defaultWIP_beforeLoad(type, form, request)
{
	try
	{
		var subsidiary = nlapiGetFieldValue('subsidiary');
		var iswip = nlapiGetFieldValue('iswip');
		nlapiLogExecution('AUDIT', '', 'Sub Id: ' + subsidiary + ' IsWIP: ' + iswip);

		if (subsidiary == 27 && iswip=='F')
		{// hardcoded for India Sub
			nlapiSetFieldValue('iswip', 'T');
			nlapiLogExecution('AUDIT', 'In Condition', 'Sub Id: ' + subsidiary + ' IsWIP: ' + nlapiGetFieldValue('iswip'));

		}

	}
	catch (e)
	{
		nlapiLogExecution('AUDIT', '', 'Error Details: ' + e.message);
	}
}
