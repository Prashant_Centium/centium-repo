/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Aug 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */

var configRecId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_config_record');
function addpdf_button_beforeLoad(type, form, request)
{
	try
	{
		var configRec = nlapiLoadRecord('customrecord_cntm_advanced_pdf', configRecId);
		var url = nlapiResolveURL('SUITELET', 'customscript_cntm_print_template', 'customdeploy_cntm_print_template', false);
		url += '&templateId=' + configRec.getFieldValue('custrecord_cntm_template') + '&recordType=' + nlapiGetRecordType() + '&recordId=' + nlapiGetRecordId();

		nlapiLogExecution('DEBUG', 'Details', 'URL: ' + url);
		form.addButton('custpage_print_button', 'PRINT', "window.open('" + url + "');");//

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', 'Details', 'Error Message: ' + e.message);
	}

}
