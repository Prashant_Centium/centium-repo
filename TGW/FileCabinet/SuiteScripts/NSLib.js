/**
 * Module Description
 * Library for NS SuiteScript 1.0
 * This files contain reusable functions
 * Version    Date            Author           Remarks
 * 1.00       10 Jun 2019     Prashant Kumar
 *
 */

/**
 * functions to get more than 1000 records from saved search
 * 
 * @param savedSearchId[required]
 * @param recordTypeId[optional]
 * @param filters[optional]
 * @param columns[optional]
 * @returns{Object}
 */

function loadRecords(recordTypeId, savedSearchId, filters, columns)
{
	var search = nlapiLoadSearch(recordTypeId, savedSearchId);
	
	if(filters){
		search.addFilters(filters);
	}
	if(columns){
		search.addColumns(columns);
	}
	var searchResults = search.runSearch();
	var results;
	var resultIndex = 0;
	var resultStep = 1000;
	var resultSet;

	// more than 1000 records
	do
	{
		resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
		if (resultIndex == 0)
			results = resultSet;
		else if (resultSet)
			results = results.concat(resultSet);
		resultIndex = resultIndex + resultStep;
	}
	while (resultSet.length > 0);
	nlapiLogExecution('DEBUG', '', 'Total Records: ' + results.length);

	return results;

}

function restrict_record_creation(type)
{
	try
	{
		if (type == 'create' || type == 'copy')
		{// to prevent from creation and make copy of record if record exist

			var recordType = nlapiGetRecordType();

			var result = nlapiSearchRecord(recordType, null);
			validateForCreation(result, recordType)

		}

	}
	catch (e)
	{
		if (e.code == '001')
		{
			throw e;
		}
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);

	}

}

function validateForCreation(result, recordType)
{
	nlapiLogExecution('DEBUG', '', 'Record Length: ' + result.length + ' Record Type: ' + recordType);

	if (result)
	{
		var error = nlapiCreateError('001', 'Configuration Record already exists.', false);
		throw error;
	}
}

