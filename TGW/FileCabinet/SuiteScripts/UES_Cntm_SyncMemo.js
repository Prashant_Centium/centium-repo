/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function ues_sync_memo_afterSubmit(type){

/*Description
 * ---Logic---
 * On Record Submit
 * Load SO Record
 * Go To the line level and fetch Create WO field for each line
 * Load WO if available
 * Submit WO
 */
	
	//Code	
	try{	
		//Load SO Record
		var record=nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
		var count=record.getLineItemCount('item');
		nlapiLogExecution('DEBUG', 'Count', count);
		//Go To the line level and fetch Create WO field for each line
		for(var linenum=1;linenum<=count;linenum++){
			var woId=record.getLineItemValue('item', 'woid', linenum);
			var memo=record.getLineItemValue('item', 'custcol_tgw_so_line_internal_memo', linenum);
			nlapiLogExecution('DEBUG', 'SO Line Memo', memo);
			nlapiLogExecution('DEBUG', 'Work Order Id', woId);
			//Load WO if available
			if(woId!=null || woId!=undefined || woId!=''){
				nlapiLogExecution('DEBUG', 'Loading WO..','');
				var recordWO=nlapiLoadRecord('workorder', woId);
				recordWO.setFieldValue('custbody_cntm_so_line_memo',memo)
				//Submit WO
				nlapiSubmitRecord(recordWO);
				nlapiLogExecution('DEBUG', 'Record Submitted','');
	
			}
		}
	}catch(e){
		nlapiLogExecution('ERROR', 'Error Occurred..', e.message);
	
	}	
	
}
