/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Aug 2019     Prashant Kumar
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function update_manufac_custom_rec_scheduled(type)
{
	try
	{
		nlapiLogExecution('Audit', '**Execution Started**');
		var results = loadRecords(null, 1106, null, null);// To be Changed
		for (var i = 0; i < results.length; i++)
		{
			var remain = nlapiGetContext().getRemainingUsage();
			nlapiLogExecution('Audit', 'Usage Remaining before Record Processed', remain);

			if (remain < 100)
			{
				// ...yield the script
				var state = nlapiYieldScript();
				// Throw an error or log the yield results
				if (state.status == 'FAILURE')
					throw "Failed to yield script";
				else if (state.status == 'RESUME')
					nlapiLogExecution('Audit', 'Resuming script');
			}

			var mfgRecId = results[i].getValue(new nlobjSearchColumn("internalid", "CUSTRECORD_CNTM_MFG_ROUTING_REC_REFF", "GROUP").setSort(false));
			nlapiLogExecution('Audit', '', 'Mfg Record Id: ' + mfgRecId);

			var mfgRec = nlapiLoadRecord('manufacturingrouting', mfgRecId);

			var count = mfgRec.getLineItemCount('routingstep');

			for (var line = 1; line <= count; line++)
			{
				var seq = mfgRec.getLineItemValue('routingstep', 'operationsequence', line);
				var name = mfgRec.getLineItemValue('routingstep', 'operationname', line);

				var mfgSearch = nlapiSearchRecord("customrecord_cntm_line_level_routing_mfg", null, [ [ "custrecord_cntm_mfg_routing_rec_reff.internalid", "anyof", mfgRecId ], "AND", [ "custrecord_cntm_routing_step_sequence", "isempty", "" ] ], [ new nlobjSearchColumn("internalid").setSort(false), new nlobjSearchColumn("internalid", "CUSTRECORD_CNTM_MFG_ROUTING_REC_REFF", null).setSort(false), new nlobjSearchColumn("custrecord_cntm_mfg_routing_rec_reff"), new nlobjSearchColumn("custrecord_cntm_routing_step_sequence"), new nlobjSearchColumn("custrecord_cntm__routing_operation_name"), new nlobjSearchColumn("custrecord_cntm_mfg_rout_instructions") ]);

				// if (mfgSearch)
				try
				{
					for (var j = 0; j < mfgSearch.length; j++)
					{
						var id = mfgSearch[j].getValue(new nlobjSearchColumn("internalid").setSort(false));
						var operName = mfgSearch[j].getText(new nlobjSearchColumn("custrecord_cntm__routing_operation_name"));
						nlapiLogExecution('Audit', '', 'Custom Record Id: ' + id);

						if (name == operName)
						{
							nlapiSubmitField("customrecord_cntm_line_level_routing_mfg", id, "custrecord_cntm_routing_step_sequence", seq);
							break;
						}

					}
				}
				catch (err)
				{
					nlapiLogExecution('AUDIT', '', 'Error Details: ' + err.message + ' on Custom Record Id: ' + id);
					continue;
				}

			}

			remain = nlapiGetContext().getRemainingUsage();
			nlapiLogExecution('Audit', 'Usage Remaining after Record Processed', remain);
		}
		nlapiLogExecution('Audit', '**Execution End**');

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);
	}

}
