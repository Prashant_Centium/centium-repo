/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Sep 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function hideField_beforeLoad(type, form, request)
{
	try
	{
		var vendor = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_vendor');
		
		if (nlapiGetFieldValue('entity') != vendor)
		{
			nlapiGetField('custbody_cntm_po_from').setDisplayType('hidden');
		}
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);

	}

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @returns {Void}
 */
/*
 * function mandatePOFrom_beforeSubmit(type){ var vendor=nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_vendor');
 * 
 * if(nlapiGetFieldValue('entity')==vendor){ if(!nlapiGetFieldValue('custbody_cntm_po_from')){
 *  } }
 *  }
 */
