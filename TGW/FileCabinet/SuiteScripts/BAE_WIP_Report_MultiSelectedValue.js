/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Sep 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function wipReport_multi_Sel_Val_SaveRecord(){
	var values=nlapiGetFieldValues('custpage_assembly_multiselect');
	nlapiSetFieldValue('custpage_multivalues', values.toString(), null, null);
    return true;
}
