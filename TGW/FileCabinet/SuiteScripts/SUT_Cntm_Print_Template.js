/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       29 Aug 2019     Prashant Kumar
 *
 */

/**
 * 
 */
function printTemplate_suitelet(request, response)
{

	try
	{
		if (request.getMethod() == 'GET')
		{

			var templateId = request.getParameter('templateId');
			var recordType = request.getParameter('recordType');
			var recordId = request.getParameter('recordId');

			nlapiLogExecution('DEBUG', 'Details', 'Template Id: ' + templateId);

			var renderer = nlapiCreateTemplateRenderer(); // initiate nlapiCreateTemplateRenderer()
			var template = nlapiLoadFile(templateId);
			template = template.getValue();
			renderer.setTemplate(template); // set the template
			renderer.addRecord('record', nlapiLoadRecord(recordType, recordId))
			// renderer.addRecord('record', record); // add the record that we loaded

			var xmlsample = nlapiEscapeXML(renderer.renderToString()); // convert to xml
			nlapiLogExecution('DEBUG', 'XML String ', 'Template Id: ' + xmlsample);

			response.writePage(nlapiXMLToPDF(xmlsample));

		}

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', 'Details', 'Error Message: ' + e.message);
	}

}
