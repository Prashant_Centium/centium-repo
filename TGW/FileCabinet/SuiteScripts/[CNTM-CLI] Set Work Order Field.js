/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Oct 2019     Shashank Rao
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @param {String}
 *            name Field internal id
 * @param {Number}
 *            linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum)
{
	var obj = new Object();
	obj.type = type;
	obj.name = name;
	obj.linenum;

	if (obj.name == 'manufacturingrouting' || obj.name == 'iswip')
	{

		var paramSub = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_sub_on_wrk_ord');
		var sub = nlapiGetFieldValue('subsidiary');

		var manufacturingRouting = nlapiGetFieldValue('manufacturingrouting');
		var isWip = nlapiGetFieldValue('iswip');
		if (isWip == 'T' && manufacturingRouting && (paramSub == sub))
		{
			nlapiSetFieldValue('schedulingmethod', 'FORWARD', true, true);
		}

	}
}
