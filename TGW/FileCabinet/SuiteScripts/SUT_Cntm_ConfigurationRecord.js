/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Jun 2019     Prashant Kumar
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
function configurationRecord_suitelet(request, response)
{
	try
	{
		var record = nlapiLoadRecord('customrecord_cntm_restict_multiple_bins', 1);
		var bins = record.getFieldValue('custrecord_cntm_bins');

		var form = nlapiCreateForm('Restricted Bins Configuration');
		var field = form.addField('custpage_bins', 'multiselect', 'Bins', "-242")
		form.addSubmitButton('Saved');
			

		if (request.getMethod() == 'POST')
		{

			record.setFieldValue('custrecord_cntm_bins', request.getParameter('custpage_bins'));
			nlapiSubmitRecord(record);
			record = nlapiLoadRecord('customrecord_cntm_restict_multiple_bins', 1);
			bins = record.getFieldValue('custrecord_cntm_bins');
			nlapiLogExecution('DEBUG', '', 'Bins: ' + bins);

		}
		field.setDefaultValue(bins);
		response.writePage(form);

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);
	}

}
