/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Nov 2018     Yogesh Jagdale
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type)
{
	try
	{
		if (nlapiGetFieldText('class') != nlapiGetFieldText('custitem_custom_class'))
		{
			nlapiSetFieldText('custitem_custom_class', nlapiGetFieldText('class'));
		}
		try
		{
			if (nlapiGetFieldText('taxschedule') == "")
			{
				nlapiSetFieldText('taxschedule', 'Taxable');
			}
		}
		catch (e1)
		{
			if (nlapiGetFieldText('taxschedule') != "")
			{
				nlapiSetFieldValue('taxschedule', 2);
			}
			nlapiLogExecution('ERROR', 'Error while setting TAX SCHEDULE Value', e1.message);
		}
		/*try
		{
			if (nlapiGetFieldText('unitstype') == "")
			{
				nlapiSetFieldText('unitstype', 'Each');
			}
		}
		catch (e2)
		{
			if (nlapiGetFieldText('unitstype') != "")
			{
				nlapiSetFieldValue('unitstype', 1);
			}
			nlapiLogExecution('ERROR', 'Error while setting Units Type', e2.message);
		}*/

	}
	catch (e)
	{
		alert('Error Occured on a script applied on this page.Script Name:Prevent Duplicate OEM Number Client Script -- NS error:' + e.message);
		nlapiLogExecution('ERROR', 'Catch', e.message);
	}

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @param {String}
 *            name Field internal id
 * @param {Number}
 *            linenum Optional line item number, starts from 1
 * @returns {Boolean} True to continue changing field value, false to abort value change
 */

function clientValidateField(type, name, linenum)
{
	//debugger;
	try
	{
		if (name == 'custitem_tgw_oem_number')
		{
			debugger;
			if (nlapiGetRecordId() == "")
			{
				var oemNumber = nlapiGetFieldValue('custitem_tgw_oem_number');
				if (oemNumber != "")
				{
					var assemblyRecordsFound = nlapiSearchRecord('assemblyitem', null, [ [ "custitem_tgw_oem_number", "is", oemNumber ] ], [ new nlobjSearchColumn("itemid").setSort(false) ]);
					var iventoryRecordsFound = nlapiSearchRecord('inventoryitem', null, [ [ "custitem_tgw_oem_number", "is", oemNumber ] ], [ new nlobjSearchColumn("itemid").setSort(false) ]);

					if ((assemblyRecordsFound && assemblyRecordsFound.length > 0) || (iventoryRecordsFound && iventoryRecordsFound.length > 0))
					{
						if (confirm('An item record with same OEM Number already exists. Please make sure OEM NUMBER is unique.Click OK to ignore and Cancel to Change OEM Number.'))
						{
							return true;
						}
						else
						{
							nlapiSetFieldValue('custitem_tgw_oem_number', '');
							return false;
						}
					}
					else
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}
			else
			{
				var oldOemNumber = nlapiLookupField(nlapiGetRecordType(), nlapiGetRecordId(), 'custitem_tgw_oem_number');
				var oemNumber = nlapiGetFieldValue('custitem_tgw_oem_number');
				if (oemNumber != "")
				{
					if (oldOemNumber != oemNumber)
					{
						var assemblyRecordsFound = nlapiSearchRecord('assemblyitem', null, [ [ "custitem_tgw_oem_number", "is", oemNumber ] ], [ new nlobjSearchColumn("itemid").setSort(false) ]);
						var iventoryRecordsFound = nlapiSearchRecord('inventoryitem', null, [ [ "custitem_tgw_oem_number", "is", oemNumber ] ], [ new nlobjSearchColumn("itemid").setSort(false) ]);

						if ((assemblyRecordsFound && assemblyRecordsFound.length > 0) || (iventoryRecordsFound && iventoryRecordsFound.length > 0))
						{
							if (confirm('An item record with same OEM Number already exists. Please make sure OEM NUMBER is unique.Click OK to ignore and Cancel to Change OEM Number.'))
							{
								return true;
							}
							else
							{
								nlapiSetFieldValue('custitem_tgw_oem_number', '');
								return false;
							}
						}
						else
						{
							return true;
						}
					}
					else
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}
		}
		else
		{
			return true;
		}
	}
	catch (e)
	{
		alert('Error Occured on a script applied on this page.Script Name:Prevent Duplicate OEM Number Client Script -- NS error:' + e.message);
		nlapiLogExecution('ERROR', 'Catch', e.message);
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @param {String}
 *            name Field internal id
 * @param {Number}
 *            linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function formatNumber(type, name, linenum)
{
	try
	{
		//debugger;
		if (name == 'custitem_tgw_length_od_imperial')
		{
			var lengthOdImperrial = parseFloat(nlapiGetFieldValue('custitem_tgw_length_od_imperial')).toFixed(3);
			if (nlapiGetFieldValue('custitem_tgw_length_od_imperial') != "")
			{
				if (lengthOdImperrial != nlapiGetFieldValue('custitem_tgw_length_od_imperial'))
				{
					if (isNaN(lengthOdImperrial))
					{
						alert('Please Enter Correct Value');
						nlapiSetFieldValue('custitem_tgw_length_od_imperial', '');
					}
					else
					{
						nlapiSetFieldValue('custitem_tgw_length_od_imperial', lengthOdImperrial);
					}
				}
			}
		}
		else if (name == 'custitem_tgw_length_od_metric')
		{
			var lengthOdImperrial = parseFloat(nlapiGetFieldValue('custitem_tgw_length_od_metric')).toFixed(1);
			if (nlapiGetFieldValue('custitem_tgw_length_od_metric') != "")
			{
				if (lengthOdImperrial != nlapiGetFieldValue('custitem_tgw_length_od_metric'))
				{
					if (isNaN(lengthOdImperrial))
					{
						alert('Please Enter Correct Value');
						nlapiSetFieldValue('custitem_tgw_length_od_metric', '');
					}
					else
					{
						nlapiSetFieldValue('custitem_tgw_length_od_metric', lengthOdImperrial);
					}
				}
			}
		}
		else if (name == 'custitem_tgw_width_id_imperial')
		{
			var lengthOdImperrial = parseFloat(nlapiGetFieldValue('custitem_tgw_width_id_imperial')).toFixed(3);
			if (nlapiGetFieldValue('custitem_tgw_width_id_imperial') != "")
			{
				if (lengthOdImperrial != nlapiGetFieldValue('custitem_tgw_width_id_imperial'))
				{
					if (isNaN(lengthOdImperrial))
					{
						alert('Please Enter Correct Value');
						nlapiSetFieldValue('custitem_tgw_width_id_imperial', '');
					}
					else
					{
						nlapiSetFieldValue('custitem_tgw_width_id_imperial', lengthOdImperrial);
					}
				}
			}
		}
		else if (name == 'custitem_tgw_width_id_metric')
		{
			var lengthOdImperrial = parseFloat(nlapiGetFieldValue('custitem_tgw_width_id_metric')).toFixed(1);
			if (nlapiGetFieldValue('custitem_tgw_width_id_metric') != "")
			{
				if (lengthOdImperrial != nlapiGetFieldValue('custitem_tgw_width_id_metric'))
				{
					if (isNaN(lengthOdImperrial))
					{
						alert('Please Enter Correct Value');
						nlapiSetFieldValue('custitem_tgw_width_id_metric', '');
					}
					else
					{
						nlapiSetFieldValue('custitem_tgw_width_id_metric', lengthOdImperrial);
					}
				}
			}
		}
		else if (name == 'custitem_tgw_thickness_imperial')
		{
			var lengthOdImperrial = parseFloat(nlapiGetFieldValue('custitem_tgw_thickness_imperial')).toFixed(3);
			if (nlapiGetFieldValue('custitem_tgw_thickness_imperial') != "")
			{
				if (lengthOdImperrial != nlapiGetFieldValue('custitem_tgw_thickness_imperial'))
				{
					if (isNaN(lengthOdImperrial))
					{
						alert('Please Enter Correct Value')
						nlapiSetFieldValue('custitem_tgw_thickness_imperial', '');
					}
					else
					{
						nlapiSetFieldValue('custitem_tgw_thickness_imperial', lengthOdImperrial);
					}
				}
			}
		}
		else if (name == 'custitem_tgw_thickness_metric')
		{
			var lengthOdImperrial = parseFloat(nlapiGetFieldValue('custitem_tgw_thickness_metric')).toFixed(2);
			if (nlapiGetFieldValue('custitem_tgw_thickness_metric') != "")
			{
				if (lengthOdImperrial != nlapiGetFieldValue('custitem_tgw_thickness_metric'))
				{
					if (isNaN(lengthOdImperrial))
					{
						alert('Please Enter Correct Value');
						nlapiSetFieldValue('custitem_tgw_thickness_metric', '');
					}
					else
					{
						nlapiSetFieldValue('custitem_tgw_thickness_metric', lengthOdImperrial);
					}
				}
			}

		}
		else if (name == 'custitem_custom_class')
		{

			if (nlapiGetFieldText('class') != nlapiGetFieldText('custitem_custom_class'))
			{
				nlapiSetFieldText('class', nlapiGetFieldText('custitem_custom_class'));
			}

		}
	}
	catch (e)
	{
		alert('Error Occured on a script applied on this page.Script Name:Prevent Duplicate OEM Number Client Script -- NS error:' + e.message);
		nlapiLogExecution('ERROR', 'Catch', e.message);
	}
}
