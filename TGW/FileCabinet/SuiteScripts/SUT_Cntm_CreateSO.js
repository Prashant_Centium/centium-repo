/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Sep 2019     Prashant Kumar
 *
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 * 
 * It will display the data based on Saved Search named India Sub Purchase Order.
 * On the basis of user preference it will either generate SO or reject PO.
 *  
 */

function createSO_suitelet(request, response)
{	nlapiLogExecution('AUDIT', 'In Suitlet', '-----Start------');
	try
	{
		if (request.getMethod() == "GET")
		{
			var form = nlapiCreateForm('Intercompany SO for India Sub', false);
			form.setScript('customscript_cntm_create_so_cli');
			form.addButton('custpage_generate_so', 'Generate Sales Order', "process('" + "approve" + "')");//call the client script
			form.addButton('custpage_reject_po', 'Reject Purchase Order', "process('" + "reject" + "')");//call the client script

			var sublist = form.addSubList('custpage_po_details', 'list', 'Purchase Order Details', null);
			sublist.addMarkAllButtons();

			sublist.addField('custpage_select', 'checkbox', 'Select', null);
			var resultSet = loadRecords(null, 'customsearch_cntm_india_sub_po', null, null);
			var columns = resultSet.columns;
			var values = new Array();

			// Columns
			sublist.addField('custpage_internalid', 'text', 'Internal Id').setDisplayType('hidden');
			sublist.addField('custpage_url', 'url', 'Link').setLinkText('VIEW');
			// sublist.addField('custpage_itemdetails', 'inlinehtml', 'Item Details');

			for (var i = 0; i < columns.length; i++)
			{

				sublist.addField('custpage_' + columns[i].getName(), 'text', columns[i].getLabel());

			}
			var textArea=sublist.addField('custpage_item', 'textarea', '(QTY)Item Name');
			textArea.setDisplaySize(250);
			textArea.setDisplayType('inline')
			//sublist.addField('custpage_qty', 'textarea', 'Quantity').setDisplayType('inline');

			//sublist.addField('custpage_rate', 'textarea', 'Rate').setDisplayType('inline');

			sublist.addField('custpage_comments', 'textarea', 'Comments').setDisplayType('entry');

			// Values
			for (var i = 0; i < resultSet.data.length; i++)
			{
				var obj = new Object();
				obj['custpage_internalid'] = resultSet.data[i].getId();
				obj['custpage_url'] = nlapiResolveURL('RECORD', 'purchaseorder', obj['custpage_internalid']);
				var itemDetails = getItemDetails(resultSet.data[i].getId());
				obj['custpage_item'] = itemDetails.item;
				//obj['custpage_qty'] = itemDetails.qty;
				//obj['custpage_rate'] = itemDetails.rate;

				for (var k = 0; k < columns.length; k++)
				{

					if (columns[k].getType() == 'checkbox')
					{
						obj['custpage_' + columns[k].getName()] = resultSet.data[i].getValue(columns[k]) == 'F' ? 'No' : 'Yes';

					}
					else if (columns[k].getType() == 'select')
					{
						obj['custpage_' + columns[k].getName()] = resultSet.data[i].getText(columns[k]);

					}
					else if (columns[k].getType() == 'currency')
					{
						obj['custpage_' + columns[k].getName()] = parseFloat(resultSet.data[i].getValue(columns[k])).toFixed(2);

					}
					else
					{
						obj['custpage_' + columns[k].getName()] = resultSet.data[i].getValue(columns[k]);
						//nlapiLogExecution('AUDIT', '', parseFloat(resultSet.data[i].getText(columns[k])).toFixed(2));

					}

				}
				values.push(obj);
			}
			sublist.setLineItemValues(values);
			
			// If request type is not null
			var type = request.getParameter('type');
			var poIds = request.getParameter('poIds');
			var msg = request.getParameter('msg');
			nlapiLogExecution('AUDIT', '', 'POIds: ' + poIds);
			nlapiLogExecution('AUDIT', '', 'Msg: ' + msg);

			// call Scheduled Script to either create or reject SO
			if (type)
			{
				// it will create parent record for status
				var parentRecord = nlapiCreateRecord('customrecord_cntm_so_creation_list');
				parentRecord.setFieldValue('custrecord_cntm_po_ids', poIds);
				parentRecord.setFieldValue('custrecord_cntm_type', type);
				parentRecord.setFieldValue('custrecord_cntm_record_status', 'Record is being processed. Please refresh the page.');

				var id = nlapiSubmitRecord(parentRecord, true, true);
				// trigger the scheduled script
				nlapiScheduleScript('customscript_cntm_process_po_sch', 'customdeploy_cntm_process_po_sch', {
					custscript_cntm_po_ids : poIds,
					custscript_cntm_type : type,
					custscript_cntm_parentId : id,
					custscript_cntm_msg : msg
				})
				// navigate to status record
				nlapiSetRedirectURL('RECORD', 'customrecord_cntm_so_creation_list', id);

			}

			response.writePage(form);
		}

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);
	}
	nlapiLogExecution('AUDIT', 'In Suitlet', '-----End------');
}
/**
 * Loads the saved search data
 * @return 1000+ data 
 */
function loadRecords(recordTypeId, savedSearchId, filters, columns)
{
	var search = nlapiLoadSearch(recordTypeId, savedSearchId);

	search.addFilters(filters);
	search.addColumns(columns);
	var filters = search.getFilters();
	var columns = search.getColumns();
	var searchResults = search.runSearch();
	var results = new Object();
	results.filters = filters;
	results.columns = columns;
	var resultIndex = 0;
	var resultStep = 1000;
	var resultSet;

	// more than 1000 records
	do
	{
		resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
		if (resultIndex == 0)
			results.data = resultSet;
		else if (resultSet)
			results.data = results.data.concat(resultSet);
		resultIndex = resultIndex + resultStep;
	}
	while (resultSet.length > 0);
	nlapiLogExecution('DEBUG', '', 'Total Records: ' + results.data.length);

	return results;

}
/**
 * 
 * @param recordId{PO Id}
 * @returns {line items details}
 */
function getItemDetails(recordId)
{
	var itemDetails = new Object();

	var record = nlapiLoadRecord('purchaseorder', recordId);
	var count = record.getLineItemCount('item');
	itemDetails.item = "";
	itemDetails.qty = "";
	//itemDetails.rate = "";

	for (var linenum = 1; linenum <= count; linenum++)
	{
		itemDetails.item = itemDetails.item +'('+record.getLineItemValue('item', 'quantity', linenum)+ ')'+' '+ record.getLineItemText('item', 'item', linenum) +'\n';
		//itemDetails.qty = itemDetails.qty + record.getLineItemValue('item', 'quantity', linenum) + '\n';
		//itemDetails.rate = itemDetails.rate + record.getLineItemValue('item', 'rate', linenum) + '\n';

	}

	nlapiLogExecution('AUDIT', '', 'Item Details: ' + JSON.stringify(itemDetails));

	return itemDetails;
}

function getData(data){
		var poMap=new Object();
		// Values
		for (var i = 0; i < resultSet.data.length; i++)
		{
			if(!poMap.resultSet.data[i].getId()){
				poMap[resultSet.data[i].getId()]=new Object();
				
				poMap[resultSet.data[i].getId()]['custpage_internalid'] = resultSet.data[i].getId();
				poMap[resultSet.data[i].getId()]['custpage_url'] = nlapiResolveURL('RECORD', 'purchaseorder', obj['custpage_internalid']);
				
				for (var k = 0; k < columns.length; k++)
				{

					if (columns[k].getType() == 'checkbox')
					{
						if(!poMap[resultSet.data[i].getId()]['custpage_' + columns[k].getName()])
							poMap[resultSet.data[i].getId()]['custpage_' + columns[k].getName()] = resultSet.data[i].getValue(columns[k]) == 'F' ? 'No' : 'Yes';

					}
					else if (columns[k].getType() == 'select')
					{
						if(!poMap[resultSet.data[i].getId()]['custpage_' + columns[k].getName()])
							poMap[resultSet.data[i].getId()]['custpage_' + columns[k].getName()] = resultSet.data[i].getText(columns[k]);

					}
					else if (columns[k].getType() == 'currency')
					{
						if(!poMap[resultSet.data[i].getId()]['custpage_' + columns[k].getName()])
							poMap[resultSet.data[i].getId()]['custpage_' + columns[k].getName()] = parseFloat(resultSet.data[i].getValue(columns[k])).toFixed(2);

					}else if (columns[k].getType() == 'item' || columns[k].getType() == 'quantity')
					{
						if(!poMap[resultSet.data[i].getId()]['custpage_item'])
							poMap[resultSet.data[i].getId()]['custpage_item'] ='('+resultSet.data[i].getValue(columns[k])+')';
						if(poMap[resultSet.data[i].getId()]['custpage_item'] && columns[k].getType() == 'item'){
							poMap[resultSet.data[i].getId()]['custpage_item']+=poMap[resultSet.data[i].getId()]['custpage_item']+'\n';
						}
						if(poMap[resultSet.data[i].getId()]['custpage_item'] && columns[k].getType() == 'quantity'){
							poMap[resultSet.data[i].getId()]['custpage_item']+='('+poMap[resultSet.data[i].getId()]['custpage_item']+')';
						}
						

					}
					else
					{
						if(!poMap[resultSet.data[i].getId()]['custpage_' + columns[k].getName()])
							obj['custpage_' + columns[k].getName()] = resultSet.data[i].getValue(columns[k]);
						//nlapiLogExecution('AUDIT', '', parseFloat(resultSet.data[i].getText(columns[k])).toFixed(2));

					}

				}

			}
			
						values.push(obj);
		}

}