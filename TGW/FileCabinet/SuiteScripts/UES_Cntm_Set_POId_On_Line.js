/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve, cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only) dropship, specialorder, orderitems (PO only) paybills (vendor payments)
 * @returns {Void}
 */
function set_poid_on_line_afterSubmit(type)
{
	try
	{
		var id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', '', 'PO Id: ' + id);
		var record = nlapiLoadRecord(nlapiGetRecordType(), id);

		for (var linenum = 1; linenum <= record.getLineItemCount('item'); linenum++)
		{
			record.setLineItemValue('item', 'custcol_cntm_po_ref', linenum, id);
		}
		nlapiSubmitRecord(record);
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', 'Error Occurred..' + e.message, e.code);
	}
}
