/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Dec 2018     Prashant Kumar   Initial Development only assembly item and service item will be printed
 * 
 * 1.10       05 June 2019    Prashant Kumar   Added Code for Image and BOM attachment
 * 1.20       12 Sep  2019    Prashant Kumar   Added Code to print all the items
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	if (request.getMethod() == "GET") {
		try{
          	var comrec=nlapiLoadConfiguration('companyinformation');
          	var compaddress = nlapiEscapeXML(comrec.getFieldValue('mainaddress_text'));
			var base=fetchURL(comrec.getFieldValue('formlogo'));
          	nlapiLogExecution('DEBUG','recID',request.getParameter('recordId'));
          	var record=nlapiLoadRecord(request.getParameter('recordType'),request.getParameter('recordId'));
          	var venAddress=nlapiEscapeXML(record.getFieldValue("custpage_vendoraddress"));
          	var sdate=record.getFieldValue("startdate");
          	var tranid=record.getFieldValue("tranid");
          	var edate=record.getFieldValue("enddate");
          	var qty=record.getFieldValue("quantity");
          	var assemblyitem=nlapiEscapeXML(record.getFieldText("assemblyitem"));
          	var memo=nlapiEscapeXML(record.getFieldValue("memo"));
          	var units=record.getFieldText("units");
          	var notes=nlapiEscapeXML(record.getFieldValue("custpage_usernotes"));
          	if(venAddress==null || venAddress==undefined)
          		venAddress='';
          	if(compaddress==null || compaddress==undefined)
          		compaddress='';
          	if(sdate==null || sdate==undefined)
          		sdate='';
          	if(tranid==null ||tranid==undefined)
          		tranid='';
          	if(edate==null || edate==undefined)
          		edate='';
          	if(qty==null || qty==undefined)
          		qty='';
          	if(assemblyitem==null || assemblyitem==undefined)
          		assemblyitem='';
          	if(memo==null || memo==undefined)
          		memo='';
          	if(units==null || units==undefined)
          		units='';
          	if(notes==null || notes==undefined)
          		notes='';
		var xml = '<?xml version="1.0"?><!DOCTYPE pdf PUBLIC "-//big.faceless.org//report" "report-1.1.dtd"><pdfset><pdf><head><macrolist><macro id="nlheader">'
			+'<table class="header" style="width: 100%;"><tr><td colspan="2" rowspan="3" ><img src="'+base
			+'" style="float: left; margin: 5px ;height:70px; width:200;" /></td><td valign="middle" align="left"><span class="title">Bill Of Materials</span>'
			+'</td></tr></table></macro><macro id="nlfooter"><table class="footer" style="width: 100%;"><tr><td align="right"><pagenumber/> of <totalpages/></td></tr><tr>'
			+'<td align="center">Tel. 859-47-383 &bull; Toll Free 1-800-407-0173 &bull; 5 Braco International Blvd. &bull; Wilder, Ky 41076 &bull; sales@tgwint.com &bull; www.tgwint.com</td>'
			+'</tr></table></macro></macrolist><style type="text/css"> table{font-size: 10pt;table-layout: fixed;}'
			+'th{font-weight: bold;font-size: 10pt;vertical-align: middle;padding: 5px 6px 3px;background-color: #e3e3e3;color: #333333;}'
			+'td{padding: 4px 6px;}td p{align:left}  b{font-weight: bold;}table.header td{padding: 0;font-size: 10pt;}table.footer td {padding: 0;font-size: 9pt;}'
			+'table.itemtable th{padding-bottom: 10px;padding-top: 10px;}table.body td{padding-top: 1px;} table.total{page-break-inside: avoid;}tr.totalrow{background-color: #e3e3e3;line-height: 150%;}'
			+'td.totalboxtop{font-size: 12pt;background-color: #e3e3e3;}td.addressheader{font-size: 10pt;padding-top: 6px;  padding-bottom: 2px;}td.address{padding-top: 0;}td.totalboxmid{font-size: 28pt;padding-top: 20px;background-color: #e3e3e3;}'
			+'td.totalboxbot {background-color: #e3e3e3;font-weight: bold;}span.title {font-size: 18pt;font-weight: bold;}span.number{font-size: 15pt;}span.itemname {font-weight: bold;line-height: 150%;}hr {width: 100%;color: #d3d3d3;background-color: #d3d3d3;height: 1px;}</style>'
			+'</head><body header="nlheader" header-height="10%" footer="nlfooter" footer-height="40pt" padding="0.5in 0.5in 0.5in 0.5in" size="Letter"><table  style="width:100%;" cellspacing="0px" cellpadding="3px"><tr><td class="addressheader" colspan="6"><b>Vendor</b><br /><br />'
			+venAddress.split('\n').join('<br/>')+'</td><td class="addressheader" colspan="6"><b>Ship To</b><br /><br />'
			+compaddress.split('\n').join('<br/>')
			+'</td><td colspan="10"><table class="headerright" width="100%"><tr><th colspan="4" style="border:1px solid black;">Production Start Date</th><td valign="middle" colspan="6" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">'
			+sdate+'</td></tr><tr><th colspan="4" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Order#</th><td colspan="6" style="border-right:1px solid black;border-bottom:1px solid black;">'
			+tranid+'</td></tr><tr><th colspan="4" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Production End Date</th><td valign="middle" colspan="6" style="border-right:1px solid black;border-bottom:1px solid black;">'
			+edate+'</td></tr><tr><th colspan="4" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Qty Required</th><td colspan="6" style="border-right:1px solid black;border-bottom:1px solid black;">'
			+qty+'</td></tr><tr><th colspan="4" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Assembly</th><td colspan="6" style="border-right:1px solid black;border-bottom:1px solid black;">'
			+assemblyitem+'</td></tr><tr><th colspan="4" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Memo</th><td colspan="6" style="border-right:1px solid black;border-bottom:1px solid black;">'
			+memo+'</td></tr><tr><th colspan="4" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Units</th><td colspan="6" style="border-right:1px solid black;border-bottom:1px solid black;">'
			+units+'</td></tr></table>'
			+'</td></tr></table>';
		
			/*var item1type=record.getLineItemValue('item', 'itemtype', 1);
			var item2type=record.getLineItemValue('item', 'itemtype', 2);
	
			
			if(record.getLineItemCount('item')==2 && (item1type=='Service' && item2type!='Service') || (item2type=='Service' && item1type!='Service')){
				
				var item1=nlapiLoadRecord(recType(item1type),record.getLineItemValue('item', 'item', 1));
				var item2=nlapiLoadRecord(recType(item2type),record.getLineItemValue('item', 'item', 2));
				if(item1type=='serviceitem'){
					var temp=item1;
					item1=item2;
					item2=temp;
				}
				var price=item2.getFieldValue('cost')*nlapiLoadRecord('assemblyitem',record.getFieldValue('assemblyitem')).getLineItemValue('member','quantity',2);
				var total=qty*price;

				xml+='<tr><td colspan="10" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><span class="itemname">'
				+(item1.getFieldValue('itemid')!=null?item1.getFieldValue('itemid'):'')
                +'</span><br/>'+item2.getFieldValue('itemid')+'<br />TGW '
				+(item1.getFieldText('custitem_tgw_material')!=null?item1.getFieldText('custitem_tgw_material'):'')
				+' KNIFE '
                +(item1.getFieldValue('custitem_tgw_length_od_imperial')!=null?item1.getFieldValue('custitem_tgw_length_od_imperial'):0.0)
				+' x '
				+(item1.getFieldValue('custitem_tgw_width_id_imperial')!=null?item1.getFieldValue('custitem_tgw_width_id_imperial'):0.0)
				+' x '
				+(item1.getFieldValue('custitem_tgw_thickness_imperial')!=null?item1.getFieldValue('custitem_tgw_thickness_imperial'):0.0)
				+'<br />Legacy TGW#: '
				+(item1.getFieldValue('custitem_tgw_legacy_item')!=null?item1.getFieldValue('custitem_tgw_legacy_item'):'')
              +'<br /><b>'
				+assemblyitem
              	+'</b><br /></td><td align="center" valign="middle" colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;">'
				+qty
				+'</td><td colspan="3" align="right" valign="middle" style="border-right:1px solid black;border-bottom:1px solid black;">'
				+'$'+price.toFixed(2)
				+'</td><td align="right" valign="middle" colspan="4" style="border-right:1px solid black;border-bottom:1px solid black;">'
				+'$'+total.toFixed(2)
				+'</td></tr>';
			}
*/			
		
		var assemblyItemId=record.getFieldValue('assemblyitem');
      	nlapiLogExecution('DEBUG','Assembly Item Id',assemblyItemId);
      	
      	var assemblyRec=nlapiLoadRecord('assemblyItem', assemblyItemId);
      	var priceObj=new Object();
      	
      /*	for(var i=1;i<=assemblyRec.getLineItemCount('member');i++){
      		priceObj[assemblyRec.getLineItemValue('member', 'item', i)]=assemblyRec.getLineItemValue('member', 'quantity', i);
      	}
      	nlapiLogExecution('AUDIT','',JSON.stringify(priceObj));
*/
		var count=record.getLineItemCount('item');
		
		if(record.getFieldValue('iswip') && count){
			
			for(var linenum=1;linenum<=count;linenum++){
				if(linenum==1){
					xml+='<table class="itemtable" style="width: 100%;"><thead><tr><th align="center" colspan="10" style="border:1px solid black;">Item and Description</th><th align="center" colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">'
						+'Quantity</th><th align="center" colspan="3" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Price Each</th><th align="center" colspan="4" style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;">Total Price</th>'
						+'</tr></thead><tbody>';
				}
				var itemType=recType(record.getLineItemValue('item', 'itemtype', linenum));
				var itemId=record.getLineItemValue('item', 'item', linenum);
				var qty=record.getLineItemValue('item', 'quantity', linenum);
				var price=0.00;
				price=nlapiLookupField(itemType,itemId,'cost',false);
				
				if(!price){
					price=nlapiLookupField(itemType,itemId,'lastpurchaseprice',false);

				}
				price=(price*1.00).toFixed(2);
				var total=(price*qty).toFixed(2);
				//nlapiLogExecution('AUDIT','linenum :'+linenum,price);
				
				xml+='<tr><td colspan="10" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;"><span class="itemname">'
					+nlapiEscapeXML(record.getLineItemText('item', 'item', linenum))+'</span><br/>'+nlapiEscapeXML(isEmpty(record.getLineItemValue('item', 'description', linenum)))
	                +'</td><td align="center" valign="middle" colspan="3" style="border-right:1px solid black;border-bottom:1px solid black;">'
	                +qty
	                +'</td><td colspan="3" align="right" valign="middle" style="border-right:1px solid black;border-bottom:1px solid black;">'
	                +'$'+price
	                +'</td><td align="right" valign="middle" colspan="4" style="border-right:1px solid black;border-bottom:1px solid black;">'
					+'$'+total
	                +'</td></tr>';
			}	
			
		}
		
		
		
		xml+='</tbody></table><br/><table width="100%" border="1 solid black"><tr><th style="border-bottom:1px solid black;">User Notes:</th></tr><tr><td>'
				+notes
				+'<br/></td></tr></table>';
			
			// Added for Image and attachment
          	var itemImageId=assemblyRec.getFieldValue('custitem_atlas_item_image');
          	nlapiLogExecution('DEBUG','Item Image Id',itemImageId);
          	
          	var itemImageUrl='';
          	if(itemImageId){
          		itemImageUrl=fetchURL(itemImageId);
	          	nlapiLogExecution('DEBUG','Item ImageURL',itemImageUrl);
	          	makeFileAvailOnline(itemImageId);
	          	xml+='<br/><table width="100%" style="page-break-inside:avoid"><tr>'
                    +'<th>Item Image:</th></tr><tr>'
                    +'<td border="solid black 1"><img style="width:595px;height:642px" src="'+itemImageUrl+'"/></td></tr></table>';
          	}
          	xml+='</body></pdf>';
          	var attachment='';
            count=assemblyRec.getLineItemCount('mediaitem');
           	nlapiLogExecution('DEBUG','', 'Files Count: '+count);

            if(count>0){
             	
            	for(var linenum=1;linenum<=count;linenum++){
            		if(assemblyRec.getLineItemValue('mediaitem','printwithbom',linenum)=="T"){
            			var attachId=assemblyRec.getLineItemValue('mediaitem','mediaitem',linenum);
            			nlapiLogExecution('DEBUG','attchId',attachId);
            			makeFileAvailOnline(attachId);
                       	var attachURL=fetchURL(attachId);
                       	nlapiLogExecution('DEBUG','attchURL',attachURL);
 						// attachURL=nlapiEscapeXML(attachURL);
                     	attachment+='<pdf src="'+attachURL+'"/>';  
             		}
            	}xml+=attachment;
            }
			xml+='</pdfset>';
			var file = nlapiXMLToPDF(xml);
			response.setContentType('PDF', 'BOM Print.pdf', 'inline');
			response.write(file.getValue());
		}
		catch(e)
		{
			response.write('Error occured while printing BOM Print Slip. NetSuite error: '+e.message);
		}
	}
}

function recType(recordtype){
  switch (recordtype) {   // Compare item type to its record type counterpart
	case 'InvtPart':
		recordtype = 'inventoryitem';
		break;
	case 'NonInvtPart':
		recordtype = 'noninventoryitem';
		break;
	case 'Service':
		recordtype = 'serviceitem';
		break;
	case 'Assembly':
		recordtype = 'assemblyitem';
		break;

	case 'GiftCert':
		recordtype = 'giftcertificateitem';
		break;
	default:
	}
  return recordtype;

}

function fetchURL(Id){
	var search=nlapiSearchRecord("file",null,[new nlobjSearchFilter("internalid",null,"is", Id)], [new nlobjSearchColumn("url")]);
  	var url=search[0].getValue('url');
  	return nlapiEscapeXML(url);
}

function isEmpty(value){
	if(value)
		return value;
	return "";
}

function makeFileAvailOnline(id){
	if(id){
		var file=nlapiLoadFile(id);
		file.setIsOnline(true);
		nlapiSubmitFile(file);
	}
	
}
