/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 May 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @return
 */
function setMandatory_beforeLoad(type, form, request)
{
	try
	{
		var field = nlapiGetField('completedquantity');
		field.setMandatory(true);
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', '', 'Error Details: ' + e.message);
	}

}
