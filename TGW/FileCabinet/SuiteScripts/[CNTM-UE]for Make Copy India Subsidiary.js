/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Oct 2019     Shashank Rao	 MAKE COPY FUNCTIONALITY IF SUBSIDIARY CHANGE WITHOUT WIPING THE DATA
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */


function userEventBeforeLoad(type, form, request)
{
	var UserSub = nlapiGetContext().getSubsidiary();

	
	
	var sub = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_subsidiary_for_mk_copy');
	nlapiLogExecution('DEBUG', 'subsidiary', UserSub+' === '+sub);
	if (UserSub == sub)
	{
		if (type == 'view')
		{
			try
			{

				var field = form.addField('custpage_check', 'checkbox', 'check');

				var recordId = nlapiGetRecordId();
				nlapiLogExecution('AUDIT', 'Sub', nlapiGetFieldValue('subsidiary'));
				if(nlapiGetFieldValue('subsidiary')==24 || nlapiGetFieldValue('subsidiary')==26){// 24 id of WMK and 26 TGW Inc
					var url = nlapiResolveURL('SUITELET', 'customscript_cntm_backend_for_makecopy', 'customdeploy_cntm_mk_copy_india_sub') + '&recordId=' + recordId;
					var btn = form.addButton('custpage_makecopy', 'Make Copy', "window.open('" + url + "','_self');");
				
				}
				}
			catch (e)
			{
				nlapiLogExecution('ERROR', 'ERROR in View', e.message);
			}
		}
		/*if (type == 'copy')
		{
			nlapiLogExecution('DEBUG', 'copy', '');

			
			var recId = request.getParameter('id');
			nlapiLogExecution('DEBUG', 'Request', JSON.stringify(request.getParameter('id')));
			var params = {};
			params['custscript_cntm_mfg_rec_id'] = recId;

			nlapiSetRedirectURL('SUITELET', 'customscript_cntm_backend_for_makecopy', 'customdeploy_cntm_mk_copy_india_sub', null, {
				custscript_cntm_mfg_rec_id : recId
			});
		}*/

	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type)
{

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit, approve, cancel, reject (SO, ER, Time Bill, PO & RMA only) pack, ship (IF only) dropship, specialorder, orderitems (PO only) paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type)
{

}
