/**[CNTM-UE]Defaulting Field Values On Work Order
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 Oct 2019     Shashank Rao
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, delete, xedit approve, reject, cancel (SO, ER, Time Bill, PO & RMA only) pack, ship (IF) markcomplete (Call, Task) reassign (Case) editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
 if(type=='create'){
	 try{
	var context = nlapiGetContext();
	var logInViaSub = context.getSubsidiary();
	
	// define parameter  subsidiary as indian subsidiary
	var indianSubsidiaryId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_subid');
	//-------------------------------------------------
	
	
	nlapiLogExecution('DEBUG', 'Before Submit', '');
	if(logInViaSub){
		if(logInViaSub==indianSubsidiaryId){
			nlapiSetFieldValue('schedulingmethod', 'forward', true, true);//  
			nlapiSetFieldValue('startdate', new Date(), true, true);
			
		}else{
			nlapiLogExecution('DEBUG', 'SUBSIDIARY NOT MATCHED WITH PARAMETER', 'condition Failed : logInViaSub==indianSubsidiaryId');

		}
		
		
	}else{
		nlapiLogExecution('DEBUG', 'SUBSIDIARY NOT PRESENT', '');

	}
	 }catch(e){
		nlapiLogExecution('ERROR', 'ERROR IN BEFORE SUBMIT', e.message); 
	 }
 }
}
