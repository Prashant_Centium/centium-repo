/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Jul 2019     Prashant Kumar
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Boolean} True to continue changing field value, false to abort value change
 */
var restrictedVendorId;

function imposed_tariff_validateField(type, name, linenum){
   
	if(!type && name=='entity'){
		var entity=nlapiGetFieldValue('entity');
		if(entity==restrictedVendorId){
			alert("Item subject to tariff. Please consult with Sales management before quoting customer pricing.");
			return false;
		}
	}
    return true;
}

function loadRestricetedVendorIds(){
	
}