/**
 * Module Description
 * CNTM[SCH_1_0]For Bulk Receiving From PO
 * Version    Date            Author           Remarks
 * 1.00       16 Jul 2019     Shashank Rao
 *
 */

/**
 * @param {String}
 *            type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled(type)
{
	try
	{

		var fileId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_po_id_list');
		var fileObj = nlapiLoadFile(fileId);
		var map = {};
		map = JSON.parse(fileObj.getValue());
		nlapiLogExecution('DEBUG', 'map', map);

		var keysArr = [];
		keysArr = Object.keys(map);

		nlapiLogExecution('DEBUG', 'keysArr', JSON.stringify(keysArr));

		po_receiving(map);
		var fileIdDeleted = nlapiDeleteFile(fileId);
		nlapiLogExecution('DEBUG', 'Deleted File', fileIdDeleted);

	}
	catch (e)
	{
		nlapiLogExecution('ERROR', 'ERROR', e.message);
	}
}


function po_receiving(map)
{
	nlapiLogExecution('DEBUG', '****map', JSON.stringify(map));
	var keys = [];
	keys = Object.keys(map);
	var masterRecReff_Id;
	var poMap = {};
	if (keys.length > 0)
	{
		masterRecReff_Id = keys[0];
		if (masterRecReff_Id)
		{
			var arrayObj = [];
			arrayObj = map[masterRecReff_Id];
			for (var i = 0; i < arrayObj.length; i++)
			{
				var obj = new Object();
				obj = arrayObj[i];
				if (poMap[obj.po_Id])
				{
					var smallerMap = {};
					smallerMap = poMap[obj.po_Id];
					smallerMap[obj.line] = (obj);
					poMap[obj.po_Id] = smallerMap;

				}
				else
				{
					var smallerMap = {};
					smallerMap[obj.line] = (obj);
					poMap[obj.po_Id] = smallerMap;

				}
			}

		}

	}

	nlapiLogExecution('DEBUG', 'PO MAP', JSON.stringify(poMap));
	if (poMap)
	{
		var pO_Lists = [];
		pO_Lists = Object.keys(poMap);
		if (pO_Lists.length > 0)
		{
			for (var i = 0; i < pO_Lists.length; i++)
			{

				var purchaseOrder_Id = pO_Lists[i];
				var mapObj = {};
				mapObj = poMap[purchaseOrder_Id];
			
				var transformRecObj = nlapiTransformRecord('purchaseorder', purchaseOrder_Id, 'itemreceipt');
				var lineNumb = transformRecObj.getLineItemCount('item');
				var count = 0;

				var po_rcv_rec = nlapiCreateRecord('customrecord_cntm_po_rcv_reff');
				po_rcv_rec.setFieldValue('custrecord_cntm_blk_rcv_rec_reff', masterRecReff_Id);
				po_rcv_rec.setFieldValue('custrecord_cntm_rcv_po', purchaseOrder_Id);
				po_rcv_rec.setFieldValue('custrecord_cntm_rcv_status', 2);

				try
				{
					for (var index = 1; index <= lineNumb; index++)
					{
						var itemId = transformRecObj.getLineItemValue('item', 'item', index);
						var orderLine = transformRecObj.getLineItemValue('item', 'orderline', index);
						if (mapObj[orderLine])
						{
							var lineObj = new Object();
							lineObj = mapObj[orderLine];
							transformRecObj.setLineItemValue('item', 'itemreceive', index, 'T');
							transformRecObj.setLineItemValue('item', 'quantity', index, lineObj.qty);
							count++;
						}
						else
						{
							transformRecObj.setLineItemValue('item', 'itemreceive', index, 'F');
						}

					}
					if (count > 0)
					{
						nlapiLogExecution('DEBUG', 'Count', '' + count);
					}
					else
					{
						nlapiLogExecution('DEBUG', 'in ELSE Count', '' + count);

					}
					var itemReceiptId = nlapiSubmitRecord(transformRecObj, true, true);
					if (itemReceiptId)
					{   
						po_rcv_rec.setFieldValue('custrecord_cntm_rcv_item_receipt_reff', itemReceiptId);
						po_rcv_rec.setFieldValue('custrecord_cntm_rcv_status', 3);
						var ChildRec_id = nlapiSubmitRecord(po_rcv_rec, true, true);
						nlapiLogExecution('DEBUG', 'ChildRec_id', ChildRec_id);
						
						
					}

					var usageLimit = nlapiGetContext().getRemainingUsage();
					if (usageLimit < 1000)
					{
						nlapiYieldScript();
					}
				}
				catch (e)
				{
					nlapiLogExecution('DEBUG', 'ERROR WHILE CREATING ITEM RECEIPT', e.message);
					po_rcv_rec.setFieldValue('custrecord_cntm_err_notes_po_per_rcv', e.message);
					po_rcv_rec.setFieldValue('custrecord_cntm_rcv_status', 4);
					var ChildRec_id = nlapiSubmitRecord(po_rcv_rec, true, true);
					nlapiLogExecution('DEBUG', 'ChildRec_id', ChildRec_id);
				}

			}
		}
		else
		{
			nlapiLogExecution('DEBUG', 'NO PO KEY IS AVAILABLE', '');
		}
		nlapiSubmitField('customrecord_cntm_bulk_rcv_reff', masterRecReff_Id, 'custrecord_cntm_blk_rcv_status', 3);

	}

}

