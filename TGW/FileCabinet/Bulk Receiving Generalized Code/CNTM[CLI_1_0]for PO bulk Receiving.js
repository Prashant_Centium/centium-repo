/**
 * Module Description
 * CNTM[CLI_1_0]for PO bulk Receiving
 * Version    Date            Author           Remarks
 * 1.00       15 Jul 2019     Shashank Rao
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type)
{
	window.onbeforeunload = null;
	// alert('Starts');
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord()
{
	var linecount = nlapiGetLineItemCount('custpage_po_sublist');
	if (linecount > 0)
	{
		var selecedLine = 0;
		for (var index = 1; index <= linecount; index++)
		{
			var chkbx = nlapiGetLineItemValue('custpage_po_sublist', 'custpage_receive', index);
			if (chkbx == 'T')
			{
				selecedLine += 1;
			}
		}
		if (selecedLine > 0)
		{
			return true;
		}
		else
		{
			alert('Please Select Some lines for processing.')
			return false;
		}

	}
	else
	{
		alert('No Data is their for processing');
		return false;
	}

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @param {String}
 *            name Field internal id
 * @param {Number}
 *            linenum Optional line item number, starts from 1
 * @returns {Boolean} True to continue changing field value, false to abort value change
 */
function clientValidateField(type, name, linenum)
{

	return true;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @param {String}
 *            name Field internal id
 * @param {Number}
 *            linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum)
{
	// alert(type+' , '+ name+' , '+ linenum);
	if (name == 'custpage_vendor')
	{
		var vendorId = nlapiGetFieldValue('custpage_vendor');
		var orderNumber = nlapiGetFieldValue('custpage_select_ordernumber');
		orderNumber = orderNumber.replace(/\s/g, '');
		var fromDate = nlapiGetFieldValue('custpage_fromdate');
		var toDate = nlapiGetFieldValue('custpage_todate');
		PopulateData(vendorId, orderNumber, fromDate, toDate);

	}
	if (name == 'custpage_select_ordernumber')
	{

		/*var orderNumber = nlapiGetFieldValue('custpage_select_ordernumber');
		orderNumber = orderNumber.replace(/\s/g, '');

		goToItemReceipt(orderNumber);*/
		

		var vendorId = nlapiGetFieldValue('custpage_vendor');
		var orderNumber = nlapiGetFieldValue('custpage_select_ordernumber');
		orderNumber = orderNumber.replace(/\s/g, '');
		var fromDate = nlapiGetFieldValue('custpage_fromdate');
		var toDate = nlapiGetFieldValue('custpage_todate');
		PopulateData(vendorId, orderNumber, fromDate, toDate);
	}
	if (name == 'custpage_fromdate')
	{
		var vendorId = nlapiGetFieldValue('custpage_vendor');
		var orderNumber = nlapiGetFieldValue('custpage_select_ordernumber');
		orderNumber = orderNumber.replace(/\s/g, '');
		var fromDate = nlapiGetFieldValue('custpage_fromdate');
		var toDate = nlapiGetFieldValue('custpage_todate');
		PopulateData(vendorId, orderNumber, fromDate, toDate);
	}
	if (name == 'custpage_todate')
	{
		var vendorId = nlapiGetFieldValue('custpage_vendor');
		var orderNumber = nlapiGetFieldValue('custpage_select_ordernumber');
		orderNumber = orderNumber.replace(/\s/g, '');
		var fromDate = nlapiGetFieldValue('custpage_fromdate');
		var toDate = nlapiGetFieldValue('custpage_todate');
		PopulateData(vendorId, orderNumber, fromDate, toDate);
	}
	if(name=='custpage_qty'){
		var availableQty = nlapiGetLineItemValue(type, 'custpage_available_qty', linenum);
		var qty = nlapiGetLineItemValue(type, 'custpage_qty', linenum);
//		alert('availableQty :'+availableQty+' , qty :'+qty);
		var diff = availableQty - qty;
		if(qty<=0){
			alert("You Can Not Select Quantity Less than or equal to Zero.");
			nlapiSetLineItemValue(type, 'custpage_qty', linenum, availableQty);
		}
		//if(diff<0){
		//	alert("You Can Not Select Quantity Greater than Available Qty.");
		//	nlapiSetLineItemValue(type, 'custpage_qty', linenum, availableQty);
		//}
		
		

		
	}

}

function goToItemReceipt(poOrderNumber)
{

	var purchaseorderSearch = nlapiSearchRecord("purchaseorder", null, [ [ "type", "anyof", "PurchOrd" ], "AND", [ "status", "anyof", "PurchOrd:B", "PurchOrd:E", "PurchOrd:D" ], "AND", [ "mainline", "is", "F" ], "AND", [ "shiprecvstatusline", "is", "F" ], "AND", [ "approvalstatus", "anyof", "2" ], "AND", [ "numbertext", "is", poOrderNumber ] ], [ new nlobjSearchColumn("type"), new nlobjSearchColumn("trandate"), new nlobjSearchColumn("entity"), new nlobjSearchColumn("tranid").setSort(false), new nlobjSearchColumn("linesequencenumber").setSort(false), new nlobjSearchColumn("item"), new nlobjSearchColumn("quantity"), new nlobjSearchColumn("quantityshiprecv"), new nlobjSearchColumn("shiprecvstatusline"), new nlobjSearchColumn("billaddress"), new nlobjSearchColumn("memo"), new nlobjSearchColumn("total"), new nlobjSearchColumn("currency"), new nlobjSearchColumn("postingperiod") ]);

	if (purchaseorderSearch)
	{
		if (purchaseorderSearch.length > 0)
		{
			// alert(JSON.stringify(purchaseorderSearch));
			var poId = purchaseorderSearch[0].getId();
//			var vendor = nlapiGetFieldValue('custpage_vendor');
			
			var vendor = nlapiLookupField('purchaseorder', poId, 'entity');
//			 alert('vendor: '+vendor);
			var url = nlapiResolveURL('SUITELET', 'customscript_cntm_po_bulk_receiving', 'customdeploy1');
			if(vendor)
			url += "&vendorName=" + vendor;
			
			url += "&poId=" + poId;
			window.open(url, '_self');

		}
	}
	else
	{
		alert('No Match.')
		nlapiSetFieldValue('custpage_select_ordernumber', '', false, false);
	}

}

function PopulateData(vendorId, orderNumber, fromDate, toDate)
{

	var url = nlapiResolveURL('SUITELET', 'customscript_cntm_po_bulk_receiving', 'customdeploy1');
	if (vendorId)
	{
		url += "&vendorName=" + vendorId;
		
	}
	if(orderNumber){
		url += "&poId=" + orderNumber;
	}
	/*if (orderNumber)
	{
		var purchaseorderSearch = nlapiSearchRecord("purchaseorder", null, [ [ "type", "anyof", "PurchOrd" ], "AND", [ "status", "anyof", "PurchOrd:B", "PurchOrd:E", "PurchOrd:D" ], "AND", [ "mainline", "is", "F" ], "AND", [ "shiprecvstatusline", "is", "F" ], "AND", [ "approvalstatus", "anyof", "2" ], "AND", [ "numbertext", "is", orderNumber ] ], [ new nlobjSearchColumn("type"), new nlobjSearchColumn("trandate"), new nlobjSearchColumn("entity"), new nlobjSearchColumn("tranid").setSort(false), new nlobjSearchColumn("linesequencenumber").setSort(false), new nlobjSearchColumn("item"), new nlobjSearchColumn("quantity"), new nlobjSearchColumn("quantityshiprecv"), new nlobjSearchColumn("shiprecvstatusline"), new nlobjSearchColumn("billaddress"), new nlobjSearchColumn("memo"), new nlobjSearchColumn("total"), new nlobjSearchColumn("currency"), new nlobjSearchColumn("postingperiod") ]);

		if (purchaseorderSearch)
		{
			if (purchaseorderSearch.length > 0)
			{
				// alert(JSON.stringify(purchaseorderSearch));
				var poId = purchaseorderSearch[0].getId();
				
				if(poId){
					var vendorName = nlapiLookupField('purchaseorder', poId, 'entity');
					if(vendorName){
//						alert('vendorName : '+vendorName);
						 url += "&vendorName=" + vendorName;
					}
				}
				 url += "&poId=" + poId;

			}
		}
		
		
	}
*/	if (fromDate)
	{
		url += "&fromDate=" + fromDate;
	}
	if (toDate)
	{
		url += "&toDate=" + toDate;
	}
	window.open(url, '_self');

}

function resets()
{
	try
	{
		var suitletUrl = nlapiResolveURL('SUITELET', 'customscript_cntm_po_bulk_receiving', 'customdeploy1');
		newWindow = window.open(suitletUrl, "_self");
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', 'Exception', e.message);
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @param {String}
 *            name Field internal id
 * @returns {Void}
 */
function clientPostSourcing(type, name)
{

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @returns {Void}
 */
function clientLineInit(type)
{

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @returns {Boolean} True to save line item, false to abort save
 */
function clientValidateLine(type)
{

	return true;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @returns {Void}
 */
function clientRecalc(type)
{

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @returns {Boolean} True to continue line item insert, false to abort insert
 */
function clientValidateInsert(type)
{

	return true;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Sublist internal id
 * @returns {Boolean} True to continue line item delete, false to abort delete
 */
function clientValidateDelete(type)
{

	return true;
}
