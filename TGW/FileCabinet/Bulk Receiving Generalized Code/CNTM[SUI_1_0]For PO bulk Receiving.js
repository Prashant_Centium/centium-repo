/**
 * Module Description
 * CNTM[SUI_1_0]For PO bulk Receiving
 * Version    Date            Author           Remarks
 * 1.00       15 Jul 2019     Shashank Rao
 * 1.1		  25 Jul 2019     Prashant Kumar   New Changes made
 * 1.2		  13 Aug 2019	  Prashant Kumar   Update for Order Line Number			
 * 1.3		  20 Aug 2019     Prashant Kumar   New Changes for Order Type
 * 1.4		  27 Aug 2019     Prashant Kumar   Updated into Generalized form
 */

/**
 * @param {nlobjRequest}
 *            request Request object
 * @param {nlobjResponse}
 *            response Response object
 * @returns {Void} Any output is written via response object
 */
// Script Parameters
var bulkReceving_SSId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_bulk_receiving_ss_id');
var refId=nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_refid');
var record = nlapiLoadRecord('purchaseorder', refId);

function suitelet(request, response)
{
	if (request.getMethod() == 'GET')
	{
		try
		{
			nlapiLogExecution('DEBUG', '**************IN GET', 'EXECUTION STARTS');
			var vendorNameParam = request.getParameter('vendorName');
			var poId_param = request.getParameter('poId');
			var fromDateParam = request.getParameter('fromDate');
			var toDateParam = request.getParameter('toDate');

			nlapiLogExecution('DEBUG', 'params', 'vendorNameParam : ' + vendorNameParam)

			var form = nlapiCreateForm('CENTIUM BULK RECEIVING');
			form.addFieldGroup('custpage_filters', 'FILTERS');

			form.setScript('customscript_cntm_cli_for_po_bulk_rcv');

			var vendorField = form.addField('custpage_vendor', 'select', 'VENDOR', 'vendor', 'custpage_filters').setLayoutType('startrow', 'startcol');

			var fromDate = form.addField('custpage_fromdate', 'date', 'FROM DATE', null, 'custpage_filters').setLayoutType('startrow', 'startcol').setDefaultValue(fromDateParam);

			var selectOrderNumber = form.addField('custpage_select_ordernumber', 'text', 'SELECT ORDER NUMBER', 'custpage_filters');

			var toDate = form.addField('custpage_todate', 'date', 'TO DATE', null, 'custpage_filters').setLayoutType('startrow', 'startcol').setDefaultValue(toDateParam);

			if (vendorNameParam)
			{
				vendorField.setDefaultValue(vendorNameParam);
			}
			if (vendorNameParam || poId_param || fromDateParam || toDateParam)
			{
				var filters = new Array();

				if (vendorNameParam)
				{
					filters.push(nlobjSearchFilter('name', null, 'anyof', vendorNameParam));
				}
				if (poId_param)
				{
					filters.push(nlobjSearchFilter("transactionnumber", null, "startswith", poId_param));

				}
				if (fromDateParam)
				{
					filters.push(nlobjSearchFilter('trandate', null, 'onorafter', fromDateParam));

				}
				if (toDateParam)
				{
					filters.push(nlobjSearchFilter('trandate', null, 'onorbefore', toDateParam));

				}
				else
				{
					filters.push(nlobjSearchFilter('trandate', null, 'onorbefore', new Date()));
				}

				var resultSet = loadRecords(null, bulkReceving_SSId, filters, null);
				// nlapiLogExecution('AUDIT', '', filters.length)

				// var filters = resultSet.filters;

				// form = get_Filters(form, filters);

				var columns = resultSet.columns;

				var dataArray = new Array();

				if (resultSet)
				{
					form.addButton('custpage_reset', 'Reset', 'resets()');
					form.addSubmitButton('Submit');

					var subTab = form.addSubTab('custpage_purchase_order_details', 'Purchase Order Details');

					var sublist = form.addSubList('custpage_po_sublist', 'list', 'Purchase Order Details', 'custpage_purchase_order_details');
					sublist.addMarkAllButtons();
					sublist.addField('custpage_internal_id', 'text', 'InternalId').setDisplayType('hidden');
					sublist.addField('custpage_receive', 'checkbox', 'RECEIVE');

					// Columns
					for (var i = 0; i < columns.length; i++)
					{

						if (columns[i].getName() == 'quantity')
						{
							sublist.addField('custpage_available_qty', 'integer', 'Available Qty').setDisplayType('inline');
							sublist.addField('custpage_qty', 'integer', 'Enter Qty').setDisplayType('entry');
						}
						else if (columns[i].getName() == 'line')
						{
							sublist.addField('custpage_' + columns[i].getName(), 'text', columns[i].getLabel()).setDisplayType('hidden');

						}
						else
						{
							sublist.addField('custpage_' + columns[i].getName(), 'text', columns[i].getLabel()).setMaxLength(1000);
						}

					}
					// Values
					for (var j = 0; j < resultSet.data.length; j++)
					{
						var data = new Object();
						data['custpage_internal_id'] = resultSet.data[j].getId();
						for (var k = 0; k < columns.length; k++)
						{

							// These all Qtys are required in saved search
							if (columns[k].getName() == 'quantity' && resultSet.data[j].getValue('quantityshiprecv') && resultSet.data[j].getValue('quantityuom'))
							{
								var rcvQty = parseInt(resultSet.data[j].getValue('quantityshiprecv'));

								var qtyInTransUnits = parseInt(resultSet.data[j].getValue('quantityuom'));

								var multple = parseInt(resultSet.data[j].getValue('quantity')) / qtyInTransUnits;

								data['custpage_available_qty'] = parseInt((parseInt(resultSet.data[j].getValue('quantity')) - rcvQty) / multple).toFixed(0);
								data['custpage_qty'] = parseInt((parseInt(resultSet.data[j].getValue('quantity')) - rcvQty) / multple).toFixed(0);

							}
							else if (columns[k].getType() == 'select')
							{
								data['custpage_' + columns[k].getName()] = resultSet.data[j].getText(columns[k]);

							}
							else if (columns[k].getType() == 'checkbox')
							{
								data['custpage_' + columns[k].getName()] = resultSet.data[j].getValue(columns[k]) == 'F' ? 'No' : 'Yes';

							}
							else
							{
								data['custpage_' + columns[k].getName()] = resultSet.data[j].getValue(columns[k]);

							}

						}
						nlapiLogExecution('AUDIT', '', 'DATA: ' + JSON.stringify(data));

						dataArray.push(data);

					}

					sublist.setLineItemValues(dataArray);
				}
				else
				{
					form = nlapiCreateForm('NOTICE');
					form.addField('custpage_notice', 'textarea', 'NOTICE').setDefaultValue('NO DATA FOUND FOR THIS VENDOR.').setDisplayType('inline');
				}

			}

			response.writePage(form);

			nlapiLogExecution('DEBUG', '**************IN GET', 'EXECUTION ENDS*****');

		}
		catch (e)
		{
			response.write('IN GET : ' + e.message);
		}
	}
	else
	{
		try
		{
			nlapiLogExecution('DEBUG', '**************IN POST', 'EXECUTION STARTS');

			var po_Id_map = {};
			var po_id_list = [];
			var vendorName = request.getParameter('custpage_vendor');

			var lineCount = request.getLineItemCount('custpage_po_sublist');

			var bulk_Rcv_Rec = nlapiCreateRecord('customrecord_cntm_bulk_rcv_reff');

			bulk_Rcv_Rec.setFieldValue('custrecord_cntm_blk_ref_created_by', nlapiGetUser());

			if (vendorName)
			{
				bulk_Rcv_Rec.setFieldValue('custrecord_cntm_blk_rcv_vendor_name', vendorName);
			}
			bulk_Rcv_Rec.setFieldValue('custrecord_cntm_blk_rcv_status', 1);

			var bulk_Rcv_Rec_Id = nlapiSubmitRecord(bulk_Rcv_Rec, true, true);

			if (lineCount > 0)
			{
				for (var index = 1; index <= lineCount; index++)
				{
					var checkBox = request.getLineItemValue('custpage_po_sublist', 'custpage_receive', index);

					if (checkBox == 'T')
					{
						var obj = new Object();
						var po_Id = request.getLineItemValue('custpage_po_sublist', 'custpage_internal_id', index);
						obj.po_Id = po_Id;
						obj.item_Id = request.getLineItemValue('custpage_po_sublist', 'custpage_item', index);
						obj.line = request.getLineItemValue('custpage_po_sublist', 'custpage_line', index);
						obj.qty = request.getLineItemValue('custpage_po_sublist', 'custpage_qty', index);
						po_id_list.push(obj);

					}
				}
			}

			po_Id_map[bulk_Rcv_Rec_Id] = po_id_list;

			var tempFile = nlapiCreateFile('Bulk_Po_Receiving', 'PLAINTEXT', JSON.stringify(po_Id_map));
			var folderId = nlapiGetContext().getSetting('SCRIPT', 'custscript_cntm_folder_id_for_blk_rcv');

			tempFile.setFolder(folderId);
			var fileId = nlapiSubmitFile(tempFile);
			nlapiLogExecution('DEBUG', 'MAP in POST', JSON.stringify(po_Id_map));

			nlapiLogExecution('DEBUG', 'fileId', fileId);

			nlapiScheduleScript('customscript_cntm_bulk_receiving_from_po', 'customdeploy_cntm_for_bulk_rcving', {
				custscript_cntm_po_id_list : fileId
			});

			nlapiSetRedirectURL('RECORD', 'customrecord_cntm_bulk_rcv_reff', bulk_Rcv_Rec_Id);
			nlapiLogExecution('DEBUG', '**************IN POST', 'EXECUTION ENDS*****');
		}
		catch (e)
		{
			response.write('IN POST : ' + e.message);

		}

	}

}

/*
 * function getPeriod(postingPeriod) { var purchaseorderSearch = nlapiSearchRecord("purchaseorder", null, [ [ "type", "anyof", "PurchOrd" ] ], [ new nlobjSearchColumn("postingperiod", null, "GROUP") ]);
 * 
 * if (purchaseorderSearch) { for (var index = 0; index < purchaseorderSearch.length; index++) { var postingPeriodValue = purchaseorderSearch[index].getValue("postingperiod", null, "GROUP"); var postingPeriodText = purchaseorderSearch[index].getText("postingperiod", null, "GROUP");
 * 
 * postingPeriod.addSelectOption(postingPeriodValue, postingPeriodText); } } }
 */

/**
 * Load the saved search data for more than 1000 records
 * 
 * @param {string}recordTypeId
 * @param {string}savedSearchId
 * @param {array}filters
 * @param {array}columns
 * @returns {nlobjSearch}results
 */

function loadRecords(recordTypeId, savedSearchId, filters, columns)
{
	var search = nlapiLoadSearch(recordTypeId, savedSearchId);

	search.addFilters(filters);
	search.addColumns(columns);
	var filters = search.getFilters();
	var columns = search.getColumns();
	var searchResults = search.runSearch();
	var results = new Object();
	results.filters = filters;
	results.columns = columns;
	var resultIndex = 0;
	var resultStep = 1000;
	var resultSet;

	// more than 1000 records
	do
	{
		resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
		if (resultIndex == 0)
			results.data = resultSet;
		else if (resultSet)
			results.data = results.data.concat(resultSet);
		resultIndex = resultIndex + resultStep;
	}
	while (resultSet.length > 0);
	nlapiLogExecution('DEBUG', '', 'Total Records: ' + results.data.length);

	return results;

}

function getOptions(fieldName)
{

	var options;
	var field = getField(fieldName);
	if (field)
		options = field.getSelectOptions();
	return options;
}

/*
 * function get_Filters(form, filters) { nlapiLogExecution('AUDIT', '', 'In Get_Filters');
 * 
 * for (var line = 0; line < filters.length; line++) {
 * 
 * var field = getField(filters[line].name);
 * 
 * if (field) { form = add_field(form, field); } }
 * 
 * return form; }
 */
function getField(fieldName)
{
	nlapiLogExecution('AUDIT', '', 'In Get Field');

	var field = record.getField(fieldName);
	if (!field)
	{
		field = record.getLineItemField('item', fieldName, 1);
	}

	return field;

}

/*
 * function add_field(form, field) { nlapiLogExecution('AUDIT', '', 'In Add_Field');
 * 
 * if (field.type == 'select') {
 * 
 * var options = getOptions(field.name); nlapiLogExecution('AUDIT', '', 'Field Label :' + field.label); if (options) { var filterField = form.addField('custpage_filter_' + field.name, field.type, field.label); filterField.addSelectOption("", "", false);
 * 
 * for (var selectLine = 0; selectLine < options.length; selectLine++) {
 * 
 * filterField.addSelectOption(options[selectLine].getId(), options[selectLine].getText(), false); } } else { form.addField('custpage_filter_' + field.name, 'text', field.label); } } else { form.addField('custpage_filter_' + field.name, field.type, field.label); }
 * 
 * return form; }
 */
