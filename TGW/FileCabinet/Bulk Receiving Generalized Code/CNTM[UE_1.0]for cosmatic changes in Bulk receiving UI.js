/**
 * Module Description
 * CNTM[UE_1.0]for cosmatic changes in Bulk receiving UI
 * Version    Date            Author           Remarks
 * 1.00       17 Jul 2019     Shashank Rao
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * 
 * @appliedtorecord recordType
 * 
 * @param {String}
 *            type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm}
 *            form Current form
 * @param {nlobjRequest}
 *            request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request)
{	var recordType = nlapiGetRecordType();

	var triggerType = nlapiGetContext().getExecutionContext();
	if(triggerType == 'userinterface'){
	if (type == 'view')
	{
		var aa = form
		.addField('custpage_a', 'inlinehtml', 'a')
		.setDefaultValue(
				'<script>window.setTimeout(function(){document.getElementById("recmachcustrecord_cntm_blk_rcv_rec_reff_main_form").style.display = "none";},500);</script>')

//var view = nlapiGetField('searchid').setDisplayType('hidden');
//var fileLogs = nlapiGetField('existingrecmachcustrecord_cntm_sftp_folder_log_ref').setDisplayType('hidden');
		var status = nlapiGetFieldValue('custrecord_cntm_blk_rcv_status');
		
		
		if (status=='3') {
			
		} else {
			var refreshBtn = form.addButton('custpage_referes',
					'Refresh', 'window.location.reload()');
		}
		
		
	}
	else
	{
		var error = nlapiCreateError('INVALID OPERATION', 'USER NOT ALLOWED TO '+type+' THIS RECORD.', true);
		throw error;
	}
	}
}
