/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Nov 2018     Yogesh Jagdale
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){

  var subsidiaryId=nlapiGetFieldValue('subsidiary');
		if(subsidiaryId!=""){
           
			var LocationId= callSuitelet(subsidiaryId);
          nlapiLogExecution('DEBUG','subsidiaryId: '+subsidiaryId,'LocationId: '+LocationId);
			if(nlapiGetRecordType()=="inventoryadjustment")
			{
				nlapiSetFieldValue('adjlocation', LocationId, true, true);
			}
			else{
				nlapiSetFieldValue('location', LocationId, true, true);
			}
		}
		else{nlapiSetFieldValue('location', "", true, true);
		}
}

function postSourcing(type, name){
	if(name=='subsidiary')
	{
		var subsidiaryId=nlapiGetFieldValue('subsidiary');
		if(subsidiaryId!=""){
           
			var LocationId= callSuitelet(subsidiaryId);
          nlapiLogExecution('DEBUG','subsidiaryId: '+subsidiaryId,'LocationId: '+LocationId);
			if(nlapiGetRecordType()=="inventoryadjustment")
			{
				nlapiSetFieldValue('adjlocation', LocationId, true, true);
			}
			else{
				nlapiSetFieldValue('location', LocationId, true, true);
			}
		}
		else{nlapiSetFieldValue('location', "", true, true);
		}
	}
	 if(name=="tosubsidiary")
	{
		var subsidiaryId=nlapiGetFieldValue('tosubsidiary');
		if(subsidiaryId!="")
		{var LocationId= callSuitelet(subsidiaryId);
		if(nlapiGetRecordType()=="intercompanytransferorder")
		{
			nlapiSetFieldValue('transferlocation', LocationId, true, true);
		}
		}
		else{nlapiSetFieldValue('location', "", true, true);
		}
	}
}

function callSuitelet(subsidiaryId)
{
	var callSuitelet = nlapiResolveURL('SUITELET','customscript_fetch_loc_on_subsidiary','customdeploy_fetch_loc_on_subs_deploy', false);
	callSuitelet +='&subsidiary_id='+subsidiaryId; 
	var response=  nlapiRequestURL(callSuitelet, null, null, null, null);
	return response.getBody();
}



function CopyMemoFromSOtoIF(){
  try{
    if(nlapiGetFieldText('createdfrom').indexOf('Sales Order')>-1)
      {
var soReference=nlapiGetFieldValue('createdfrom');
if(soReference!="" && soReference!=null && soReference!=undefined)
{
nlapiSetFieldValue('custbody_custom_memo',nlapiLookupField('salesorder',soReference,'memo'));
}
      }
  }
  catch(e)
    {
      alert('Some Error Occured. NS error:'+e.message);
    }
}


function PrintSOandIf(id,type){
 
   var url =nlapiResolveURL('SUITELET', 'customscript_print_suitelet', 'customdeploy_print_suitelet')+'&recordId='+id+'&recordType='+type;
	window.open(url);

}

function SoWarning(){
  debugger;
  
	try{
		if(nlapiGetRecordType()=="itemfulfillment"){
			var flag=false;
			var itemObjArray=new Array;
			if(nlapiGetFieldText('createdfrom').indexOf('Sales Order')>-1)
			{

				var soRecord=nlapiLoadRecord('salesorder',nlapiGetFieldValue('createdfrom'));
				for(var i=1;i<=soRecord.getLineItemCount('item');i++)
				{
					var soItemObj={};
					soItemObj.item=soRecord.getLineItemValue('item','item',i);
					soItemObj.expShipDate=soRecord.getLineItemValue('item','expectedshipdate',i);
					itemObjArray.push(soItemObj);
				}
               nlapiLogExecution('DEBUG','itemObjArray',JSON.stringify(itemObjArray));
				for(var j=1;j<=nlapiGetLineItemCount('item');j++)
				{

					for(var k=0;k<itemObjArray.length;k++)
					{
                      nlapiLogExecution('DEBUG','before if',itemObjArray[k].item==nlapiGetLineItemValue('item','item',j) && nlapiStringToDate(itemObjArray[k].expShipDate)>new Date() && itemObjArray[k].expShipDate!=null);
						if(itemObjArray[k].item==nlapiGetLineItemValue('item','item',j) && nlapiGetLineItemValue('item','itemreceive',j)=='T' && nlapiStringToDate(itemObjArray[k].expShipDate)>nlapiStringToDate(nlapiGetFieldValue('trandate')) && itemObjArray[k].expShipDate!=null){
							flag=true;
							break;
						}
					}
				}
				if(flag){
					if(confirm('You are trying to fulfill an item earlier than its Expected Ship Date. Please Click on OK to continue or click on Cancel to go Back.'))
					{
						return true;
					}
					else{
						return false;
					}
				}else{
					return true;
				}
			}
			else{
				return true;
			}
		}
		else{return true;}
	}
	catch(e)
	{
		alert('Some Error Occured. NS error:'+e.message);
      nlapiLogExecution('ERROR','in catch',e.message);
	}
}

function printPickTicket(){
 
   var url =nlapiResolveURL('SUITELET', 'customscript_cntm_print_pick_tckt', 'customdeploy_cntm_print_pick_tckt')+'&recordId='+nlapiGetRecordId();
	window.open(url);

}